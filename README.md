# TopDecked Price Utils

Project to import and aggregate pricing data for MTG/Card data from various sources.

## Getting Started

You'll need a few prerequisites:

* Node.js version 10.X: <https://nodejs.org/en/>
* MongoDB: 3.6.x <https://docs.mongodb.com/manual/installation/>

You may also want Robo3T (RoboMongo) to inspect the local database via a GUI:

* Robomongo: <https://robomongo.org/download> (Just download Robo3T, the other project is some commercial thing that's not Robo3T);

## Verify the environment is sane

Once node (includes npm), and mongodb are installed, verify the environment by running:

```bash
npm -v
node -v
mongo --version
```

Make sure your MongoDB instance is running.

### Download the npm dependencies and card data-file

From within the project root folder, run:

```bash
npm install
wget https://mtgjson.com/json/AllSets-x.json.zip
unzip Allsets-x.json.zip
```

### Run the scripts

From within the project root folder, run:

`npm start scripts/cards-import-mtgjson.js Allsets-x.json`

Price update script for TCGPlayer Legacy Partner API:

`npm start scripts/cards-update-prices-tcg.js ["setcode"] ["Card Name"]`

Update "Underground Sea" from Alpha set:
`npm start scripts/cards-update-prices-tcg.js "LEA" "Underground Sea"`

Update all cards name Underground Sea:
`npm start scripts/cards-update-prices-tcg.js "" "Underground Sea"`

Update all cards in Alpha set:
`npm start scripts/cards-update-prices-tcg.js "LEA"`

### Debugging

To run the scripts in DEBUg mode with a TEST stage:

`NODE_ENV=test DEBUG=true NODE_PATH=$(pwd) node scripts/test-tcg-player.js`
