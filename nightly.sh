#!/bin/bash

# Get the source directory of the script, even if it's buried in a symlink. 
# Credit: https://stackoverflow.com/questions/59895/getting-the-source-directory-of-a-bash-script-from-within
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# Script begins here
cd $DIR

export LOG_ERROR_FILE="error.log"
export LOG_OUT_FILE="combined.log"
if [ -f $LOG_ERROR_FILE ]; then
    echo "" > $LOG_ERROR_FILE
fi
if [ -f $LOG_OUT_FILE ]; then
    echo "" > $LOG_OUT_FILE
fi

HOSTNAME=`hostname`
runfunction()
{
    TASK_LOG="task.log"
    echo "Executing: $@"
    if eval "$@" > $TASK_LOG 2>&1; then
        echo " ... Success"
    else
        TO="admin@topdecked.com"
        FROM="batch-failed@topdecked.com"
        HEADER="Batch job failed: $@"
        TASK_OUTPUT=`cat $TASK_LOG`
        MSG=`printf "%s\r\n\r\n%s\n" "$HEADER"  "$TASK_OUTPUT"`
        echo "$MSG"
        echo "... Failed"
        echo "... Sending mail to $TO"
        echo "$MSG" | mail -s "$HOSTNAME: FAILED - $SOURCE" $TO -A $LOG_ERROR_FILE -a "From: $FROM"
        echo "... Failed"
        exit
    fi

}

# CD to our script path before performing work
NODE="node"
export NODE_PATH=$DIR
export NODE_OPTIONS=--max_old_space_size=8192

if [ ! -d "build" ]; then
    echo ">>> PROJECT NOT BUILT - BUILDING"
    `npm i`
    `npm run build`
fi

RUN="$NODE $DIR/build/main/index.js"

echo "$(date +%Y-%m-%d.%H:%M:%S) >>> NIGHTLY UPDATE STARTING"

# Run the scripts
runfunction "$RUN import -s scryfall --mark-deprecated $@" && \
runfunction "$RUN metadata -s scryfall $@" && \
runfunction "$RUN metadata -s cardkingdom $@" && \
runfunction "$RUN metadata -s tcgplayer $@" && \
runfunction "$RUN prices -s cardkingdom $@" && \
runfunction "$RUN prices -s scryfall $@" && \
runfunction "$RUN prices -s tcgplayer $@" && \
runfunction "$RUN summarize $@" && \
runfunction "$RUN backportPrices $@" && \
runfunction "$RUN priceMotionSummary $@" && \

if [ $? -ne 0 ]
then
    echo "$(date +%Y-%m-%d.%H:%M:%S) >>> NIGHTLY UPDATE FAILED"
else
    echo "$(date +%Y-%m-%d.%H:%M:%S) >>> NIGHTLY UPDATE SUCCEEDED"
fi

# Return to the original directory, just in case
cd -
