
import { Command } from 'commander';

import { Booleans, Debounce, Strings } from '@topdecked/common-util';

import { database } from './config/database';
import { CatalogService } from './services/api/metadata.api';
import { CardKingdomCatalog } from './services/cardkingdom/catalog';
import { CardKingdomPriceService } from './services/cardkingdom/price';
import { PriceService } from './services/interfaces';
import { ScryfallCatalog } from './services/scryfall/catalog';
import {
  DEFAULT_IMAGE_OUTDIR, DEFAULT_IMAGE_PARTITION, ScryfallImageDownloader
} from './services/scryfall/images';
import { ScryfallImporter } from './services/scryfall/importer';
import { ScryfallPriceService } from './services/scryfall/price';
import { TcgPlayerCatalog } from './services/tcgplayer/catalog';
import { TcgPlayerPriceService } from './services/tcgplayer/price';
import { CardMigrationService } from './services/topdecked/card-migration-service';
import { PriceSummaryService } from './services/topdecked/card-summarizer';
import { PriceBackportService } from './services/topdecked/price-backporter';
import { PriceTrendService } from './services/topdecked/price-trend-service';
import { ENV } from './util/env';
import { logger } from './util/logger';

const program = new Command();
program
  .version('0.0.1', '-v, --version')
  .option('-d, --database-name <db>', 'MongoDB Database name', /^([a-z0-9-_]+)$/i, 'local')
  .option('-h, --database-host <host>', 'MongoDB Database host', /^([a-z0-9-_]+)$/i, 'localhost')
  .option('-r, --database-port <port>', 'MongoDB Database port', /^([0-9]+)$/i, '27017')

  .option('-P, --tcg-public-key <key>', 'TCGPlayer Auth Public Key', process.env.TCGPLAYER_AUTH_PUBLIC_KEY)
  .option('-p, --tcg-private-key <key>', 'TCGPlayer Auth Private Key', process.env.TCGPLAYER_AUTH_PRIVATE_KEY)

  .option('-Y, --dry-run', 'Enable dry-run mode, do not actually do anything')
  .option('-D, --debug', 'Enable debug mode')
  .option('-L, --log-level <level>', 'Set runtime log level', /^(debug|verbose|warn|error|info|trace)$/i, 'info');

program.on('option:log-level', function () {
  const options = program.opts();
  logger.level = options.logLevel;
  logger.debug('Setting log level [%s]', logger.level);
});

program.on('option:debug', function () {
  const options = program.opts();
  process.env.DEBUG = (!!options.debug) + '';
  logger.debug('Setting debug mode [%s]', process.env.DEBUG);
});

program.on('option:tcg-public-key', function () {
  const options = program.opts();
  if (options.tcgPublicKey) {
    process.env.TCGPLAYER_AUTH_PUBLIC_KEY = (options.tcgPublicKey + '').trim();
    logger.warn('TCGPlayer public key configured via command option.');
    logger.debug('-P --public %s', !Strings.isNilOrEmpty(process.env.TCGPLAYER_AUTH_PUBLIC_KEY));
  }
});

program.on('option:tcg-private-key', function () {
  const options = program.opts();
  if (options.tcgPrivateKey) {
    process.env.TCGPLAYER_AUTH_PRIVATE_KEY = (options.tcgPrivateKey + '').trim();
    logger.warn('TCGPlayer private key configured via command option.');
    logger.debug('-p --private %s', !Strings.isNilOrEmpty(process.env.TCGPLAYER_AUTH_PRIVATE_KEY));
  }
});

program.on('option:dry-run', function () {
  const options = program.opts();
  process.env.DRY_RUN = (!!options.dryRun) + '';
  if (process.env.DRY_RUN) {
    logger.warn('Performing dry run. %s', process.env.DRY_RUN);
  }
});

program.on('option:database-name', function () {
  const options = program.opts();
  logger.debug('Using database name [%s]', options.databaseName);
  database.database = options.databaseName;
});

program.on('option:database-host', function () {
  const options = program.opts();
  logger.debug('Using database host [%s]', options.databaseHost);
  database.host = options.databaseHost;
});

program.on('option:database-port', function () {
  const options = program.opts();
  logger.debug('Using database port [%s]', options.databasePort);
  database.port = options.databasePost;
});

program
  .command('import')
  .option('-s, --source <source>', 'source of the data to be imported', /^(scryfall|tcgplayer)$/i, 'scryfall')
  .option('-C, --cards', 'import card data')
  .option('-i, --card-id <cardId>', 'import only one specified card')
  .option('-c, --set-code <setCode>', 'import just one set')
  .option('-S, --sets', 'import set data')
  .option('-m, --mark-deprecated', 'mark cards as deprecated if they no longer appear in scryfall')
  .option('-f, --force', 'force the download & import logic')
  .option('-I, --forceImport', 'force the import logic')
  .option('-x, --forceDeprecation', 'force deprecation even if maximum threshold is reached')
  .option('-D, --forceDownload', 'force the download logic')
  .description('import CARD/SET/RULING data from selected source')
  .action(async (cmd) => {
    try {
      const importAll = (!cmd.cards && !cmd.cardId && !cmd.sets && !cmd.rulings);
      if (importAll) {
        logger.info('importing sets, cards and rulings data from %s', cmd.source);
      }
      if (!cmd.source) {
        cmd.source = 'scryfall';
      }
      if (cmd.source !== 'scryfall') {
        throw new Error(`invalid import source: ${cmd.source}`);
      }

      const scryfallImporter = new ScryfallImporter();
      if (cmd.sets || importAll) {
        await scryfallImporter.importSets({
          forceDownload: cmd.force || cmd.forceDownload,
          forceImport: cmd.force || cmd.forceImport,
          markDeprecated: cmd.markDeprecated,
          pretend: Booleans.isTrueProperty(process.env.DRY_RUN),
          setCode: cmd.setCode
        });
      }
      if (cmd.cards || cmd.cardId || importAll) {
        await scryfallImporter.importCards({
          cardId: cmd.cardId,
          forceDownload: cmd.force || cmd.forceDownload,
          forceImport: cmd.force || cmd.forceImport,
          markDeprecated: cmd.markDeprecated,
          pretend: Booleans.isTrueProperty(process.env.DRY_RUN),
          setCode: cmd.setCode
        });
      }

      logger.debug('done importing data');
      process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error importing cards/sets: %o', err);
      process.exit(1);
    }
  });


program
  .command('images')
  .option('-s, --source <source>', 'source of the images to be downloaded', /^(scryfall|tcgplayer)$/i, 'scryfall')
  .option('-C, --cards', 'import card images')
  .option('-S, --sets', 'import set symbols')
  .option('-z, --sizes <sizes>', 'comma separated list of image sizes', null)
  .option('-d, --default', 'default to parent set if not found')
  .option('-c, --code <code>', 'code of specific set to download')
  .option('-i, --id <id>', 'id of specific card to download')
  .option('-p, --partition <type>', 'partition strategy to use when downloading (e.g. separate index per set)', DEFAULT_IMAGE_PARTITION)
  .option('-o, --outdir <outdir>', 'output directory where images are to be saved', DEFAULT_IMAGE_OUTDIR)
  .option('-f, --force', 'download even if files already exist on disk', false)
  .description('download CARD/SET images from selected source')
  .action(async (cmd) => {
    try {
      const importAll = (!cmd.cards && !cmd.sets);
      if (!cmd.source) {
        cmd.source = 'scryfall';
      }
      if (cmd.source !== 'scryfall') {
        throw new Error(`invalid download source: ${cmd.source}`);
      }

      if (!process.env.DRY_RUN) {
        if (importAll) {
          logger.info('downloading images from %s', cmd.source);
        }

        const scryfallImages = new ScryfallImageDownloader();
        if (cmd.sets || importAll) {
          logger.info('downloading set images from %s to %s', cmd.source, cmd.outdir);
          await scryfallImages.downloadImagesSets(cmd);
        }
        if (cmd.cards || importAll) {
          logger.info('downloading card images from %s', cmd.source);
          await scryfallImages.downloadImagesCards(cmd);
        }
      }

      logger.info('done downloading images');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error downloading set images: %o', err);
      return process.exit(1);
    }
  });

program
  .command('link')
  .option('-c, --set-code [setCode]', 'link a specific set code')
  .option('-f, --force', 'force linkage even if cards have been previously linked')
  .description('link CARD/SET data from db version 1 to version 2')
  .action(async (cmd) => {
    try {
      const migrateAll = (!cmd.setCode);
      const migration = new CardMigrationService();

      if (!process.env.DRY_RUN) {
        if (migrateAll) {
          logger.info('linking all sets / cards');
          await migration.linkAll(cmd.force);
        } if (cmd.setCode) {
          logger.info('linking sets / cards: %s', cmd.setCode);
          await migration.linkSetCards(cmd.setCode, { force: cmd.force, verbose: true });
        }
      }
      logger.debug('done linking');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error linking: %o', err);
      return process.exit(1);
    }
  });


program
  .command('linkSets')
  .option('-c, --set-code [setCode]', 'link a specific set code')
  .option('-f, --force', 'force linkage even if sets have been previously linked')
  .description('link SET data from db version 1 to version 2')
  .action(async (cmd) => {
    try {
      const migration = new CardMigrationService();
      if (!process.env.DRY_RUN) {
        await migration.linkAllSets(cmd.force);
      }
      logger.debug('done linking sets');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error linking: %o', err);
      return process.exit(1);
    }
  });


program
  .command('clearLinks')
  .description('clear linked SET/CARD data from db version 1 to version 2')
  .action(async (cmd) => {
    try {
      const migration = new CardMigrationService();
      await migration.clearLinks();
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error clearing links: %o', err);
      return process.exit(1);
    }
  });

program
  .command('clearBackported')
  .description('clear backported SET/CARD data from db version 1 to version 2')
  .action(async (cmd) => {
    try {
      if (!process.env.DRY_RUN) {
        const migration = new CardMigrationService();
        await migration.clearBackportedSetsCards();
      }
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error clearing backported data: %o', err);
      return process.exit(1);
    }
  });


function collect (value, previous) {
  return previous.concat([value]);
}

program
  .command('backportFields')
  .option('-c, --set-code [setCode]', 'backport a specific set code')
  .option('-f, --force', 'force backport even if card has previously been updated')
  .option('-F, --field <field>', 'A field to backport (supports multiples/list)', collect, [])
  .option('-i, --ignore-code [ignoreCode]', 'ignore a specific set code')
  .description('migrate field data from db version 2 to version 1')
  .action(async (cmd) => {
    try {
      const migration = new CardMigrationService();
      const opts = {
        ...cmd,
        fields: cmd.field
      };
      logger.info('backporting fields on all cards %o', opts.fields);

      if (!process.env.DRY_RUN) {
        await migration.backportFields(opts);
      }
      logger.debug('done backporting fields on all cards');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error backporting fields on all cards: %o', err);
      return process.exit(1);
    }
  });

program
  .command('backportLegalities')
  .option('-c, --set-code [setCode]', 'backport a specific set code')
  .option('-i, --ignore-code [ignoreCode]', 'ignore a specific set code')
  .description('back-port SET legality data from db version 2 to version 1')
  .action(async (cmd) => {
    try {
      const migration = new CardMigrationService();
      logger.info('backporting legalities');

      if (!process.env.DRY_RUN) {
        await migration.backportLegalities(cmd);
      }
      logger.debug('done backporting legalities');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error backporting legalities: %o', err);
      return process.exit(1);
    }
  });

program
  .command('backportMissing')
  .option('-f, --force', 'force backport even if cards have been previously copied [this erases old set & card data from destination]')
  .description('migrate SET data from db version 2 to version 1')
  .action(async (cmd) => {
    try {
      const migration = new CardMigrationService();
      logger.info('backporting missing cards');

      if (!process.env.DRY_RUN) {
        await migration.backportMissingCards(cmd.force);
      }
      logger.debug('done backporting cards');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error backporting missing cards: %o', err);
      return process.exit(1);
    }
  });

program
  .command('backportSet')
  .option('-c, --set-code <setCode>', 'migrate set code')
  .option('-f, --force', 'force backport even if cards have been previously copied [this erases old set & card data from destination]')
  .description('migrate SET data from db version 2 to version 1')
  .action(async (cmd) => {
    try {
      const migration = new CardMigrationService();

      if (!process.env.DRY_RUN) {
        if (cmd.setCode) {
          logger.info('backporting set: %s', cmd.setCode);
          await migration.backportSet(cmd.setCode, cmd.force);
        } else {
          logger.error('set code required');
        }
      }
      logger.debug('done backporting');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error backporting set: %s, %o', cmd.setCode, err);
      return process.exit(1);
    }
  });

program
  .command('backportPrices')
  .option('-c, --set-code [setCode]', 'backport prices for a specific set code')
  .option('-i, --card-id [cardId]', 'backport prices for a specific card id (v2)')
  .option('-b, --batch-size [batchSize]', 'max batch size before writes to DB occur')
  .option('-f, --force', 'force backport card prices & re-update trends for today')
  .description('back-port card pricing data from db version 2 to version 1')
  .action(async (cmd) => {
    try {
      const pb = new PriceBackportService();

      logger.info('backporting all prices');
      if (!process.env.DRY_RUN) {
        await pb.backportPrices(cmd);
      }

      logger.debug('done backporting prices');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error backporting prices: %o', err);
      return process.exit(1);
    }
  });

program
  .command('summarize')
  .option('-c, --set-code [setCode]', 'summarize data for a specific set code')
  .option('-i, --card-id [cardId]', 'summarize data for a specific card id (v2)')
  .option('-n, --card-name [cardName]', 'summarize data for a specific card name (v2)')
  .option('-b, --batch-size [batchSize]', 'max batch size before writes to DB occur')
  .option('-f, --force', 'force calculate card data and price summaries & overwrite current data')
  .description('summarize card data (prices, games_available) from all sources into fields stored on each card instance')
  .action(async (cmd) => {
    try {
      const ps = new PriceSummaryService();

      logger.info('summarizing card data');
      if (!process.env.DRY_RUN) {
        await ps.summarize(cmd);
      }

      logger.debug('done summarizing');
      return process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error summarizing: %o', err);
      return process.exit(1);
    }
  });

program
  .command('diff')
  .option('-c, --set-code [setCode]', 'set code -- used for v1 and v2')
  .option('-s, --set-code-v1 [setCodeV1]', 'v1 set code')
  .option('-S, --set-code-v2 [setCodeV2]', 'v2 set code')
  .description('Display a card diff between v1 and v2 databases')
  .action(async (cmd) => {
    try {
      const migrateAll = !(cmd.setCode || cmd.setCodeV1 || cmd.setCodeV2);
      const migration = new CardMigrationService();

      if (migrateAll) {
        logger.info('diffing all sets / cards');
        await migration.diffAll();
      } if (cmd.setCode || cmd.setCodeV1 || cmd.setCodeV2) {
        logger.info('diffing sets / cards: %s, %s, %s', cmd.setCode, cmd.setCodeV1, cmd.setCodeV2);
        await migration.diffSet(cmd.setCodeV1 || cmd.setCode, cmd.setCodeV2 || cmd.setCode);
        logger.info('-----------------------');
      }

      const delayExit = (Debounce.fn(() => {
        return process.exit(ENV.getExitCode());
      }, 2000)());
      return delayExit;
    } catch (err) {
      logger.error('error displaying diff: %o', err);
      return process.exit(1);
    }
  });

program
  .command('prices')
  .description('update database with prices from selected source')
  .option('-s, --source <source>', 'source of the data to be updated', /^(scryfall|tcgplayer|cardkingdom)$/i, 'tcgplayer')
  .option('-i, --card-id [cardId]', 'Only update pricing for the given Card ID')
  .option('-c, --set-code <value>', 'Only update pricing for the given Set Code')
  .option('-n, --card-name [name]', 'Only update pricing for the given Card Name (all copies)')
  .option('-F, --force', 'Force update, even within 24h window')
  .action(async (cmd) => {
    try {
      let priceService: PriceService;
      if (cmd.source === 'cardkingdom') {
        priceService = new CardKingdomPriceService();
      } else if (cmd.source === 'scryfall') {
        priceService = new ScryfallPriceService();
      } else {
        logger.debug('TCGPLAYER_AUTH_PUBLIC_KEY %s, TCGPLAYER_AUTH_PRIVATE_KEY %s',
          process.env.TCGPLAYER_AUTH_PUBLIC_KEY, process.env.TCGPLAYER_AUTH_PRIVATE_KEY);
        priceService = new TcgPlayerPriceService();
      }

      logger.info('updating prices from %s', cmd.source);

      if (!process.env.DRY_RUN) {
        logger.info(`Retrieving prices for ${cmd.setCode} / ${cmd.cardName}`);
        await priceService.updatePrices({
          tcgPublicKey: program.options['tcgPublicKey'],
          tcgPrivateKey: program.options['tcgPrivateKey'],
          setCode: cmd.setCode,
          cardName: cmd.cardName,
          cardId: cmd.cardId,
          force: cmd.force
        });
      }

      logger.info('done updating prices');
      process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error updating prices: %o', err);
      process.exit(1);
    }
  });

program
  .command('priceMotionSummary')
  .description('calculate and update price motion trends for the current day (uses all available price data)')
  .option('-F, --force', 'Force recalculate, even within 24h window')
  .option('-t, --format [format]', 'Format for which to test / calculate trends (requires --pretend mode)')
  .option('-p, --pretend', 'Do everything but modify the database')
  .option('-c, --set-code [setCode]', 'set code to test / calculte (requires --pretend mode)')
  .action(async (cmd) => {
    try {
      const service = new PriceTrendService();
      logger.info('updating price motion summary');

      const trends = await service.calculateMotionSummary({
        setCode: cmd.setCode,
        format: cmd.format,
        force: cmd.force,
        pretend: cmd.pretend,
      });
      if (!process.env.DRY_RUN) {
        await service.saveMotionSummary(trends, cmd);
        logger.verbose('price motion summary data persisted to database');
      }

      logger.info('done updating price motion summary');
      process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error updating prices: %o', err);
      process.exit(1);
    }
  });



program
  .command('metadata')
  .option('-c, --set-code [setCode]', 'set code to update')
  .option('-s, --source <source>', 'source of the metadata to be processed', /^(cardkingdom|scryfall|tcgplayer)$/i, 'tcgplayer')
  .option('-i, --card-id [cardId]', 'Only update pricing for the given Card ID')
  .option('-I, --incremental', 'attempt an incremental update (WARNING: THIS CAN BE COSTLY IF DB IS NOT PRIMED)')
  .option('-f, --force', 'force updating, even if cards have previously been updated')
  .description('update existing DB with metadata specific from the selected source')
  .action(async (cmd) => {
    try {
      logger.info('updating metadata from %s', cmd.source);
      if (!process.env.DRY_RUN) {

        let catalog: CatalogService;
        if (cmd.source && cmd.source === 'tcgplayer') {
          catalog = new TcgPlayerCatalog();
        } else if (cmd.source && cmd.source === 'scryfall') {
          catalog = new ScryfallCatalog();
        } else if (cmd.source && cmd.source === 'cardkingdom') {
          catalog = new CardKingdomCatalog();
        } else {
          throw new Error('Unknown source: ' + cmd.source);
        }

        await catalog.cleanupDeprecatedMetadata(cmd);
        await catalog.updateAllMetadata(cmd);
      }
      logger.info('done updating metadata');
      process.exit(ENV.getExitCode());
    } catch (err) {
      logger.error('error updating database metadata from \'%s\': %o', cmd.source, err);
      process.exit(1);
    }
  });

const run = async () => {
  await database.ready();
  program.parse(process.argv);
};

run();

