import fs from 'fs';

import { MongooseInstance } from '@topdecked/common-model';

import { ENV } from '../util/env';
import { logger } from '../util/logger';

const mongoose = MongooseInstance.get();

export class Database {
  private _ready: Promise<void> = null;

  public host = 'localhost';
  public port = 27017;
  public database = ENV.isTest() ? 'test' : 'topdecked';
  public options = {};
  public mongoose = null;

  public constructor() {
  }

  public async ready () {
    if (!this._ready) {
      mongoose.Promise = global.Promise;
      mongoose.set('autoCreate', true);
      this._ready = this.connect().then((mgoose) => {
        logger.info('Connected to database: %o, %o', mongoose.connection.config, this.options);
        this.mongoose = mgoose;
      });
      return this;
    }
    return this._ready;
  }

  private async connect () {
    const dbConfigFile = process.env.TD_DATABASE_CONNECTION_CONFIG_FILE;
    if (dbConfigFile) {
      logger.info('Using MongoDB configuration at: %s', dbConfigFile);
      const config = JSON.parse(fs.readFileSync(dbConfigFile, { encoding: 'utf-8' }));
      if (config.url) {
        const options = { ...this.options, ...config.options ?? {} };
        logger.info('Final MongoDB configuration opts: %o', options);
        return mongoose.connect(config.url, options);
      } else {
        throw new Error('Invalid connection URL: ' + config.url);
      }
    } else {
      logger.info('Using MongoDB default connection.');
      logger.info('Connecting to database: %s, %o', this.url(), this.options);
      return mongoose.connect(this.url(), this.options);
    }
  }

  public async disconnect () {
    return new Promise<void>((res, rej) => {
      mongoose.disconnect((err) => {
        if (err) {
          rej(err);
        } else {
          logger.info('Database disconnected: %s, %o', this.url(), this.options);
          res();
          this._ready = null;
        }
      });
    });
  }

  public url () {
    return `mongodb://${this.host}:${this.port}/${this.database}`;
  }
}

export const database = new Database();
