import fs from 'fs';
import fse from 'fs-extra';
import path from 'path';

import { Time } from '@topdecked/common-util';

import { logger } from './logger';

export interface HasFilename {
  filename: string;
}

export interface FileDownload<T> {
  downloaded: boolean;
  filePath: string;
  data: T;
}

export interface FileDownloadOpts<T> extends HasFilename {
  download: () => Promise<T>;
  force?: boolean;
  clean?: boolean;
  ttl: number;
}


export class Downloads {
  public static async downloadIfExpired<T extends object> (opts: FileDownloadOpts<T>): Promise<FileDownload<T>> {
    logger.verbose('downloading new data if a more recent version is found');
    const dataFolder = path.join(process.cwd(), 'bulkdata');
    const filePath = path.join(dataFolder, `${opts.filename}`);

    if (!fse.existsSync(dataFolder)) {
      fse.mkdirsSync(dataFolder);
    }

    const modified = (fs.existsSync(filePath) ? fs.statSync(filePath)?.mtime : null) ?? new Date(0);
    if (opts.force || Time.after(Time.ago(opts.ttl * Time.SECOND), modified)) {
      if (opts.clean && fse.existsSync(filePath)) {
        logger.verbose('Cleaning old data file (%s)', filePath);
        fse.removeSync(filePath);
      }

      if (opts.download) {
        logger.verbose('Downloading %s', filePath);
        try {
          const data = await opts.download();
          logger.verbose('Downloaded %s', filePath);
          fs.writeFileSync(filePath, JSON.stringify(data));
          return { downloaded: true, filePath, data };
        } catch (error) {
          logger.error('Failed to download %s %o', filePath, error);
        }
      } else {
        throw new Error('Must provide download function.');
      }
    }

    logger.verbose('Data file up to date %s', filePath);
    return { downloaded: false, filePath, data: null };
  }

  public static readJsonFile<T> (opts: HasFilename): Promise<T> {
    const dataFolder = path.join(process.cwd(), 'bulkdata');
    const filePath = path.join(dataFolder, `${opts.filename}`);

    if (fs.existsSync(filePath)) {
      return JSON.parse(fs.readFileSync(filePath).toString());
    }
    return null;
  }
}
