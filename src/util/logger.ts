
import winston from 'winston';

import { ENV } from './env';

function getErrorLogFilename () {
  return process.env.LOG_ERROR_FILE ? process.env.LOG_ERROR_FILE : 'error.log';
}
function getOutputLogFilename () {
  return process.env.LOG_OUT_FILE ? process.env.LOG_OUT_FILE : 'error.log';
}

export const logger = winston.createLogger({
  level: process.env.LOG_LEVEL,
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.splat(),
    winston.format.json()
  ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log`
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: getErrorLogFilename(), level: 'error' }),
    new winston.transports.File({ filename: getOutputLogFilename() })
  ]
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (!ENV.isProd()) {
  logger.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.timestamp(),
      winston.format.cli(),
      winston.format.splat()
    )
  }));
}
