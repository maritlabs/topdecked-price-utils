import axios, {
  AxiosInstance, AxiosRequestConfig, AxiosResponse, InternalAxiosRequestConfig
} from 'axios';
import axiosThrottle from 'axios-request-throttle';

import { Numbers } from '@topdecked/common-util';

export interface AxiosThrottleConfig {
  maxRequestsPerSecond?: number;
}

export abstract class HttpClient {
  protected readonly instance: AxiosInstance;
  protected interceptRequest: (request: InternalAxiosRequestConfig) => InternalAxiosRequestConfig | Promise<InternalAxiosRequestConfig>;
  protected interceptResponse: (response: AxiosResponse) => AxiosResponse | Promise<AxiosResponse>;

  public constructor(config: string | (AxiosRequestConfig & AxiosThrottleConfig)) {

    const settings: AxiosRequestConfig & AxiosThrottleConfig = typeof config === 'string' ? { baseURL: config } : config ;

    this.instance = axios.create(settings);

    if (Numbers.isNumeric(settings.maxRequestsPerSecond)) {
      axiosThrottle.use(this.instance, { requestsPerSecond: settings.maxRequestsPerSecond });
    }

    this._initializeResponseInterceptor();
  }

  private _initializeResponseInterceptor = () => {
    this.instance.interceptors.request.use(
      this._handleRequest,
      this._handleError,
    );
    this.instance.interceptors.response.use(
      this._handleResponse,
      this._handleError,
    );
  }

  private _handleRequest = async (request: InternalAxiosRequestConfig) => this.interceptRequest ? this.interceptRequest(request) : request;
  private _handleResponse = async (response: AxiosResponse) => this.interceptResponse ? this.interceptResponse(response) : response;
  protected _handleError = (error: any) => Promise.reject(error);


  public base64encode(part: string) {
    return Buffer.from(part).toString('base64');
  }

  public encodeURI(part: string) {
    return encodeURIComponent(part);
  }
}
