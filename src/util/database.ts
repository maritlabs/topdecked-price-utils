import { Dates } from '@topdecked/common-util';

export class DatabaseUtil {
  static ensureDate (date: string | Date): Date {
    if ((date + '')?.length === 10) {
      return Dates.ensureDate((date + '') + 'T04:00:00.000Z');
    }
    return date as Date;
  }
}
