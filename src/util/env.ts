
const TEST = 'test';
const DEV = 'dev';
const PROD = 'prod';

export class Env {
  exitCode: 0 | 1 = 0;

  constructor() {
    console.log('Running in stage: ' + this.getStage().toUpperCase());
  }

  public getStage () {
    let result = DEV;
    const stage = process.env.NODE_ENV;
    if (stage === TEST) {
      result = TEST;
    } else if (stage === DEV) {
      result = DEV;
    } else if (stage === PROD || stage === 'production') {
      result = PROD;
    }
    return result;
  }

  public isDebug () {
    return process.env.DEBUG === 'true';
  }

  public isDev () {
    return this.getStage() === DEV;
  }

  public isProd () {
    return this.getStage() === PROD;
  }

  public isTest () {
    return this.getStage() === TEST;
  }

  public setExitCode (code: 0 | 1) {
    this.exitCode = code;
  }

  public getExitCode () {
    return this.exitCode;
  }
}

export const ENV = new Env();
