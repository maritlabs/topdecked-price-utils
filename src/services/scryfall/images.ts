import Bottleneck from 'bottleneck';
import fs from 'fs';
import { CardSchema } from 'node_modules/@topdecked/common-model/build/main';
import path from 'path';
import request, { RequestPromiseOptions } from 'request-promise';
import { URL } from 'url';

import { Arrays, Numbers } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, SetModel } from '../../model';
import { logger } from '../../util/logger';

const MAX_RETRIES = 3;
const RETRY_DELAY = 5000;
const REQUESTS_PER_SECOND = 20;
const limiter = new Bottleneck({
  minTime: 1000 / REQUESTS_PER_SECOND,
  maxConcurrent: REQUESTS_PER_SECOND,
  resevoir: REQUESTS_PER_SECOND,
  resevoirRefreshAmount: REQUESTS_PER_SECOND,
  resevoirRefreshInterval: REQUESTS_PER_SECOND * 1000
});

limiter.on('failed', async (error, jobInfo) => {
  const id = jobInfo.options.id;
  console.warn(`Job ${id} failed: ${error}`);
  if (jobInfo.retryCount < MAX_RETRIES) {
    console.log(`Retrying job ${id} in ${RETRY_DELAY}ms!`);
    return RETRY_DELAY;
  }
  return null;
});

export interface ImageDownloadOptions {
  code?: string;
  id?: string;
  force?: boolean;
  default?: boolean;
  outdir?: string;
  partition?: 'set';
  sizes?: string;
}

export interface ImageResizeOpts {
  background?: string;
  brightness?: number;
  dpr?: number;
  saturation?: number;
  blur?: number;
  defaultImage?: string;
  height?: number;
  width?: number;
  quality?: number;
  fit?: 'inside' | 'outside' | 'cover' | 'fill' | 'contain';
  compression?: number;
  enlargement?: boolean;
  interlacing?: boolean;
}

export interface EncodingOptions {
  encoding?: BufferEncoding;
}

export const DEFAULT_IMAGE_OUTDIR = 'image-cache';
export const DEFAULT_IMAGE_PARTITION = 'set';

export class ScryfallImageDownloader {

  public async downloadAllImages (opts: ImageDownloadOptions = {}) {
    await this.downloadImagesSets(opts);
    await this.downloadImagesCards(opts);
  }

  public async downloadImagesSets (opts: ImageDownloadOptions = {}) {
    logger.verbose('downloading images for all known sets');
    await database.ready();

    const code = opts.code ?? { $ne: null };
    const query = { code };
    logger.debug('Query cards', query)
    const cursor = SetModel.find(query).lean().batchSize(1000).cursor();

    if (opts?.outdir === DEFAULT_IMAGE_OUTDIR) {
      opts = { ...opts, outdir: DEFAULT_IMAGE_OUTDIR + '/sets' };
    }

    const jobs: Promise<void>[] = [];
    await cursor.eachAsync(async (set) => {

      // https://img.scryfall.com/sets/tpr.svg?1591588800
      const uri = set?.icon_svg_uri;

      if (uri) {
        const filename = set.code.toLowerCase() + '.svg';
        const fullDestPath = this.getImagePath(filename, opts);
        if (!this.fileExists(fullDestPath) || opts.force) {
          const job = async () => {
            try {
              await this.downloadImage(fullDestPath, uri, {
                ...opts,
                encoding: 'utf-8'
              })
            } catch (error) {
              logger.error(`failed to download set icon [${set.code}]: %o`, error.error ?? error);
              if (opts.default) {
                logger.error(`failed to download set icon [${set.code}] -- attempting fallback to [${set.parent_set_code}]: %o`);
                const parent = await SetModel.findOne({ code: set.parent_set_code })
                if (parent?.icon_svg_uri) {
                  await this.downloadImage(fullDestPath, parent?.icon_svg_uri, {
                    ...opts,
                    encoding: 'utf-8'
                  })
                }
              }
            }
          };
          jobs.push(limiter.schedule(job).catch(err => {
            logger.error(`failed to download set icon [${set.code}]: %o`, err.error ?? err);
          }));
        } else {
          logger.verbose(`img exists for set icon ${set.code}: ${filename}`);
        }
      } else {
        logger.error(`invalid uri for set icon [${set.code}]: '${uri}'`);
      }
    });
    return Promise.all(jobs);
  }

  public async downloadImagesCards (opts: ImageDownloadOptions = {}) {
    logger.verbose('downloading images for all known cards');
    await database.ready();
    const id = opts.id ?? { $ne: null };
    const set_code = opts.code ?? { $ne: null };
    const query = { id, set_code };
    const partition = opts.partition;

    const sizes: string[] = (opts.sizes ?? '').split(',');

    const run = async (runOpts: {
      partitionId?: string;
      query?: any;
    } = {}) => {
      const runQuery = { ...query, ...runOpts.query };
      logger.debug('Query cards', runQuery)
      const cursor = CardModel.find(runQuery).lean().batchSize(1000).cursor();
      const cards: CardSchema[] = [];
      await cursor.eachAsync(async (card) => {

        cards.push(card);
        for (let i = 0; i < card.card_faces?.length; i++) {
          const face = card.card_faces[i];
          const imageTypes = Object.keys(face?.image_uris ?? {});
          if (imageTypes?.length) {

            SIZES: for (const type of imageTypes) {
              if (sizes.length && !sizes.includes(type)) {
                continue SIZES;
              }

              // https://img.scryfall.com/cards/small/front/9/0/903d9fde-d7da-4a0e-a337-b63023c6d74b.jpg?1591836744
              const uri = face.image_uris[type];

              if (uri?.length) {
                let filename = new URL(uri).pathname;
                if (filename.startsWith('/cards')) {
                  filename = filename.slice(6);
                }

                const fullDestPath = this.getImagePath(filename, { ...opts, partitionId: runOpts.partitionId });
                if (!this.fileExists(fullDestPath) || opts.force) {
                  const job = async () => {
                    await this.downloadImage(fullDestPath, uri, {
                      ...opts,
                      encoding: 'binary'
                    });
                  }; cards.length
                  jobs.push(limiter.schedule(job).catch(err => {
                    logger.error(`${cards.length} failed to download img for card ${card.id} face [${i}][${type}]: ${uri} -- 'error': %o`, err);
                  }));
                } else {
                  logger.verbose(`${cards.length} img exists for card ${card.id} face [${i}][${type}]: [${card.set_code}] ${card.name} -- 'skipped'`);
                }
              } else {
                logger.error(`${cards.length} invalid img for card ${card.id} face [${i}][${type}]: [${card.set_code}] ${card.name} -- '${uri}'`);
              }
            }
          } else {
            logger.warn(`${cards.length} no images for card ${card.id} face [${i}]: [${card.set_code}] ${card.name}`);
          }
        }
      });

      await Promise.all(jobs);

      if (cards.length && runOpts.partitionId) {
        const partitionDir = path.normalize(path.join(opts.outdir, runOpts.partitionId))
        const indexFile = path.normalize(path.join(partitionDir, `cards.json`));
        if (fs.existsSync(partitionDir)) {
          fs.writeFileSync(indexFile, cards.map(c => JSON.stringify(c)).join('\n'));
        }
      }
    }

    const jobs: Promise<void>[] = [];
    if (partition === 'set') {
      logger.verbose('downloading images partitioned by set');
      const sets: string[] = await SetModel.distinct('code').exec();
      for (const set of sets) {
        await run({ query: { set_code: set }, partitionId: set });
      }
    } else {
      logger.verbose('downloading images no partitions');
      await run();
    }
    return Promise.all(jobs);
  }

  private async downloadImage (fullDestPath: string, uri: string, opts: ImageDownloadOptions & EncodingOptions = {}) {

    fullDestPath = path.normalize(fullDestPath);
    const dirname = path.dirname(fullDestPath);
    if (!fs.existsSync(dirname)) {
      fs.mkdirSync(dirname, { recursive: true });
    }

    if (!this.fileExists(fullDestPath) || opts.force) {
      const encoding = opts.encoding ?? 'binary';
      logger.verbose(`downloading ${encoding}: ${fullDestPath} <-- ${uri}`);
      const options: RequestPromiseOptions = {
        method: 'GET',
        encoding
      };

      const img = await request(this.getResizedURL(uri), options);
      try {
        fs.writeFileSync(fullDestPath, img, {
          encoding
        });
      } catch (err) {
        logger.error('error downloading file: %s', err);
      }
    }
  }

  private getImagePath (filename: string, opts: { outdir?: string, partitionId?: string } = {}) {
    return path.normalize(path.join(...Arrays.nonNullValues([opts.outdir, opts.partitionId, filename])));
  }

  private fileExists (filename: string) {
    return fs.existsSync(filename) && fs.statSync(filename).size > 0;
  }

  private readonly imageResizeURL = 'https://transmute.topdecked.com/?url=';
  public getResizedURL (url: string, opts: ImageResizeOpts = {}) {
    if (!url) {
      return url;
    }

    let result = url;
    while (result.startsWith(this.imageResizeURL)) {
      const optsIndex = result.indexOf('&');
      result = decodeURIComponent(result.slice(this.imageResizeURL.length, (optsIndex >= 0 ? optsIndex : result.length)));
    }

    let startIndex: number;
    while ((startIndex = result.indexOf('http')) > 0) {
      result = result.slice(startIndex);
    }
    result = `${this.imageResizeURL}${encodeURIComponent(result)}`;

    const compression = 6;
    const quality = opts.quality ?? 95;
    const fit = opts.fit ?? 'cover';

    if (quality) { result += `&q=${quality}`; }
    if (Numbers.isInteger(compression)) { result += `&l=${compression}`; }

    if (opts?.background) { result += `&bg=${opts.background}`; }
    if (opts?.blur) { result += `&blur=${opts.blur}`; }
    if (opts?.brightness) { result += `&mod=${opts.brightness}`; }
    if (opts?.saturation) { result += `&sat=${opts.saturation}`; }
    if (opts?.height) { result += `&h=${opts.height}`; }
    if (opts?.width) { result += `&w=${opts.width}`; }
    if (fit) { result += `&fit=${fit}`; }
    if (fit && opts?.enlargement === false) { result += `&we`; }
    if (opts.interlacing) { result += `&li`; }
    if (opts?.dpr) { result += `&dpr=${opts.dpr}`; }
    if (opts.defaultImage) { result += `&default=${opts.defaultImage}`; }

    return result;
  }

}
