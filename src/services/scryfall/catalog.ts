import { Document } from 'mongoose';

import { CardPurchaseURIsSchema, CardSchema } from '@topdecked/common-model';

import { database } from '../../config/database';
import { CardModel, SetModel } from '../../model';
import { logger } from '../../util/logger';
import { CatalogService, UpdateMetadataOpts } from '../api/metadata.api';
import { TcgPlayerCatalog } from '../tcgplayer/catalog';
import { ScryfallPriceService } from './price';
import { ScryfallClient } from './scryfall.client';

const scryfall = new ScryfallClient();

export class ScryfallCatalog implements CatalogService {

  private priceService: ScryfallPriceService;
  private tcgplayer = new TcgPlayerCatalog();

  constructor() {
    this.priceService = new ScryfallPriceService();
  }

  public async updateAllMetadata (opts: UpdateMetadataOpts = {}) {
    logger.info('updating metadata for all cards');
    await database.ready();

    const sets = await SetModel.find({ scryfall_id: { $ne: null } });
    if (sets && sets.length > 0) {
      for (const set of sets) {
        if (!opts.setCode || opts.setCode === set.code) {
          const setCards = await CardModel.find({
            ...(opts.cardId ? { id: opts.cardId } : {}),
            ...(opts.force ? {} : { 'metadata.scryfallLinksInitialized': { $ne: true } }),
            deprecated: { $ne: true },
            set_code: set.code,
          }) as CardSchema[];
          if (setCards && setCards.length > 0 || opts.force) {
            logger.verbose('updating metadata for %s cards in set [%s]: %s', setCards.length, set.code, set.name);
            await this.updateMetadataBySet(set.code, (opts.cardId ? setCards : [] as any[]), opts);
          } else {
            logger.verbose('no cards to update in set [%s]: %s', set.code, set.name);
          }
        }
      }

      if (!opts.setCode || !opts.cardId) {
        logger.info('checking for cards still missing metadata');
        const remainingCards = await CardModel.find({
          deprecated: { $ne: true },
          'metadata.scryfallLinksInitialized': { $ne: true }
        }, { strict: false });
        if (remainingCards && remainingCards.length > 0) {
          for (const card of remainingCards) {
            await this.updateMetadataByCard(card, opts);
          }
        } else {
          logger.info('no cards requiring metdata updates');
        }
      }
    } else {
      logger.error('no sets found in database');
    }
  }

  public async updateMetadataBySet (setCode: string, cards: (CardSchema & Document)[], opts: UpdateMetadataOpts) {
    try {
      logger.verbose('Fetching latest metadata from Scryfall for set %s', setCode);
      const scryfallCards = await scryfall.getCardsBySet(setCode);
      logger.info('Updating metadata for set %s', setCode);
      if (opts.cardId) {
        logger.info('Updating metadata for card ID %s', opts.cardId);
      }
      if (!opts.cardId) {
        await this.saveMetadata(scryfallCards, opts);
        logger.info('Verifying set metadata...');
        cards = await CardModel.find({
          ...(opts.cardId ? { id: opts.cardId } : {}),
          ...(opts.force ? {} : { 'metadata.scryfallLinksInitialized': { $ne: true } }),
          deprecated: { $ne: true },
          set_code: setCode,
        }) as (CardSchema & Document)[];
      }

      if (cards && cards.length > 0) {
        logger.warn(`Metadata not complete for set ${setCode}. ${cards.length} cards are still incomplete`);
        for (const card of cards) {
          await this.updateMetadataByCard(card, opts);
        }

        const cardsSanityCheck = await CardModel.find({
          ...(opts.cardId ? { id: opts.cardId } : {}),
          deprecated: { $ne: true },
          set_code: setCode,
          'metadata.scryfallLinksInitialized': { $ne: true }
        });

        if (cardsSanityCheck && cardsSanityCheck.length > 0) {
          logger.warn(`Metadata not complete for set ${setCode}. ${cards.length} cards are still incomplete: %o`, cardsSanityCheck);
        }

      } else {
        logger.info('metadata for set %s updated successfully', setCode);
      }

    } catch (err) {
      logger.error('error getting metadata for set(%s): %o', setCode, err);
    }
  }


  private async updateMetadataByCard (card: CardSchema, opts: UpdateMetadataOpts) {
    try {
      logger.verbose('Fetching latest metadata from Scryfall for card name: %s', card.name);
      const scryfallCard = await scryfall.getCardById(card.id);
      logger.info('Updating metadata for card \'%s\', \'%s\', \'%s\'', card.name, card.set_code, card.id);
      await this.saveMetadata([scryfallCard], opts);
      logger.info('metadata for card \'%s\' updated successfully', card.name);
    } catch (err) {
      logger.error('error getting metadata for card(%s): %o', card.name, err);
    }
  }

  private async saveMetadata (scryfallCards: any[], opts: UpdateMetadataOpts) {
    if (scryfallCards && scryfallCards.length > 0) {
      await database.ready();
      logger.debug('%o', scryfallCards);
      scryfallCards = scryfallCards.filter((c) => {
        return c && c.purchase_uris;
      });

      logger.verbose('Fetching latest metadata from Scryfall for %s cards', scryfallCards.length);
      for (const scryfallCard of scryfallCards) {
        const purchaseURIs = this.getAffiliateURIs(scryfallCard.purchase_uris);
        const relatedURIs = this.getAffiliateURIs(scryfallCard.related_uris);
        const card = await CardModel.findOne({ scryfall_id: scryfallCard.id }).select('+metadata');
        if (card) {
          for (const source of Object.keys(purchaseURIs)) {
            card.purchase_uris[source] = purchaseURIs[source];
            logger.debug('Assigning %s => %s:  %s', source, purchaseURIs[source], card.purchase_uris[source]);
          }
          for (const source of Object.keys(relatedURIs)) {
            card.related_uris[source] = relatedURIs[source];
            logger.debug('Assigning %s => %s:  %s', source, relatedURIs[source], card.related_uris[source]);
          }

          if (scryfallCard.prices) {
            await this.priceService.updatePricesForCard(card, scryfallCard, { ...opts, modifyOnly: true });
          }

          if (!card.metadata.scryfallLinksInitialized) {
            card.set('metadata.scryfallLinksInitialized', true);
          }


          if (card.card_faces.length > 0 && scryfallCard.image_uris && Object.keys(scryfallCard.image_uris).length > 0) {
            card.card_faces[0].image_uris = scryfallCard.image_uris;
          } else if (card.card_faces.length > 1) {
            for (let i = 0; i < card.card_faces.length; i++) {
              if (scryfallCard.card_faces
                && scryfallCard.card_faces[i]
                && scryfallCard.card_faces[i].image_uris
                && Object.keys(scryfallCard.card_faces[i].image_uris).length > 0) {
                card.card_faces[i].image_uris = scryfallCard.card_faces[i].image_uris;
              }
            }
          }

          await card.save();
          logger.verbose(`Card metadata updated for scryfall_id: ${scryfallCard.id} ${scryfallCard.name}`);
        } else {
          logger.warn(`Card in database not found for scryfall_id: ${scryfallCard.id} ${scryfallCard.name}`);
        }
      }
    }
    return scryfallCards;
  }

  public async cleanupDeprecatedMetadata (opts: UpdateMetadataOpts = {}) {
    await database.ready();
  }


  public getAffiliateURIs (uris: any): CardPurchaseURIsSchema {
    if (uris) {
      const result = JSON.parse(JSON.stringify(uris));
      for (const source of Object.keys(result)) {
        const uri = result[source];
        const rebrand = (link: string) => {
          // tslint:disable-next-line:prefer-conditional-expression
          if (source.indexOf('tcgplayer') > -1) {
            return link.replace(/scryfall/ig, 'TopDeckedMTG');
          } else {
            return link.replace(/scryfall/ig, 'AFFILIATECODE');
          }
        }

        if (uri) {
          if (source === 'cardhoarder') {
            if (uri.match(/.*(%5B)?search(%5D)?=.*/i)) {
              result[source] = uri
                .replace(/data(%5B|\[)?search(%5D|\])?=/i, 'cardname=')
            }
            result[source] = uri
              .replace(/(scryfall|AFFILIATECODE)/ig, 'topdecked');
          }
          if (uri.includes('pxf.io') || uri.includes('partner.tcgplayer.com')) {
            if (uri.includes('tcgplayer.pxf.io') || uri.includes('partner.tcgplayer.com')) {
              const extracted = rebrand(decodeURIComponent(uri.replace(/.*(?!tcgplayer.pxf.io|partner.tcgplayer.com)\/c\/.*u=([^&]+)$/i, '$1')));
              const sanitized = extracted.replace(/([?&])partner=[^?&]+/i, '$1');
              const branded = this.tcgplayer.createAffiliateLink(sanitized);
              result[source] = branded;
            } else {
              throw new Error("Unexpected affiliate URL structure: " + uri);
            }
          }

          result[source] = rebrand(result[source]);
          logger.debug(`FINAL URI: ${source} ${result[source]}`);
        }
      }
      return result;
    }
    return {};
  }
}
