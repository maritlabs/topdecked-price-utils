import { Document } from 'mongoose';

import { CardSchema } from '@topdecked/common-model';
import { Arrays, Numbers, Time } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, SetModel } from '../../model';
import { logger } from '../../util/logger';
import { cardRequiresPriceUpdate, PriceService, UpdatePricesOptions } from '../interfaces';
import { ScryfallClient } from './scryfall.client';

const scryfall = new ScryfallClient();

export class ScryfallPriceService implements PriceService {

  public async updatePrices (opts: UpdatePricesOptions) {
    await database.ready();
    if (opts.cardId) {
      return this.updatePricesById(opts.cardId, opts);
    } else if (opts.setCode && opts.cardName) {
      return this.updatePricesByCardAndSet(opts.setCode, opts.cardName, opts);
    } else if (opts.cardName) {
      return this.updatePricesByCard(opts.cardName, opts);
    } else if (opts.setCode) {
      return this.updatePricesBySet(opts.setCode, opts);
    } else {
      return this.updateAllSets(opts);
    }
  }

  private async updatePricesById (cardId: string, opts: UpdatePricesOptions) {
    try {
      logger.verbose(`${cardId}: Querying prices'`);
      const card = await scryfall.getCardById(cardId);
      logger.info(`${cardId}: Updating prices`);
      await this.savePrices(card, opts);
      logger.info(`${cardId}: Prices updated`);
    } catch (err) {
      logger.error(`${cardId}: Error updating prices: %o`, err);
    }
  }

  private async updatePricesByCardAndSet (setCode: string, cardName: string, opts: UpdatePricesOptions) {
    try {
      logger.verbose(`${setCode}, ${cardName}: Fetching latest prices from Scryfall`);
      const cards = await scryfall.getCardsByNameAndSet(cardName, setCode);
      logger.info(`${setCode}, ${cardName}: Updating prices`);
      await this.savePrices(cards, opts);
      logger.info(`${setCode}, ${cardName}: Prices updated successfully`);
    } catch (err) {
      logger.error(`${setCode}, ${cardName}: Error updating prices: %o`, err);
    }
  }

  private async updatePricesByCard (cardName: string, opts: UpdatePricesOptions) {
    try {
      logger.verbose(`${cardName} Fetching latest prices from Scryfall`);
      const cards = await scryfall.getCardsByName(cardName);
      logger.info(`${cardName} Updating prices`);
      await this.savePrices(cards, opts);
      logger.info(`${cardName} Prices updated successfully`);
    } catch (err) {
      logger.error(`${cardName}: Error updating prices: %o`, err);
    }
  }

  private async updatePricesBySet (setCode: string, opts: UpdatePricesOptions) {
    try {
      logger.verbose(`${setCode}: Fetching latest prices from Scryfall`);
      const cards = await scryfall.getCardsBySet(setCode);
      logger.info(`${setCode}: Updating prices`);
      await this.savePrices(cards, opts);
      logger.info(`${setCode}: Prices updated successfully`);
    } catch (err) {
      logger.error(`${setCode}: Error updating prices: %o`, err);
    }
  }

  public async updateAllSets (opts: UpdatePricesOptions) {
    logger.verbose(`ALL SETS: Fetching latest prices from Scryfall`);
    await database.ready();
    const sets = await SetModel.find({ scryfall_id: { $ne: null } });
    if (sets && sets.length > 0) {
      for (const set of sets) {
        logger.info(`${set.code}: Updating prices for ${set.name}`);
        await this.updatePricesBySet(set.code, opts);
      }
    } else {
      logger.error('ALL SETS: No sets found in database');
    }
  }

  public async savePrices (scryfallCards: any, opts: UpdatePricesOptions) {
    logger.debug('scryfall cards: %o', scryfallCards);
    if (scryfallCards) {
      await database.ready();
      const count = 0;

      scryfallCards = Arrays.arrayify(scryfallCards).filter((c) => {
        const p = c.prices;
        return c && p && (p.usd || p.eur || p.tix || p.usd_foil || p.eur_foil || p.tix_foil);
      });

      for (const c of scryfallCards) {
        logger.debug('%s, %o', count, c);
        const card = await CardModel.findOne({ scryfall_id: c.id });
        if (card) {
          await this.updatePricesForCard(card, c, opts);
        } else {
          logger.error(`${c.id}: Card in database not found by scryfall_id. Name: ${c.name}`);
        }
      }
    }
    return scryfallCards;
  }

  public async updatePricesForCard (card: CardSchema & Document, scryfallCard: any, opts: UpdatePricesOptions) {
    if (opts.force || cardRequiresPriceUpdate('scryfall', card)) {
      if (!card.prices.scryfall) {
        card.prices.scryfall = {};
        card.prices.scryfall.created = Time.now();
      }

      if (scryfallCard && scryfallCard.prices) {
        const p = scryfallCard.prices;
        logger.debug(`${card.id}: ${card.name} scryfall prices, %o`, p);
        card.prices.scryfall.eur = Numbers.ensureFloat(p.eur, null);
        card.prices.scryfall.eur_foil = Numbers.ensureFloat(p.eur_foil, null);
        card.prices.scryfall.eur_etched = Numbers.ensureFloat(p.eur_etched, null);

        card.prices.scryfall.usd = Numbers.ensureFloat(p.usd, null);
        card.prices.scryfall.usd_etched = Numbers.ensureFloat(p.usd_etched, null);
        card.prices.scryfall.usd_foil = Numbers.ensureFloat(p.usd_foil, null);

        card.prices.scryfall.tix = Numbers.ensureFloat(p.tix, null);
        card.prices.scryfall.tix_foil = Numbers.ensureFloat(p.tix_foil, null);
      }

      card.prices.scryfall.updated = Time.now();

      if (!opts.modifyOnly) {
        logger.verbose(`${card.id}: Prices updated for "${card.name}": %o`, card.prices.scryfall);
        return card.save();
      }
    } else {
      logger.info(`${card.id}: Prices already current`);
    }
    return card;
  }
}
