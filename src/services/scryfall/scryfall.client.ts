import { InternalAxiosRequestConfig } from 'axios';
import * as scryfall from 'scryfall-api';

import { Set } from '@topdecked/common-model-api';
import { Objects, Time } from '@topdecked/common-util';

import { DatabaseUtil } from '../../util/database';
import { ENV } from '../../util/env';
import { HttpClient } from '../../util/http-client.service';
import { logger } from '../../util/logger';

const DEBUG_SCRYFALL = false && !ENV.isProd();
const KEAP_API_QUOTA_PER_MINUTE = 5000;

function stringifyParams (params: Record<string, string>): string {
  return Object.keys(params).reduce((accum, param) => {
    if (accum) {
      accum += "&";
    }

    return (accum += `${param}=${params[param]}`);
  }, "");
}

export class ScryfallClient extends HttpClient {
  public static readonly COLD_DAYS_MULTIPLIER = 180;

  private static _ready: Promise<void> = null;

  constructor() {
    super({
      baseURL: 'https://api.scryfall.com',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
      maxRequestsPerSecond: (KEAP_API_QUOTA_PER_MINUTE / 60) - 10,
      timeout: Time.SECOND * 20
    });

    this.interceptRequest = async (request: InternalAxiosRequestConfig) => {
      request.headers['User-Agent'] = 'Custom Client';
      return Promise.resolve(request);
    };
  }

  public ready () {
    if (!ScryfallClient._ready) {
      ScryfallClient._ready = this.init().then(async () => {
        if (await this.isEnabled()) {
        }
      }).catch(err => {
        ScryfallClient._ready = null;
        throw err;
      });
    }
    return ScryfallClient._ready;
  }

  async init () {

  }

  async isEnabled () {
    return DEBUG_SCRYFALL || ENV.isProd();
  }

  private getUrl (
    endpoint: string,
    query?: Record<string, string>,
  ): string {
    let url = this.instance.getUri() + endpoint;

    if (query) {
      const queryParams = stringifyParams(query);

      if (url.indexOf("?") > -1) {
        url += "&";
      } else {
        url += "?";
      }

      url += queryParams;
    }

    return url;
  }

  async get<T> (uri: string, query?: Record<string, string>,): Promise<T> {
    const response = await this.instance.get(uri, query);
    return response.data;
  }

  async getSets (opts: { setCode?: string } = {}): Promise<Set[]> {

    const response: any[] = opts.setCode ? [await scryfall.Sets.byCode(opts.setCode)] : await scryfall.Sets.all();
    const result: Set[] = [];

    for (const record of response) {
      if (record.object === 'set' && (!opts.setCode || record.code === opts.setCode)) {
        logger.verbose(`Processing set: [${record.code}]\t"${record.name}"`);
        const parsed = JSON.parse(JSON.stringify(record));
        const set = Objects.renameKeys(parsed, {
          id: 'scryfall_id',
          uri: 'scryfall_api_uri',
          search_uri: 'scryfall_search_uri',
          released_at: 'released',
          digital: 'digital_only',
          set_type: 'type',
          tcgplayer_id: 'tcgplayer_group_id'
        });
        delete set._id;
        delete set.object;
        set.released = DatabaseUtil.ensureDate(set.released);
        result.push(set);
      } else {
        logger.warn('Ignored invalid record %o', record);
      }
    }

    return result;
  }

  public async getCardById (id: string): Promise<any> {
    logger.verbose('Fetching cards for id \'%s\'', id);
    const card = await scryfall.Cards.byId(id);
    logger.debug('%o', card);
    return card;
  }

  public async getCardsBySet (setCode: string): Promise<any[]> {
    logger.verbose('Fetching cards for set %s', setCode);
    const page = await scryfall.Cards.search(`set:${setCode}`, {
      include_extras: true,
      include_variations: true,
      unique: 'prints'
    });
    logger.verbose('Fetched and returned %d cards', page.count);
    return page.all(); // a list of cards for the set
  }

  public async getCardsByName (name: string): Promise<any[]> {
    logger.verbose('Fetching cards named \'%s\'', name);
    const page = await scryfall.Cards.search('name:' + name, {
      unique: 'prints'
    });
    logger.debug('%o', page);
    return page.all();
  }

  public async getCardsByNameAndSet (name: string, setCode: string): Promise<any[]> {
    logger.verbose('Fetching card: [%s]%s', setCode, name);
    const page = await scryfall.Cards.search('name:' + name + ' set:' + setCode, {
      unique: 'prints'
    });
    logger.debug('%o', page);
    return page.all();
  }
}
