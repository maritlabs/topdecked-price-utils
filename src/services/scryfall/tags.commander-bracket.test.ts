import {
  TAG_COMMANDER_BRACKET_EXTRA_TURNS, TAG_COMMANDER_BRACKET_GAME_CHANGER,
  TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL, TAG_COMMANDER_BRACKET_TUTORS
} from '@topdecked/common-model-api';

import calculateCommanderBracketTags from './tags.commander-bracket';

describe('calculateCommanderBracketTags', () => {
  it('should add TAG_COMMANDER_BRACKET_EXTRA_TURNS tag', () => {
    const card = { card_faces: [{ oracle_text: 'Take an extra turn after this one.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_EXTRA_TURNS);
  });

  it('should add TAG_COMMANDER_BRACKET_EXTRA_TURNS tag (target player)', () => {
    const card = { card_faces: [{ oracle_text: 'Target player takes an extra turn after this one.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_EXTRA_TURNS);
  });

  it('should add TAG_COMMANDER_BRACKET_EXTRA_TURNS tag (any player)', () => {
    const card = { card_faces: [{ oracle_text: 'player takes an extra turn after this one.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_EXTRA_TURNS);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy all lands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (boardwipe effect)', () => {
    const card = { card_faces: [{ oracle_text: 'This spell costs 3 less to cast if there are ten or more nonland permanents on the battlefield.\nDestroy all nonland permanents.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (eye of singularity effect)', () => {
    const card = { card_faces: [{ oracle_text: 'When this enchantment enters, destroy each permanent with the same name as another permanent, except for basic lands. They can\'t be regenerated.\nWhenever a permanent other than a basic land enters, destroy all other permanents with that name. They can\'t be regenerated.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (alpine moon effect)', () => {
    const card = { card_faces: [{ oracle_text: 'As this enchantment enters, choose a nonbasic land card name.\nLands your opponents control with the chosen name lose all land types and abilities, and they gain "Tap: Add one mana of any color.".' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (toxicrene effect)', () => {
    const card = { card_faces: [{ oracle_text: 'As this enchantment enters, choose a nonbasic land card name.\nLands your opponents control with the chosen name lose all land types and abilities, and they gain "Tap: Add one mana of any color.".' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (blood moon effect)', () => {
    const card = { card_faces: [{ oracle_text: 'Nonbasic lands are mountains.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy four lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy four lands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy up to four lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy up to four lands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy four lands target player)', () => {
    const card = { card_faces: [{ oracle_text: 'You destroy four lands you control, then target opponent destroys four lands they control. Then Burning of Xinye deals 4 damage to each creature.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all other permanents)', () => {
    const card = { card_faces: [{ oracle_text: 'Exile Urza\'s Sylex: Each player chooses six lands they control. Destroy all other permanents. Activate only as a sorcery.\nWhen Urza\'s Sylex is put into exile from the battlefield, you may pay {3}. If you do, search your library for a planeswalker card, reveal it, put it into your hand, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all permanents)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy all permanents.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all permanents worldslayer)', () => {
    const card = { card_faces: [{ oracle_text: 'Whenever equipped creature deals combat damage to a player, destroy all permanents other than this Equipment.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all permanents same name)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy each permanent with the same name as another permanent.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (exile all lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Exile all lands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Sacrifice lands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice lands for each)', () => {
    const card = { card_faces: [{ oracle_text: 'Sacrifices lands of their choice for each.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice lands for each attacking creature)', () => {
    const card = { card_faces: [{ oracle_text: 'Green creatures can\'t attack unless their controller sacrifices a land of their choice for each green creature they control that\'s attacking. (This cost is paid as attackers are declared.).' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice lands for each target player)', () => {
    const card = { card_faces: [{ oracle_text: 'Target player sacrifices all lands they control.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (return all lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Return all lands to their owners\' hands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (return four lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Return four lands to their owners\' hands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (return up to four lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Return up to four lands to their owners\' hands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (tap all lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Tap all lands. They don\'t untap.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (nonland don\'t untap)', () => {
    const card = { card_faces: [{ oracle_text: 'Nonland permanents don\'t untap during their controllers\' untap steps.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (tap all lands that player)', () => {
    const card = { card_faces: [{ oracle_text: 'Counter target spell unless its controller pays {C}. If that player doesn\'t, they tap all lands with mana abilities they control and lose all unspent mana.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (tap four lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Tap four lands. They don\'t untap.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (tap up to four lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Tap up to four lands. They don\'t untap.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (lands don\'t untap)', () => {
    const card = { card_faces: [{ oracle_text: 'Lands don\'t untap.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (can\'t untap more than one land)', () => {
    const card = { card_faces: [{ oracle_text: 'Can\'t untap more than one land.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (you can\'t untap more than one land)', () => {
    const card = { card_faces: [{ oracle_text: 'You can\'t untap more than one land.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (can\'t untap more than one basic land)', () => {
    const card = { card_faces: [{ oracle_text: 'Can\'t untap more than one basic land.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (can\'t untap more than one nonbasic land)', () => {
    const card = { card_faces: [{ oracle_text: 'Can\'t untap more than one nonbasic land.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (land lose all abilities)', () => {
    const card = { card_faces: [{ oracle_text: 'Land lose all abilities.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice annihilator)', () => {
    const card = { card_faces: [{ oracle_text: 'Annihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents of their choice.)' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice of your choice)', () => {
    const card = { card_faces: [{ oracle_text: 'Each player sacrifices a creature of their choice. If you sacrificed a creature this way, you may return another permanent card from your graveyard to the battlefield.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (land are mountains)', () => {
    const card = { card_faces: [{ oracle_text: 'Land are mountains.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_TUTORS tag', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should add TAG_COMMANDER_BRACKET_TUTORS tag (search)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a card. Shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should add TAG_COMMANDER_BRACKET_TUTORS tag (command tower)', () => {
    const card = { card_faces: [{ oracle_text: 'When this creature enters, search your library and/or graveyard for a card named Command Tower, reveal it, and put it into your hand. If you search your library this way, shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should add TAG_COMMANDER_BRACKET_TUTORS tag (searches)', () => {
    const card = { card_faces: [{ oracle_text: 'Searches your library for a card. Shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should add TAG_COMMANDER_BRACKET_TUTORS tag (library)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a card and put it into your hand. Shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (shuffle)', () => {
    const card = { card_faces: [{ oracle_text: 'Shuffle your library.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (land)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a land card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (mass land)', () => {
    const card = { card_faces: [{ oracle_text: 'Tempting offer — Search your library for a land card and put it onto the battlefield. Each opponent may search their library for a land card and put it onto the battlefield. For each opponent who searches a library this way, search your library for a land card and put it onto the battlefield. Then each player who searched a library this way shuffles.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should add TAG_COMMANDER_BRACKET_TUTORS tag (land and card)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a basic land card and put it onto the battlefield tapped. Search your library and graveyard for a card named Nissa, Nature\'s Artisan, reveal it, put it into your hand, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (mass land)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy all nonbasic lands. For each land destroyed this way, its controller may search their library for a basic land card and put it onto the battlefield. Then each player who searched their library this way shuffles.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (haunting echoes)', () => {
    const card = { card_faces: [{ oracle_text: 'Exile all cards from target player\'s graveyard other than basic land cards. For each card exiled this way, search that player\'s library for all cards with the same name as that card and exile them. Then that player shuffles.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (plains)', () => {
    const card = { card_faces: [{ oracle_text: 'When Claim Jumper enters the battlefield, if an opponent controls more lands than you, you may search your library for a Plains card and put it onto the battlefield tapped. Then if an opponent controls more lands than you, repeat this process once. If you search your library this way, shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (desert)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a desert card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (gate)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a gate card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (mountain)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a mountain card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (forest)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a forest card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (island)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a island card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKETTUTORS tag (plains)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a plains card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (swamp)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a swamp card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag (waste)', () => {
    const card = { card_faces: [{ oracle_text: 'Search your library for a wastes? card, then shuffle.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should add TAG_COMMANDER_BRACKET_GAME_CHANGER tag', () => {
    const card = { card_faces: [{ oracle_text: 'Some text.' }], name: 'Test Card', game_changer: true };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_GAME_CHANGER);
  });

  it('should not add any tags', () => {
    const card = { card_faces: [{ oracle_text: 'Some text.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags.length).toBe(0);
  });

  it('should handle empty oracle_text', () => {
    const card = { card_faces: [{ oracle_text: '' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags.length).toBe(0);
  });

  it('should handle null oracle_text', () => {
    const card = { card_faces: [{ oracle_text: null }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags.length).toBe(0);
  });

  it('should not add TAG_COMMANDER_BRACKET_EXTRA_TURNS tag', () => {
    const card = { card_faces: [{ oracle_text: 'Some text.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_EXTRA_TURNS);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag', () => {
    const card = { card_faces: [{ oracle_text: 'Some text.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all lands backside)', () => {
    const card = {
      card_faces: [
        { "oracle_text": "Destroy target land you control and target land you don't control.", },
        { "oracle_text": "Destroy all lands.", }
      ], name: 'Test Card'
    };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy attached)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy all creatures and all permanents attached to creatures.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy nonland)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy all nonland permanents.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice permanents with colors)', () => {
    const card = { card_faces: [{ oracle_text: 'Sacrifices all permanents they control that are one or more colors.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice permanents with colors)', () => {
    const card = { card_faces: [{ oracle_text: 'Sacrifices four permanents that are one or more colors.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (combat damage trigger)', () => {
    const card = { card_faces: [{ oracle_text: 'Whenever this creature deals combat damage to player, that player sacrifices a permanent of their choice.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (combat damage trigger four)', () => {
    const card = { card_faces: [{ oracle_text: 'Whenever this creature deals combat damage to player, that player sacrifices four permanents of their choice.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (dies trigger)', () => {
    const card = { card_faces: [{ oracle_text: 'When this creature dies, each player sacrifices a land of their choice.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (dies trigger all)', () => {
    const card = { card_faces: [{ oracle_text: 'When this creature dies, each player sacrifices all lands of their choice.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (annihilator)', () => {
    const card = { card_faces: [{ oracle_text: 'Whenever this creature attacks, defending player sacrifices two permanents of their choice.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice nonland)', () => {
    const card = { card_faces: [{ oracle_text: 'Sacrifice nonland permanents.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice permananent each)', () => {
    const card = { card_faces: [{ oracle_text: 'Each player sacrifices a creature or planeswalker of their choice. If you sacrificed a permanent this way, you may return another permanent card from your graveyard to your hand.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice a permanent spell)', () => {
    const card = { card_faces: [{ oracle_text: 'Each player sacrifices a permanent.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice a permanent)', () => {
    const card = { card_faces: [{ oracle_text: 'When this creature dies, each player sacrifices a permanent.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice a land end step)', () => {
    const card = { card_faces: [{ oracle_text: 'At the beginning of each end step, each player who tapped a land for mana this turn sacrifices a land of their choice. This permanent deals 2 damage to each player who sacrificed a plains this way.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (sacrifice a land upkeep)', () => {
    const card = { card_faces: [{ oracle_text: 'At the beginning of each upkeep, that player sacrifices a nonbasic land of their choice.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (return except lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Return all permanents to their owners\' hands except for lands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (return except giants and lands)', () => {
    const card = { card_faces: [{ oracle_text: 'Return all permanents to their owners\' hands except for giands and lands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (return nonland)', () => {
    const card = { card_faces: [{ oracle_text: 'Return all nonland permanents to their owners\' hands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (tap nonland)', () => {
    const card = { card_faces: [{ oracle_text: 'Tap all nonland permanents. They don\'t untap.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (untap artifacts)', () => {
    const card = { card_faces: [{ oracle_text: 'Players can\'t untap more than one artifact during their untap steps.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (lands you control)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy all creatures. Lands you control don\'t untap during your next untap step.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (nonland don\'t untap)', () => {
    const card = { card_faces: [{ oracle_text: 'Nonland permanents don\'t untap.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (can\'t untap more than one nonland)', () => {
    const card = { card_faces: [{ oracle_text: 'Can\'t untap more than one nonland permanent.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (nonland lose all abilities)', () => {
    const card = { card_faces: [{ oracle_text: 'Nonland permanents lose all abilities.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (nonland are mountains)', () => {
    const card = { card_faces: [{ oracle_text: 'Nonland permanents are mountains.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all wireflies)', () => {
    const card = { card_faces: [{ oracle_text: 'Flip a coin. If you win the flip, create a 2/2 colorless Insect artifact creature token with flying named Wirefly. If you lose the flip, destroy all permanents named Wirefly.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (destroy all color)', () => {
    const card = { card_faces: [{ oracle_text: 'Destroy all white permanents.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL tag (return all cmc x or less)', () => {
    const card = { card_faces: [{ oracle_text: 'Return all nonland permanents with mana value X or less to their owners\' hands.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
  });

  it('should not add TAG_COMMANDER_BRACKET_TUTORS tag', () => {
    const card = { card_faces: [{ oracle_text: 'Some text.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_TUTORS);
  });

  it('should not add TAG_COMMANDER_BRACKET_GAME_CHANGER tag', () => {
    const card = { card_faces: [{ oracle_text: 'Some text.' }], name: 'Test Card' };
    const tags = calculateCommanderBracketTags(card);
    expect(tags).not.toContainEqual(TAG_COMMANDER_BRACKET_GAME_CHANGER);
  });

});
