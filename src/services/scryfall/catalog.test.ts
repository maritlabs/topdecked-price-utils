import { ScryfallCatalog } from './catalog';

test('getPurchaseURIs can extract URIs', async () => {
  const cat = new ScryfallCatalog();
  const subject = {
    tcgplayer:
      'https://shop.tcgplayer.com/productcatalog/product/show' +
      '?ProductName=Wurm&partner=Scryfall&utm_campaign=affiliate&utm_medium=scryfall&utm_source=scryfall',
    cardmarket:
      'https://www.cardmarket.com/en/Magic/Products/Search?referrer=scryfall&searchString=Wurm',
    cardhoarder:
      'https://www.cardhoarder.com/cards' +
      '?affiliate_id=scryfall&data%5Bsearch%5D=Wurm&ref=card-profile&utm_campaign=affiliate&utm_medium=card&utm_source=scryfall'
  } as any;
  const result = cat.getAffiliateURIs(subject);
  expect(result.tcgplayer).toBeTruthy();
  expect(result.cardmarket).toBeTruthy();
  expect(result.cardhoarder).toBeTruthy();
  expect(result.cardkingdom).toBeFalsy();
});
