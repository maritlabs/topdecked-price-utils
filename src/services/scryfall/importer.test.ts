// import { CardRulingSchema } from '@topdecked/common-model';
// import {
//   TAG_COMMANDER_BRACKET_EXTRA_TURNS, TAG_COMMANDER_BRACKET_GAME_CHANGER,
//   TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL, TAG_COMMANDER_BRACKET_TUTORS
// } from '@topdecked/common-model-api';

import { CardRulingSchema } from '@topdecked/common-model';
import {
  TAG_COMMANDER_BRACKET_EXTRA_TURNS, TAG_COMMANDER_BRACKET_GAME_CHANGER,
  TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL, TAG_COMMANDER_BRACKET_TUTORS
} from '@topdecked/common-model-api';

import { database } from '../../config/database';
import { CardModel, SetModel } from '../../model';
import { logger } from '../../util/logger';
import { ScryfallCatalog } from './catalog';
import { CardImportOptions, ScryfallImporter } from './importer';
import { ScryfallClient } from './scryfall.client';

beforeAll(async () => {
  await database.ready();
  await CardModel.deleteMany({}).exec();
  await SetModel.deleteMany({}).exec();
});


const importCardList = async (importer: ScryfallImporter, cards: any[], rulings: { [id: string]: CardRulingSchema[] } = {}, opts: CardImportOptions = {}) => {
  let cardCount = 0;
  let objectCount = 0;
  let processedCount = 0;
  for (const card of cards) {
    logger.verbose(`${cardCount}\tStaging card: ${card.set_name} ${card.lang}, ${card.name}`);
    const result = await importer.importCard(card, rulings, opts);
    cardCount += result.cards;
    objectCount += result.objects;
    processedCount += result.processed;
    if (objectCount % 1000 === 0) {
      logger.verbose('objectCount (%d): cardCount (%d): processedCardCount (%d)', objectCount, cardCount, processedCount);
    }
  }
  return { cards: cardCount, objects: objectCount, processed: processedCount };
}


test('importScryfall', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsBySet('parl');
  const result = await importCardList(imp, cards);
  const expected_cards = 7;
  expect(result.objects).toBe(expected_cards);
  expect(result.cards).toBe(expected_cards);
  expect(result.processed).toBe(expected_cards);
  const models = await CardModel.find({ set_code: 'parl' }).exec();
  expect(models.length).toBe(expected_cards);
});

test('importScryfallCurrentSet', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const expected_cards = 399;
  const cards = await querier.getCardsBySet('eld');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBe(expected_cards);
  expect(result.cards).toBe(expected_cards);
  expect(result.processed).toBe(expected_cards);
  const models = await CardModel.find({ set_code: 'eld' }).exec();
  expect(models.length).toBe(expected_cards);
  const power = await CardModel.find({ 'card_faces.power_number': 2 }).exec();
  expect(power.length).toBe(70);
  const toughness = await CardModel.find({ 'card_faces.toughness_number': 2 }).exec();
  expect(toughness.length).toBe(58);
}, 25000);

test('test update metadata creates purchase_uris', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cat = new ScryfallCatalog();
  const cards = await querier.getCardsBySet('parl');
  const result = await importCardList(imp, cards);

  expect(result.objects).toBe(7);
  expect(result.cards).toBe(7);
  expect(result.processed).toBe(7);

  await cat.updateMetadataBySet('parl', [], {});
  const models = await CardModel.find({ set_code: 'parl' }).exec();
  expect(models.length).toBe(7);
  for (const m of models) {
    expect(Object.keys(m.purchase_uris).length).toBeGreaterThan(0);
  }
});


test('importCommanderBracketExtraTurns', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Time Walk"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Time Walk' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_EXTRA_TURNS.name)).toBe(true);
  }
});

test('importCommanderBracketGameChanger', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Cyclonic Rift"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Cyclonic Rift' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_GAME_CHANGER.name)).toBe(true);
  }
});

test('importCommanderBracketMassLandDenialDestroy', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Acid Rain"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Acid Rain' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});


test('importCommanderBracketMassLandDenialDestroy2', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Burning of Xinye"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Burning of Xinye' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});

test('importCommanderBracketMassLandDenialUntap', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Winter Moon"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Winter Moon' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});



test('importCommanderBracketMassLandDenialReturn 1', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Omen of Fire"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Omen of Fire' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});

test('importCommanderBracketMassLandDenialReturn 2', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Sunder"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Sunder' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});

test('importCommanderBracketMassLandDenialSacrifice', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Wildfire"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Wildfire' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});

test('importCommanderBracketMassLandDenialSacrifice2', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Thoughts of Ruin"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Thoughts of Ruin' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});

test('importCommanderBracketMassLandDenialDestroy', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Urza\'s Sylex"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Urza\'s Sylex' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});

test('importCommanderBracketMassLandDenialDestroy2', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Urza\'s Sylex"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Urza\'s Sylex' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL.name)).toBe(true);
  }
});

test('importCommanderBracketMassLandDenialReturn', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Displacement Wave"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Displacement Wave' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});

test('importCommanderBracketMassLandDenialSacrifice', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Tragic Arrogance"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Tragic Arrogance' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});

test('importCommanderBracketMassLandDenialSacrifice2', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Nazgûl Battle-Mace"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Nazgûl Battle-Mace' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});

test('importCommanderBracketTutor', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Demonic Tutor"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Demonic Tutor' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBeGreaterThan(0);
    expect(model.tags.some(t => t.name === TAG_COMMANDER_BRACKET_TUTORS.name)).toBe(true);
  }
});

test('importCommanderBracketLandTutor', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Crop Rotation"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Crop Rotation' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});

test('importCommanderBracketLandTutor1', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Marsh Flats"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Marsh Flats' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});

test('importCommanderBracketLandTutor2', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Knight of the White Orchid"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Knight of the White Orchid' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});


test('importCommanderBracketLandTutor3', async () => {
  const querier = new ScryfallClient();
  const imp = new ScryfallImporter();
  const cards = await querier.getCardsByName('"Maze\'s End"');
  const result = await importCardList(imp, cards);
  expect(result.objects).toBeGreaterThan(0);
  expect(result.cards).toBeGreaterThan(0);
  expect(result.processed).toBeGreaterThan(0);
  const models = await CardModel.find({ name: 'Maze\'s End' }).exec();
  expect(models.length).toBeGreaterThan(0);
  for (const model of models) {
    expect(model.tags.length).toBe(0);
  }
});
