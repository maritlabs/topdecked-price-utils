import { CardFaceSchema, CardTagSchema } from '@topdecked/common-model';
import {
  TAG_COMMANDER_BRACKET_EXTRA_TURNS, TAG_COMMANDER_BRACKET_GAME_CHANGER,
  TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL, TAG_COMMANDER_BRACKET_TUTORS
} from '@topdecked/common-model-api';

import { logger } from '../../util/logger';

/**
 * WARNING: This function uses regex negative lookbehind which will NOT work on Safari.
 */
function calculateCommanderBracketTags (card: { name: string } & { card_faces: Partial<CardFaceSchema>[] } & Partial<{ game_changer: boolean }>): CardTagSchema[] {
  const tags: CardTagSchema[] = [];

  const oracle_texts = card.card_faces?.map(face => face.oracle_text?.toLowerCase());
  for (const oracle_text of oracle_texts) {
    if (oracle_text) {

      if (oracle_text.includes('take an extra turn')
        || oracle_text?.includes('player takes an extra turn')
      ) {
        tags.push(TAG_COMMANDER_BRACKET_EXTRA_TURNS);
      }

      if (oracle_text?.match(/^.*(?<!(target|defending)\s+player\s+)(destroys?|exiles?|sacrifices?) (all|four|up to four) (?!.*nonland.*)(.*)\s?(lands|mountains|forests|islands|plains|swamps|wastes|permanents)(?!.*(named|you control).*).*/im)
        || oracle_text?.match(/^.*sacrifices (?!.*nonland.*)(.*)(land|mountain|forest|island|plains|swamp|waste|permanent)s? (of their choice)?\s?(for each).*/im)
        || oracle_text?.match(/^.*return (all|four|up to four)? (?!.*nonland.*).*\s?(lands|mountains|forests|islands|plains|swamps|wastes|permanents).*to their owners\' hands.*/im)
        || oracle_text?.match(/^.*tap (all|four|up to four)?(?!.*nonland.*).*\s?(lands|mountains|forests|islands|plains|swamps|wastes|permanents)(?!.*you control.*).*don\'t untap*/im)
        || oracle_text?.match(/^(?!.*nonland.*).*(lands|mountains|forests|islands|plains|swamps|wastes|permanents)(?!.*you control.*) don\'t untap.*/im)
        || oracle_text?.match(/^.*(?<!you\s+)can\'t untap (more than)? \S+ (basic|nonbasic)?\s*(?!.*nonland.*)(land|mountain|forest|island|swamp|waste|permanent).*/im)
        || oracle_text?.match(/^(?!.*nonland.*).*(land|mountain|forest|island|plains|swamp|waste|permanent).*lose all/im)
        || oracle_text?.match(/^(?!.*nonland.*).*(land|mountain|forest|island|plains|swamp|waste|permanent).* are (mountain|forest|island|swamp|waste)s.*/im)
      ) {
        if (!oracle_text.match(/^.*that are one or more colors.*/im)
          && !oracle_text.match(/^(?!.*for each.*).*(white|blue|black|red|green) permanent.*/im)
          && !oracle_text.match(/^.*originally printed in.*/im)
          && !oracle_text.match(/^.*with the same name.*/im)
          && !oracle_text.match(/^.*in addition to.*/im)
          && !oracle_text.match(/^.*(?!.*:.*)(choose a|choose an|target) (player|opponent).*/im)
          && !oracle_text.match(/^.*except.* lands.*/im)
          && !oracle_text.match(/^.*attached to (creatures|a creature).*/im)
          && !oracle_text.match(/^.*that's attacking.*/im)
          && !oracle_text.match(/^.*(defending|that) player*/im)
          && !oracle_text.match(/^.*that are (rare|mythic|uncommon|common).*/im)
          && !oracle_text.match(/^.*choose artifact or enchantment.*/im)
          && !oracle_text.match(/^.*add one mana of any color.*/im)
          && !oracle_text.match(/^.*of the color of your choice.*/im)
          && !oracle_text.match(/^.*puts it onto the battlefield.*/im)
          && !oracle_text.match(/^.*deals combat damage.*(defending|that) player.*/im)
        ) {
          tags.push(TAG_COMMANDER_BRACKET_MASS_LAND_DENIAL);
        }
      }

      if (
        oracle_text?.match(/.*search(es)?.*library.*for\b(?!.*(land|cave|desert|gate|mountain|forest|island|plains|swamp|waste).*).*shuffle/)
      ) {
        if (!oracle_text.includes('command tower')
          && !oracle_text.includes('that player')) {
          tags.push(TAG_COMMANDER_BRACKET_TUTORS);
        }
      }
    }
    if (card.game_changer) {
      tags.push(TAG_COMMANDER_BRACKET_GAME_CHANGER);
    }

    if (tags.length) {
      logger.debug(`TAGS: ${card.name} ${JSON.stringify(tags)} ${oracle_text}`);
    }
  }
  return tags;
}

export default calculateCommanderBracketTags;
