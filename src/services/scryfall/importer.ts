import fs, { ReadStream } from 'fs';
import fse from 'fs-extra';
import JSONStream from 'JSONStream';
import request from 'request-promise';
import { Card } from 'scryfall-api';
import stream from 'stream';

import {
  CardFaceSchema, CardRulingSchema, CardSchema, DefenseIndex, LoyaltyIndex, PowerToughnessIndex
} from '@topdecked/common-model';
import { TAG_GROUP_COMMANDER_BRACKET } from '@topdecked/common-model-api';
import { Arrays, Numbers, Objects, Strings } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, SetModel } from '../../model';
import { DatabaseUtil } from '../../util/database';
import { ENV } from '../../util/env';
import { logger } from '../../util/logger';
import { ScryfallCatalog } from './catalog';
import { ScryfallClient } from './scryfall.client';
import calculateCommanderBracketTags from './tags.commander-bracket';

const allSuperTypes = ['Basic', 'Elite', 'Host', 'Legendary', 'Ongoing', 'Snow', 'World'];

const catalog = new ScryfallCatalog();
const scryfall = new ScryfallClient();

export const DEPRECATION_THRESHOLD = 500;


// FIXME: Extract to constant in common-model-api?

export interface CardImportOptions {
  cardId?: string;
  setCode?: string;
  force?: boolean;
  forceDeprecation?: boolean;
  forceDownload?: boolean;
  forceImport?: boolean;
  markDeprecated?: boolean;
  pretend?: boolean;
}

export class ScryfallImporter {

  public async importSets (opts: CardImportOptions = {}) {
    logger.info('sets: importing scryfall data');
    logger.debug('%o', opts)
    await database.ready();
    const sets = await scryfall.getSets();
    logger.info('sets: successfully downloaded latest json data from scryfall');
    for (let set of sets) {
      try {
        let model = await SetModel.findOne({ code: set.code }).exec();
        if (!model) {
          model = await SetModel.findOne({ name: set.name }).exec();
        }
        if (model) {
          for (const prop of Object.keys(set)) {
            if (prop === 'type') {
              model[prop] = this.sanitizeSetType(set[prop]);
            } else {
              model[prop] = set[prop];
            }
          }
          logger.verbose(`Updating set: ${set.name}`);
          if (!opts.pretend) {
            model = await model.save();
          }
          logger.verbose(`Updated set: ${model.code}, ${model.name}`);
        } else {
          logger.verbose(`Inserting set: ${set.code}, ${set.name}`);
          if (!opts.pretend) {
            model = await SetModel.create({
              ...set,
              type: this.sanitizeSetType(set.type)
            });
          }
          logger.verbose(`Inserted set:${model.code}, ${model.name}`);
        }

        logger.debug('%o', model);
      } catch (err) {
        ENV.setExitCode(1);
        logger.error(`All set data: %o`, sets);
        logger.error(`Error importing set ${set.code}: %o --> %o`, err, set);
      }
    }
    logger.info('sets: successfully imported latest json data from scryfall');
  }

  public async importCards (opts: CardImportOptions = {}) {
    logger.info('%s: importing scryfall card data');
    logger.debug('%o', opts)
    await database.ready();
    const rulingsData = await this.downloadLatestBulkData('rulings', opts.forceDownload);
    const rulings = await this.readRulingsJsonBulkData(rulingsData.jsonPath);
    const bulkData = await this.downloadLatestBulkData('default_cards', opts.forceDownload);
    if (bulkData.downloaded || opts.forceImport) {

      try {
        const result = await this.importCardJsonBulkData(bulkData.jsonPath, rulings, opts);
        logger.info('%s: successfully imported latest json data from scryfall');

        if (opts.markDeprecated) {
          if (!opts.cardId) {
            const deprecated = await this.markDeprecations(result.imported, opts);
            logger.verbose('Number of card deprecated: %d', deprecated.deprecated);
            logger.verbose('Number of card un-deprecated: %d', deprecated.undeprecated);
          } else {
            throw new Error('cannot use --mark-deprecated with --card-id');
          }
        }

      } catch (err) {
        ENV.setExitCode(1);
        logger.error('error importing latest json data from scryfall: %s', err);
      }

    } else {
      logger.info('%s: no new data available, database is up to date');
    }
  }

  private async downloadLatestBulkData (dataType: string, forceImport: boolean) {
    const data = await this.getLatestBulkData(dataType);
    return this.downloadIfMoreRecent(data, forceImport);
  }

  private async getLatestBulkData (dataType: string) {
    logger.verbose('getting latest bulk data info from scryfall');
    const list = (await scryfall.get<{ data: any }>('bulk-data')).data;
    for (const bulkData of list) {
      if (bulkData.type === dataType) {
        logger.verbose('got latest bulk data info from scryfall');
        logger.debug('%o', bulkData);
        return bulkData;
      }
    }
    throw new Error('no bulk data info from scryfall for ' + dataType);
  }

  private async downloadIfMoreRecent (data: any, forceRefresh: boolean) {
    logger.verbose('downloading new data if a more recent version is found');
    const dataFolder = process.cwd() + '/bulkdata/' + data.type;
    const filePath = dataFolder + '/scryfall-' + data.updated_at + '.json';

    if (fse.existsSync(filePath) && forceRefresh) {
      logger.verbose('Cleaning old bulk data file (%s)', filePath);
      fse.removeSync(filePath);
    }
    if (!fse.existsSync(filePath)) {
      fse.mkdirsSync(dataFolder);
      logger.verbose('Downloading json to %s', filePath);
      await this.downloadBulkData(filePath, data.download_uri);
      logger.verbose('successfully downloaded json file (%s) from (%s)', filePath, data.download_uri);
      return { downloaded: true, jsonPath: filePath };
    } else {
      logger.verbose('Bulk Data File already present and up to date, skipping download');
      return { downloaded: false, jsonPath: filePath };
    }

  }

  private async downloadBulkData (destinationPath: string, uri: string) {
    logger.debug('Downloading %s %o', destinationPath, uri);
    const options = {
      method: 'GET',
      uri
    };

    try {
      const data = await request(options);
      fse.writeFileSync(destinationPath, data);
    } catch (err) {
      ENV.setExitCode(1);
      logger.error('error downloading file: %s', err);
      throw err;
    }
  }

  private async readRulingsJsonBulkData (sourcePath: string): Promise<{ [id: string]: CardRulingSchema[] }> {
    logger.verbose('importing Rulings JSON data');
    const strm = await this.getJSONStreamFromFile(sourcePath);

    const rulingsById: { [id: string]: CardRulingSchema[] } = {};

    let objectCount = 0;
    let ruleCount = 0;
    let ignoredCount = 0;

    for await (const rule of strm) {

      objectCount++;
      if (rule.object === 'ruling') {
        ruleCount++;
      } else {
        ignoredCount++;
        logger.verbose('objectCount (%d) ruleCount (%d), ignoredCount (%d)', objectCount, ruleCount, ignoredCount);
      }

      if (objectCount % 1000 === 0) {
        logger.verbose('objectCount (%d) ruleCount (%d), ignoredCount (%d)', objectCount, ruleCount, ignoredCount);
      }
      if (!rulingsById[rule.oracle_id]) {
        rulingsById[rule.oracle_id] = [];
      }

      rulingsById[rule.oracle_id].push({ date: rule.published_at, text: rule.comment, source: rule.source });
    }

    /* No more objects in the stream. We can do the update now */
    const ids = Object.keys(rulingsById);
    logger.verbose('Number of oracle_ids with rulings: %d', ids.length);
    logger.verbose('Number of objects proccessed: %d', objectCount);
    logger.verbose('Number of rules found: %d', ruleCount);
    return rulingsById;

  }

  private async importCardJsonBulkData (
    sourcePath: string,
    rulings: { [id: string]: CardRulingSchema[] },
    opts: CardImportOptions) {

    logger.verbose('importing Card JSON data');
    const strm = await this.getJSONStreamFromFile(sourcePath);
    const processedIds: string[] = [];

    let cardCount = 0;
    let objectCount = 0;
    let processedCount = 0;

    opts.setCode = opts.setCode?.toLowerCase();

    for await (const card of strm) {
      if (!opts.cardId || opts.cardId === card.id || opts.cardId === card.oracle_id) {
        if (!opts.setCode || card.set === opts.setCode) {
          logger.debug(`${cardCount}\tprocessing card: ${card.set_name} ${card.lang}, ${card.name}`);
          if (card.id) {
            processedIds.push(card.id)
          }

          const result = await this.importCard(card, rulings, opts);
          cardCount += result.cards;
          objectCount += result.objects;
          processedCount += result.processed;
          if (objectCount % 1000 === 0) {
            logger.verbose('objectCount (%d): cardCount (%d): processedCardCount (%d)', objectCount, cardCount, processedCount);
          }
        }
      }
    }

    logger.verbose('Number of objects proccessed: %d', objectCount);
    logger.verbose('Number of cards found: %d', cardCount);
    logger.verbose('Number of card processed: %d', processedCount);

    return { imported: processedIds };
  }

  private async markDeprecations (ids: string[], opts: CardImportOptions) {
    const setCodeFilter = (opts.setCode ? { set_code: opts.setCode } : {});
    const total = await CardModel.countDocuments(setCodeFilter);
    const existing = await CardModel.countDocuments({ deprecated: true, ...setCodeFilter }).lean(true).exec();
    const deprecated = await CardModel.find({ id: { $nin: ids }, ...setCodeFilter }).lean(true).exec();

    if ((deprecated?.length > existing + DEPRECATION_THRESHOLD) && !opts.forceDeprecation) {
      logger.error(`Attempting to deprecate too many cards: %o`, deprecated.map(c => `${c.oracle_id} ${c.id} ${c.set_code} ${c.name}`))
      throw new Error(`Deprecation threshold reached. More than ${DEPRECATION_THRESHOLD} cards will be updated. Re-run with --forceDeprecation to continue.`)
    }

    logger.debug(`${deprecated.length ?? 0}/${total} cards will be marked deprecated: \n\t`
      + deprecated.map(c => `${c.id} ${c.set_code} ${c.name}`).join('\n\t'));

    if (!opts.pretend) {
      const deprecationQuery = { id: { $in: deprecated.map(d => d.id) }, ...setCodeFilter };
      const undeprecationQuery = { id: { $nin: deprecated.map(d => d.id) }, ...setCodeFilter };

      const deprecations = await CardModel.updateMany(deprecationQuery, { $set: { deprecated: true } });
      const undeprecations = await CardModel.updateMany(undeprecationQuery, { $set: { deprecated: false } });

      return {
        deprecated: deprecations?.modifiedCount,
        undeprecated: undeprecations.modifiedCount
      };
    }
    return { deprecated: deprecated.length, undeprecated: ids.length - deprecated.length };
  }


  private extractAllTypes (typeLine: string) {
    const supertypes = [];
    const types = [];
    let subtypes = [];

    if (typeLine) {
      const split = typeLine.trim().split('—');
      const supertypestypes = split[0].trim().split(' ');
      for (const type of supertypestypes) {
        if (allSuperTypes.includes(type)) {
          supertypes.push(type);
        } else {
          types.push(type);
        }
      }

      if (split.length === 2) {
        subtypes = split[1].trim().split(' ');
      }
    }

    return {
      supertypes,
      types,
      subtypes,
    };

  }

  private async getJSONStreamFromFile (sourcePath: string): Promise<ReadStream> {
    try {
      // tslint:disable-next-line:no-bitwise
      fse.accessSync(sourcePath, (fse as any).F_OK | (fse as any).R_OK);
      const rs: ReadStream = fs.createReadStream(sourcePath, {
        encoding: 'utf8'
      });
      // Needed to bind JSONStream with async iterator
      const passThrough = new stream.PassThrough({
        objectMode: true
      });
      const parser = JSONStream.parse('*')
      return rs.pipe(parser).pipe(passThrough);
    } catch (err) {
      logger.error('Permission error for file (%s): %o', sourcePath, err);
      throw err;
    }
  }


  private calculateCardFaceCMC (card: CardSchema, face: CardFaceSchema) {
    const regex = /\{([^}]+)\}/g;
    const numberRegex = /\d+/;
    let result = face.cmc ?? 0;
    if (!result) {
      if (face.mana_cost?.length) {
        const symbols = face.mana_cost.match(regex);
        for (const symbol of (symbols ?? [])) {
          if (!['{X}'].includes(symbol.trim().toUpperCase())) {
            const numbers = symbol.match(numberRegex);
            if (numbers?.length) {
              result += Numbers.ensureInteger(numbers[0], 0);
            } else {
              result += 1;
            }
          }
        }
      } else {
        result = Numbers.ensureInteger(card.cmc, 0);
      }
    }

    logger.debug(`${card.cmc}, ${card.name}, ${card.mana_cost}: CMC for ${face.name} is ${result}`);
    return result;
  }


  public extractCardFace (card: CardSchema, cardFace: CardFaceSchema) {

    const getAsNumberOrUndefined = (value: string) => {
      // power":["*","-1","0","1","1+*","10","11","12","13","15","16","2","2+*","20","3","4","5","6","7","8","9"]
      // "toughness":["*","-1","0","1","1+*","10","11","12","13","14","15","16","2","2+*","20","3","4","5","6","7","7-*","8","9"]
      const num = Number.parseInt(value, 10);
      if (!isNaN(num)) {
        return num;
      }
      return undefined;
    };

    const result: CardFaceSchema & DefenseIndex & PowerToughnessIndex & LoyaltyIndex = {
      name: cardFace.name,
      image_uris: cardFace.image_uris,

      color_indicator: cardFace.color_indicator,
      colors: cardFace.colors,

      cmc: cardFace.cmc,
      mana_cost: cardFace.mana_cost,

      defense: cardFace.defense,
      defense_number: getAsNumberOrUndefined(cardFace.defense),
      loyalty: cardFace.loyalty,
      loyalty_number: getAsNumberOrUndefined(cardFace.loyalty),
      power: cardFace.power,
      power_number: getAsNumberOrUndefined(cardFace.power),
      toughness: cardFace.toughness,
      toughness_number: getAsNumberOrUndefined(cardFace.toughness),

      oracle_text: cardFace.oracle_text,
      flavor_name: Strings.isNilOrEmpty(cardFace.flavor_name) ? null : cardFace.flavor_name,
      flavor_text: cardFace.flavor_text,

      type_line: this.extractTypeLine(card, cardFace),
      ...this.extractAllTypes(cardFace.type_line),

      printed_name: cardFace.printed_name,
      printed_text: cardFace.printed_text,
      printed_type_line: cardFace.printed_type_line,

      illustration_id: cardFace.illustration_id,
      artist: cardFace.artist,
      watermark: cardFace.watermark
    };

    if (!Numbers.isNumeric(result.cmc)) {
      result.cmc = this.calculateCardFaceCMC(card, result);
    }

    return Objects.deleteNils(result);
  }

  private extractTypeLine (card: CardSchema, cardFace: CardFaceSchema) {
    if (card && cardFace) {
      if (card.layout === 'art_series') {
        return 'Card';
      }
      return cardFace.type_line ?? 'Card';
    }
    return 'Card';
  }

  public async importCard (card: any, rulings: { [id: string]: CardRulingSchema[] }, opts: CardImportOptions) {
    let cardCount = 0;
    let objectCount = 0;
    let processedCount = 0;

    objectCount++;

    if (card && card.object === 'card') {
      cardCount++;

      try {
        const doc: CardSchema = {
          // Some fields are not calucluated here, and are left undefined

          id: card.id,
          metadata: undefined,

          oracle_id: this.getOracleId(card),

          all_parts: undefined,
          card_faces: card.card_faces,
          card_back_id: card.card_back_id,

          games: this.getSupportedGames(card),
          games_available: undefined,

          prices: undefined,
          purchase_uris: undefined,
          related_uris: undefined,

          flavor_name: Strings.isNilOrEmpty(card.flavor_name) ? null : card.flavor_name,
          name: this.getCardName(card),
          names: [],

          set_id: card.set_id,
          set_code: card.set,
          set_name: card.set_name,
          collector_number: card.collector_number,

          reserved: card.reserved,

          lang: card.lang,

          foil: card.foil,
          nonfoil: card.nonfoil,
          finishes: this.getSupportedFinishes(card),

          released_at: DatabaseUtil.ensureDate(card.released_at),
          rarity: this.sanitizeRarity(card.rarity),

          oversized: card.oversized,
          layout: card.layout,
          border_color: card.border_color,
          frame: card.frame,
          frame_effects: this.sanitizeFrameEffects(card.frame_effects),

          promo: card.promo,
          reprint: card.reprint,

          booster: card.booster,
          full_art: card.full_art,
          textless: card.textless,
          security_stamp: card.security_stamp,
          story_spotlight: card.story_spotlight,
          edhrec_rank: Numbers.ensureInteger(card.edhrec_rank, -1),
          variation: card.variation,
          digital: card.digital,
          artist_ids: card.artist_ids,
          promo_types: card.promo_types,
          preview: undefined,

          keywords: card.keywords ?? [],
          legalities: card.legalities,
          rulings: undefined,
          tags: [],
          annotations: undefined,

          cmc: card.cmc || 0,
          mana_cost: card.mana_cost,
          colors: card.colors,
          color_identity: card.color_identity,
          max_quantity: 0,

          arena_id: card.arena_id,
          multiverse_ids: card.multiverse_ids,
          mtgo_id: card.mtgo_id,
          mtgo_foil_id: card.mtgo_foil_id,

          scryfall_id: card.id,
          scryfall_api_uri: card.uri,
          scryfall_prints_search_uri: card.prints_search_uri,
          scryfall_rulings_uri: card.rulings_uri,
          scryfall_set_search_uri: card.set_search_uri,
          scryfall_uri: card.scryfall_uri,

          tcgplayer_id: card.tcgplayer_id,
          tcgplayer_etched_id: card.tcgplayer_etched_id
        };

        // Don't update these fields here, they may be updated below if possible, or set manually / by other tasks
        delete doc.metadata;
        delete doc.all_parts;
        delete doc.games_available;
        delete doc.rulings;
        delete doc.annotations;
        delete doc.prices;
        delete doc.purchase_uris;
        delete doc.related_uris;

        // TODO: Is there a better place to do this?
        if (doc.name === 'Firemind\'s Research' && doc.id === 'cd318e8f-418a-4e17-bf0e-a4bfc0fb8ebf') {
          doc.arena_id = 69780;
        }

        if (card.preview) {
          doc.preview = card.preview;
        }

        if (card.all_parts) {
          doc.all_parts = card.all_parts.map((part: any) => {
            return {
              id: part.id,
              component: part.component,
              name: part.name,
              type_line: part.type_line
            };
          });
        }

        if (doc.card_faces) {
          doc.card_faces = doc.card_faces.map(face => this.extractCardFace(doc, face));
        } else {
          doc.card_faces = [this.extractCardFace(doc, card)];
        }

        if (doc.cmc === 0 && ['reversible_card'].includes(doc.layout)) {
          doc.cmc = doc.card_faces[0]?.cmc ?? doc.cmc;
        }

        if (!doc.flavor_name) {
          const flavor_name = (card.card_faces ?? [card]).map(c => c.flavor_name).filter(n => n).join(" // ");
          if (!Strings.isNilOrEmpty(flavor_name)) {
            doc.flavor_name = flavor_name;
          }
        }

        doc.names = Arrays.uniqueValues(Arrays.nonNullValues([
          doc.name,
          doc.flavor_name,
          Strings.sanitizeDiacritics(doc.name, { form: 'NFD' }),
          Strings.sanitizeDiacritics(doc.flavor_name, { form: 'NFD' })
        ]));

        if (doc.card_faces.length > 0 && card.image_uris && Object.keys(card.image_uris).length > 0) {
          doc.card_faces[0].image_uris = card.image_uris;
        }

        const mergeFaceProperty = (prop: string) => {
          return doc.card_faces.map(f => f[prop]).filter(c => c && c.length > 0).join(' // ');
        };

        const mergeFacePropertyArray = (prop: string) => {
          const result = [];
          for (const face of doc.card_faces) {
            if (face && Array.isArray(face[prop])) {
              Arrays.merge(face[prop], result);
            }
          }
          return Arrays.nonNullValues(result);
        };

        // Merge all colors from all faces (and incuding root property, since sometimes this data is not present on faces.)
        doc.colors = Arrays.uniqueValues(Arrays.nonNullValues(mergeFacePropertyArray('colors').concat(card.colors || [])));
        doc.mana_cost = mergeFaceProperty('mana_cost');

        if (rulings) {
          doc.rulings = rulings[doc.oracle_id]?.filter(r => !!r.text && !!r.date);
        }

        if (card.purchase_uris) {
          doc.purchase_uris = {};
          Object.assign(doc.purchase_uris, catalog.getAffiliateURIs(card.purchase_uris));
        }

        if (card.related_uris) {
          doc.related_uris = {};
          Object.assign(doc.related_uris, catalog.getAffiliateURIs(card.related_uris));
        }

        doc.tags = calculateCommanderBracketTags({ ...doc, game_changer: card.game_changer });

        const maxQuantities = {
          'Nazgûl': 9,
          'Seven Dwarves': 7,
          'Persistent Petitioners': -1,
          'Rat Colony': -1,
          'Relentless Rats': -1,
          'Shadowborn Apostle': -1,
          'Dragon\'s Approach': -1,
          'Hare Apparent': -1
        };
        if (Object.keys(maxQuantities).includes(card.name) || doc.card_faces?.[0].oracle_text?.includes('can have any number of')) {
          doc.max_quantity = maxQuantities[card.name] ?? -1;
          logger.warn('Setting custom max quantity for card: %s %d', card.name, doc.max_quantity);
        }
        if (doc.card_faces?.[0]?.supertypes?.includes('Basic')) {
          doc.max_quantity = -1;
        }

        Objects.deleteNils(doc);

        logger.debug(`Staged card: ${card.set_name} ${card.lang}, ${card.name}, %o`, doc);
        let model = await CardModel.findOne({ id: doc.id }).exec();
        if (model) {
          for (const prop of Object.keys(doc)) {
            if (['purchase_uris', 'related_uris', 'prices'].includes(prop)) {
              Object.assign(model[prop], doc[prop]);
            } else if (prop === 'tags') {
              model[prop] = model[prop].filter(t => t.group !== TAG_GROUP_COMMANDER_BRACKET).concat(doc.tags);
            } else {
              model[prop] = doc[prop];
            }
            model.markModified(prop);
          }
          Objects.deleteNils(doc);
          logger.debug(`Updating card: ${card.set_name} ${card.lang}, ${card.name}, promo: ${card.promo}, reprint: ${card.reprint}`);

          if (!opts.pretend) {
            try {
              model = await model.save();
            } catch (error) {
              logger.error('%o', error)
              // Retry on error incase database version got out of sync?
              const current = await CardModel.findOne({ _id: model._id }).exec();
              model.__v = current.__v;
              await model.save();
            }
          }
          logger.debug(`Updated card:${model.set_name} ${model.lang}, ${model.name}\t${model.foil ? 'foil' : ''}` +
            ` \t${model.nonfoil ? 'nonfoil' : ''}`);
        } else {
          logger.debug(`Inserting card: ${card.set_name} ${card.lang}, ${card.name}, promo: ${card.promo}, reprint: ${card.reprint}`);
          Objects.deleteNils(doc);
          if (!opts.pretend) {
            model = await CardModel.create(doc);
          }
          logger.debug(`Inserted card: ${model.set_name}, ${model.lang}, ${model.name}\t${model.foil ? 'foil' : ''}` +
            ` \t${model.nonfoil ? 'nonfoil' : ''}`);
        }

      } catch (err) {
        logger.error(`Card errored: ${card.set_name}, ${card.lang}, ${card.name}: %s, %o, %o`,
          err.message, card, err);
        ENV.setExitCode(1);
      }
      processedCount = processedCount + 1;
    }

    return { cards: cardCount, objects: objectCount, processed: processedCount };
  }

  private getCardName (card: Card): string {
    if (card.games.length === 1 && card.games?.includes('arena')) {
      return Strings.replaceAll(card.name, /\bA-/, '');
    }
    return card.name;
  }

  private getOracleId (card: Card) {
    if (card?.oracle_id) {
      return card.oracle_id;
    }
    const oracle_ids = Arrays.uniqueValues((card?.card_faces ?? []).map((f: any) => f.oracle_id));
    return (oracle_ids.length === 1) ? oracle_ids[0] : null;
  }

  private getSupportedFinishes (card: Card) {
    const converted = {
      'glossy': 'nonfoil'
    };
    return Arrays.uniqueValues(Arrays.arrayify(card.finishes) ?? []).map(f => converted[f] ?? f);
  }

  private getSupportedGames (card: Card) {
    if (['afc', 'h1r'].includes(card.set)) {
      return card.games?.length ? card.games : ['paper', 'mtgo'];
    }
    return card.games ?? [];
  }

  private sanitizeFrameEffects (effects: string[]) {
    effects = Arrays.nonNullValues(effects ? Arrays.arrayify(effects) : []);
    effects = effects.map(e => e === 'legenddary' ? 'legendary' : e);
    return Arrays.uniqueValues(effects);
  }

  private sanitizeRarity (rarity: string) {
    if (!['common', 'uncommon', 'rare', 'mythic', 'special'].includes(rarity)) {
      return 'special';
    }
    return rarity;
  }

  private sanitizeSetType (type: string) {
    if (type === 'promos') {
      return 'promo';
    }
    return type;
  }
}
