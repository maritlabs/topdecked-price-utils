import { SetModel, SetModelV1 } from '../../model';
import { logger } from '../../util/logger';

export class SetBackporter {
  public async set2toSet1 (code: string) {
    logger.verbose('finding v2 set');
    const setV2 = await SetModel.findOne({ code }).exec();

    if (setV2) {
      const existingV1 = await SetModelV1.findOne({
        $or: [
          { code },
          { name: setV2.name }
        ]
      });

      if (existingV1) {
        logger.verbose('set already exists: %s, %s', existingV1.code, existingV1.releaseDate);
        return existingV1;
      }

      logger.verbose('backporting set: %s, %s', setV2.name, setV2.released);
      const setV1 = {
        code,
        releaseDate: new Date(setV2.released).toISOString().slice(0, 10),
        name: setV2.name,
        onlineOnly: setV2.digital_only,
        numcards: setV2.card_count,
        gathererCode: code,
        type: setV2.type,
        checksum: null,
        initialized: true,
        source: 'imported',
        scryfall_code: code
      };
      const created = await new SetModelV1(setV1).save();
      logger.verbose('backported set: %s, %s', created.code, created.releaseDate);
      logger.debug('%o', created);

      setV2.topdecked_code = code;
      await setV2.save();
      return created;
    } else {
      console.error('no set found in V2 database for code %s', code);
      return null;
    }
  }
}
