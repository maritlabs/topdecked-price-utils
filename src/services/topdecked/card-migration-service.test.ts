

const OVERLAP_QUANTITY = 63;

// beforeAll(async () => {
//   await database.disconnect();
//   database.database = 'local';
//   await database.ready();
// });

// afterAll(async () => {
//   await database.disconnect();
//   database.database = 'test';
// });

test('placeholder', async () => {
}, 2000);

// test('Can migrate Card1 to Card2', async () => {
//   const migration = new CardMigrationService();
//   const originals = await SetModelV1.find({}).exec();

//   expect(originals).toBeTruthy();
//   expect(originals.length).toBeGreaterThan(0);

//   const result = await migration.migrateAll();

//   expect(result.missingCards.length).toBe(0);
//   expect(result.setsFound.length).toBe(originals.length);
//   expect(result.setsMissing.length).toBe(0);

//   const oldDbCount = (await CardModelV1.countDocuments({ scryfall_id: { $ne: null } }).exec());
//   const newDbCount = (await CardModel.countDocuments({ topdecked_id: { $ne: null } }).exec());
//   console.log('oldCount', oldDbCount, 'newCount', newDbCount);
//   expect(oldDbCount).toBe(newDbCount + OVERLAP_QUANTITY);
//   done();
// }, (1000 * 60 * 60));


// test('Validate Card1 to Card2', async () => {
//   const migration = new CardMigrationService();
//   const card1s = await CardModelV1.find({ scryfall_id: { $ne: null } }).exec();
//   for (const card1 of card1s) {
//     const card2 = await CardModel.findOne({ id: card1.scryfall_id }).exec();
//     expect(migration.cardToCard2Name(card1)).toBe(card2.name);
//     // expect(card1.scryfall_id).toBe(card2.id);
//     // expect(card1.scryfall_set_code).toBe(card2.set_code);
//     expect(card1.scryfall_name).toBe(card2.name);
//   }
//   done();
// }, (1000 * 60 * 60));


// test('Validate Card2 to Card1', async () => {
//   const migration = new CardMigrationService();
//   const card2s = await CardModel.find({ topdecked_id: { $ne: null } }).exec();
//   for (const card2 of card2s) {
//     const card1 = await CardModelV1.findOne({ id: card2.topdecked_id }).exec();
//     expect(migration.cardToCard2Name(card1)).toBe(card2.name);
//     // expect(card1.scryfall_id).toBe(card2.id);
//     // expect(card1.scryfall_set_code).toBe(card2.set_code);
//     expect(card1.scryfall_name).toBe(card2.name);
//   }
//   done();
// }, (1000 * 60 * 60));
