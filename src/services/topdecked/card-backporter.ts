import { CardFaceSchema, CardSchema, CardV1 } from '@topdecked/common-model';
import { Numbers, Objects, Strings } from '@topdecked/common-util';

import { CardModelV1 } from '../../model';
import { logger } from '../../util/logger';
import { CardConstantMapper } from './v1-v2-constant-mappings';

export class CardBackporter {
  public async card2toCard1 (card: CardSchema, opts: {
    force?: boolean
  } = {}) {
    if (card['toJSON']) {
      card = (card as any).toJSON();
    }
    if (card) {
      logger.debug('backporting card: %s, %s', card.name, card.set_code, card.id);
      logger.debug('%o', card);
      for (let i = 0; i < card.card_faces.length; i++) {
        const face: CardFaceSchema = card.card_faces[i];
        let loyalty = Numbers.ensureInteger(face.loyalty, -1);
        if (loyalty < 0) {
          loyalty = null;
        }
        const cardV1: CardV1 = {
          id: card.id + (card.card_faces.length > 1 ? ('-' + i) : ''),
          artist: face.artist,
          imageURL: (face.image_uris ? face.image_uris.normal : null),
          border: card.border_color,
          cmc: face.cmc,
          colors: card.colors,
          flavor: face.flavor_text,
          colorIdentity: card.color_identity,
          foil: card.foil,
          foilonly: !card.nonfoil,
          foreignNames: undefined,
          hand: undefined,
          hasfoil: card.foil,
          imageName: face.illustration_id,
          layout: CardConstantMapper.layoutToV1(card),
          legalities: CardConstantMapper.legalitiesToV1(card.legalities),
          life: undefined,
          loyalty,
          manaCost: card.mana_cost,
          mciNumber: null,
          multiverseid: CardConstantMapper.multiverseIdToV1(card, i),
          name: face.name,
          names: (card.card_faces.length > 0 ? card.card_faces.map(f => f.name) : []),
          number: (i === 0 ? card.collector_number : card.collector_number + 'b'),
          originalText: face.printed_text,
          originalType: face.printed_type_line,
          power: face.power,
          priceTrends: null,
          priceUpdated: null,
          prices: null,
          pricesSet: null,
          printings: null,
          promo: card.promo,
          quantity: null,
          rarity: Strings.isNilOrEmpty(card.rarity) ? 'common' : Strings.capitalizeFirst(CardConstantMapper.rarityToV1(card.rarity)),
          releaseDate: card.released_at.toISOString().slice(0, 10),
          reprint: card.reprint,
          reserved: card.reserved,
          rulings: card.rulings.map(r => ({
            ...r,
            date: r.date.toISOString().slice(0, 10)
          })),
          set: null,
          setcode: card.set_code,
          source: 'imported',
          starter: null,
          subtypes: face.subtypes,
          supertypes: face.supertypes,
          text: face.oracle_text,
          timeshifted: null,
          toughness: face.toughness,
          type: face.type_line,
          types: face.types,
          variations: null,
          watermark: face.watermark,

          // Migration fields
          scryfall_id: card.id,
          scryfall_name: card.name,
          scryfall_set_code: card.set_code,
        };

        Objects.deleteNils(cardV1);
        logger.debug('about to save backported card: %o', cardV1);
        const existing = await CardModelV1.findOne({ id: cardV1.id }).exec();
        if (!existing) {
          const created = await new CardModelV1(cardV1).save();
          logger.verbose('backported card: %s, %s, %s', created.id, created.setcode, created.name, created.number);
          logger.debug('%o', created);
          return created;
        } else {
          logger.verbose('backported card exists: %s, %s, %s', existing.id, existing.setcode, existing.name, existing.number);
          if (opts.force) {
            Object.assign(existing, cardV1);
            await existing.save();
          }
          return existing;
        }
      }
    } else {
      console.error('card is required, was %o', card);
    }
    return null;
  }
}
