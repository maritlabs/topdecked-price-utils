// tslint:disable:max-line-length

import { Document } from 'mongoose';

import { CardPricesV1, CardV1 } from '@topdecked/common-model';
import { Time } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, CardModelV1 } from '../../model';
import { logger } from '../../util/logger';
import { TcgPlayerCatalog } from '../tcgplayer/catalog';
import { PriceMigrationUtils } from './card-price-migration-utils';

const EMPTY_CARD_PRICES_V1 = {
  lowprice: 0,
  avgprice: 0,
  marketprice: 0,
  hiprice: 0,

  etchedlowprice: 0,
  etchedavgprice: 0,
  etchedmarketprice: 0,
  etchedhiprice: 0,

  foillowprice: 0,
  foilavgprice: 0,
  foilmarketprice: 0,
  foilhiprice: 0,

  euro: 0,
  euro_foil: 0,
  tix: 0,
  tix_foil: 0
};

export class PriceBackportService {

  constructor() {
    // nothing yet
  }


  /**
   * Backport Card Prices from V2 to V1
   */
  public async backportPrices (opts: {
    cardId?: string,
    setCode?: string,
    batchSize?: number,
    force?: boolean
  } = {}) {
    await database.ready();
    const catalog = new TcgPlayerCatalog();
    const priceUtils = new PriceMigrationUtils();

    const batchSize = opts.batchSize ?? 500;
    const batch: (CardV1 & Document)[] = [];
    const fireBatch = async () => {
      if (batch && batch.length > 0) {
        await CardModelV1.bulkWrite(batch.map(card => ({
          updateOne: {
            filter: { id: card.id },
            update: {
              prices: card.prices,
              pricesSet: card.pricesSet,
              priceTrends: card.priceTrends
            }
          }
        })));
        logger.verbose(`saved batch of ${batch.length} cards`);
        batch.length = 0;
      }
    };


    const query = { scryfall_id: { $ne: null } };
    if (opts.cardId) {
      logger.info('Backporting prices for single card id: ' + opts.cardId);
      query['id'] = opts.cardId;
    } else if (opts.setCode) {
      query['setcode'] = opts.setCode;
    }

    const count = await CardModelV1.countDocuments(query);
    let i = 0;
    const cursor = CardModelV1.find(query).lean().batchSize(batchSize).cursor();
    await cursor.eachAsync(async (card) => {
      if (batch.length >= batchSize) {
        await fireBatch();
      }

      if (card) {
        logger.verbose(`backporting prices and trends for: ${card.id} ${card.setcode} ${card.name}`);
        const card2 = await CardModel.findOne({ scryfall_id: card.scryfall_id }).lean();
        if (card2) {

          const counts: Partial<CardPricesV1> = {
            ...EMPTY_CARD_PRICES_V1
          };

          const pricesSet: CardPricesV1 = {
            ...EMPTY_CARD_PRICES_V1,

            tcglink: catalog.substituteAffiliateCode(card2.purchase_uris?.tcgplayer || (card.prices ? card.prices.tcglink : '')),
            updated: card2.prices?.tcgplayer ? card2.prices?.tcgplayer?.updated as Date : Time.now()
          };

          // Backport set/version prices
          if (card2.prices?.tcgplayer) {
            const p = card2.prices.tcgplayer.prices;
            pricesSet.avgprice = (p && p.mid ? p.mid : 0);
            pricesSet.lowprice = (p && p.low ? p.low : 0);
            pricesSet.hiprice = (p && p.high ? p.high : 0);
            pricesSet.marketprice = (p && p.market ? p.market : 0);

            const pf = card2.prices.tcgplayer.prices_foil;
            pricesSet.foilavgprice = (pf && pf.mid ? pf.mid : 0);
            pricesSet.foillowprice = (pf && pf.low ? pf.low : 0);
            pricesSet.foilhiprice = (pf && pf.high ? pf.high : 0);
            pricesSet.foilmarketprice = (pf && pf.market ? pf.market : 0);

            card.pricesSet = pricesSet;
          } else if (card2.prices.scryfall) {
            const p = card2.prices.scryfall;
            pricesSet.avgprice = p.usd || 0;
            pricesSet.foilavgprice = p.usd_foil || 0;
            card.pricesSet = pricesSet;
          }

          if (card2.prices.scryfall) {
            const p = card2.prices.scryfall;
            pricesSet.euro = p.eur || 0;
            pricesSet.euro_foil = p.eur_foil || 0;
            pricesSet.tix = p.tix || 0;
            pricesSet.tix_foil = p.tix_foil || 0;
          }

          // Calculate average prices
          const allCard2s = await CardModel.find({ oracle_id: card2.oracle_id }).lean().exec();
          if (allCard2s && allCard2s.length) {


            const avgPrices: CardPricesV1 = {
              ...EMPTY_CARD_PRICES_V1,

              tcglink: catalog.substituteAffiliateCode(card2.purchase_uris.tcgplayer || (card.prices ? card.prices.tcglink : '')),
              updated: card2.prices.tcgplayer ? card2.prices.tcgplayer.updated as Date : Time.now()
            };

            for (const c2 of allCard2s) {
              if (c2.prices?.tcgplayer) {

                for (const v of ['', 'foil', 'etched']) {
                  const p = c2.prices.tcgplayer['prices' + (v?.length ? '_' + v : '')];
                  if (p) {
                    if (p) {
                      avgPrices[v + 'avgprice'] += (p && p.mid ? p.mid : 0);
                      if (avgPrices[v + 'avgprice']) { counts[v + 'avgprice']++; }

                      avgPrices.lowprice += (p && p.low ? p.low : 0);
                      if (avgPrices.lowprice) { counts.lowprice++; }

                      avgPrices.hiprice += (p && p.high ? p.high : 0);
                      if (avgPrices.hiprice) { counts.hiprice++; }

                      avgPrices.marketprice += (p && p.market ? p.market : 0);
                      if (avgPrices.marketprice) { counts.marketprice++; }
                    }
                  }
                }
              }

              if (card2.prices.scryfall) {
                const p = card2.prices.scryfall;
                avgPrices.euro = p.eur || 0;
                if (avgPrices.euro) { counts.euro++; }

                avgPrices.euro_foil = p.eur_foil || 0;
                if (avgPrices.euro_foil) { counts.euro_foil++; }

                avgPrices.tix = p.tix || 0;
                if (avgPrices.tix) { counts.tix++; }

                avgPrices.tix_foil = p.tix_foil || 0;
                if (avgPrices.tix_foil) { counts.tix_foil++; }
              }
            }

            for (const point of Object.keys(EMPTY_CARD_PRICES_V1)) {
              if (avgPrices[point] && counts[point]) { avgPrices[point] /= counts[point]; }
            }

            card.prices = avgPrices;
          }

          priceUtils.updateTrends(card, opts);

          logger.verbose(`${i++}/${count}: calculated prices & trends: %s %s`, card.setcode, card.name);
          logger.debug('prices: %o, pricesSet: %o trends: %o', card.prices, card.pricesSet, card.priceTrends);

          batch.push(card);
        }
      }
    });

    await fireBatch();
  }


}
