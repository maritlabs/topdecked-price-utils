import {
  CardSchema, CardV1, PriceMotionPeriodSchema, PriceMotionSummarySchema
} from '@topdecked/common-model';
import { Dates, Time } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, CardModelV1, PriceMotionModelV2 } from '../../model';
import { logger } from '../../util/logger';

// tslint:disable:max-line-length

export interface PriceTrendOpts {
  format?: string;
  force?: boolean;
  pretend?: boolean;
  setCode?: string;
}

export type MinimalCardSchema = Pick<CardSchema,
  'name'
  | 'id'
  | 'legalities'
  | 'rarity'
  | 'released_at'
  | 'reserved'
  | 'set_code'
>

export interface MoverData {
  change: number;
  difference: number;
  percent: number;
  current: number;
}

const MAX_MOVERS = 100;

const IGNORED_SETS = ['mb1', 'cmb1'];
const ANNIV_30 = ['30a', 'p30a', 'p30h', 't30a', 'p30t', 'p30m']
const ANNOYING_SETS = ['lea', 'leb', '2ed', 'ced', 'fbb', 'cei', ...ANNIV_30];

const CATEGORIES = ['all', 'interests', 'standard', 'pioneer', 'modern', 'legacy', 'vintage', 'pauper'];
const PERIODS = ['daily', 'weekly', 'monthly'];
const METRICS = ['difference', 'percent'];
const PRICE_POINTS = ['high', 'market', 'mid', 'low', 'foil', 'euro', 'euro_foil', 'tix'];

const EMPTY_POINTS = () => {
  const result = {};
  for (const point of PRICE_POINTS) {
    result[point] = [];
  }
  return result;
};

const EMPTY_METRICS_V2 = () => {
  const result = {};
  for (const metric of METRICS) {
    result[metric] = EMPTY_POINTS();
  }
  return result;
};

const EMPTY_PERIODS_V2 = () => {
  const result: PriceMotionPeriodSchema = {} as any;
  for (const period of PERIODS) {
    result[period] = EMPTY_METRICS_V2();
  }
  return result;
};


const isAnnoyance = (card: MinimalCardSchema) => {
  return ANNOYING_SETS.includes(card.set_code);
};

const isPossiblyStandard = (card: MinimalCardSchema) => {
  return Dates.ensureDate(card.released_at, new Date())?.getTime() > Time.ago(Time.YEAR * 3).getTime();
};

const isMover = <T extends MoverData> (
  currentMovers: T[],
  card: CardV1,
  opts: {
    metric: string,
    period: string,
    point: string
  }) => {
  const change = card?.priceTrends?.motion[opts.metric][opts.period][opts.point] ?? 0;
  const threshold = currentMovers[0]?.change ?? 0;
  return change && (currentMovers.length < MAX_MOVERS) || (Math.abs(change) > Math.abs(threshold));
};


const setMover = <T extends MoverData> (
  currentMovers: T[],
  lean: MinimalCardSchema,
  mover: CardV1,
  opts: {
    metric: string,
    period: string,
    point: string,
    convert: (lean: MinimalCardSchema, base: MoverData) => T
  }
) => {
  const sort = () => {
    currentMovers.sort((l, r) => {
      return Math.abs(l.change) - Math.abs(r.change);
    });
  };

  const trend = mover.priceTrends.daily.slice(-1)[0];
  if (trend) {
    const change: number = mover.priceTrends.motion[opts.metric][opts.period][opts.point];
    const difference: number = mover.priceTrends.motion['difference'][opts.period][opts.point];
    const percent: number = mover.priceTrends.motion['percent'][opts.period][opts.point];
    const current: number = trend[opts.point];

    if (current !== difference && current !== 0 && percent !== 0 && change !== 0 && difference !== 0) {
      if (currentMovers.length >= MAX_MOVERS) {
        logger.debug('trimmed length of mover result list');
        currentMovers.shift();
      }
      const entry = {
        ...opts.convert(lean, {
          change,
          difference,
          percent,
          current
        }),
      };
      logger.debug('added entry %o', entry)
      currentMovers.push(entry);
      sort();
    }
  }
};

const convertV2 = (card: MinimalCardSchema, base: MoverData) => {
  return {
    ...base,
    id: card.id,
    name: card.name,
    rarity: card.rarity,
    reserved: !!card.reserved,
    set_code: card.set_code
  };
};

const isLegal = (card: MinimalCardSchema, format: string) => {
  return ['legal', 'restricted'].includes(card?.legalities?.[format]);
};

export class PriceTrendService {

  constructor() { }

  async saveMotionSummary (trends: PriceMotionSummarySchema, opts: PriceTrendOpts = {}) {
    await database.ready();

    const v2 = new PriceMotionModelV2(trends);
    if (!opts.pretend) {
      await v2.save();
    }

    logger.info('saved card price motion summary data');
  }

  async calculateMotionSummary (opts: PriceTrendOpts = {}): Promise<PriceMotionSummarySchema> {

    if(opts.setCode) {
      opts = { ...opts, pretend: opts.pretend || !!opts.setCode };
      logger.warn('NOTICE: Pretend mode is **ON** due to --format or --set-code specified in opts.');
    }
    logger.debug('OPTS: %o',opts);

    const started = Time.now();
    await database.ready();
    const result: PriceMotionSummarySchema = {
      categories: {}
    };

    const categories = opts.format ? [opts.format] : CATEGORIES;

    for (const cat of categories) {
      result.categories[cat] = EMPTY_PERIODS_V2();
    }

    const query = { 'priceTrends': { $ne: null }, set_code: opts.setCode ? opts.setCode : { $nin: IGNORED_SETS } };
    const count = await CardModel.countDocuments(query);
    const cursor = CardModel.find(query, { id: 1, legalities: 1, name: 1, set_code: 1, released_at: 1, rarity: 1, reserved: 1 }).lean().batchSize(5000).cursor();

    const calculate = (lean: MinimalCardSchema, mover: CardV1, category: string) => {
      for (const metric of METRICS) {
        for (const period of PERIODS) {
          for (const point of PRICE_POINTS) {

            // Period and metric have an inverted hierarchy in V2
            const movers = result.categories[category][period][metric][point];
            if (isMover(movers, mover, { metric, period, point })) {
              logger.verbose(`${i}/${count} adding mover - ${lean.id}, ${lean.set_code}, ${lean.name} - ${mover.id} ${category} ${period} ${metric} ${point}`);
              setMover(movers, lean, mover, {
                metric, period, point,
                convert: convertV2
              });
            }

          }
        }
      }
    };

    let i = 0;
    await cursor.eachAsync(async (lean: MinimalCardSchema) => {
      try {
        if (i % 1000 === 0) {
          logger.info(`${i}/${count} Processed.`);
        }
        logger.verbose(`${i++}/${count} Processing: ${lean.id}, ${lean.set_code}, ${lean.name}`);

        const mover = await CardModelV1.findOne({ scryfall_id: lean.id });
        for (const cat of categories) {
          if (cat === 'all') {
            calculate(lean, mover, cat);
          } else if (cat === 'interests') {
            if (!isAnnoyance(lean)) {
              calculate(lean, mover, cat);
            }
          } else {
            // Process formats
            if (isLegal(lean, cat)) {
              if (cat === 'standard') {
                if (!isAnnoyance(lean) && isPossiblyStandard(lean)) {
                  calculate(lean, mover, cat);
                } else {
                  logger.verbose(`${i}/${count} IGNORED - Annoyance in ${cat} - ${lean.id}, ${lean.set_code}, ${lean.name}`);
                }
              } else if (cat !== 'vintage' && !isAnnoyance(lean)) {
                calculate(lean, mover, cat);
              } else if (cat === 'vintage') {
                calculate(lean, mover, cat);
              }
            } else {
              logger.verbose(`${i}/${count} IGNORED - Not legal in ${cat} - ${lean.id}, ${lean.set_code}, ${lean.name}`);
            }
          }
        }
      } catch (err) {
        logger.error(`${i}/${count} Failed: ${lean.id}, ${lean.set_code}, ${lean.name} %o`, err);
      }
    });

    logger.debug('trends calculated successfully %s', JSON.stringify(result, null, 2));
    logger.info(`processed card price motion summary in ${(Time.getElapsedMs(Time.now(), started) / 1000).toFixed(3)}s`);
    return result;
  }



}
