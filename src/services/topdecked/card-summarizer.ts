// tslint:disable:max-line-length



import { CardSchema, SummaryPricePointSchema, SummaryPricesSchema } from '@topdecked/common-model';
import { Arrays, Objects, Time } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel } from '../../model';
import { logger } from '../../util/logger';

const EMPTY_AVERAGES: SummaryPricesSchema = {
  instances: 0,
  average: {} as any,
  budget: {} as any,
  highest: {} as any,
  lowest: {} as any
};

export class PriceSummaryService {

  constructor() {
    // nothing yet
  }


  /**
   * Backport Card Prices from V2 to V1
   */
  public async summarize (opts: {
    cardId?: string,
    cardName?: string,
    setCode?: string,
    batchSize?: number,
    pretend?: boolean,
    force?: boolean
  } = {}) {
    await database.ready();

    const oracleIds = [];
    if (opts.cardId) {
      logger.info('Summarizing prices for single card id: ' + opts.cardId);
      oracleIds.push((
        await CardModel.findOne({ id: opts.cardId }).lean()
        ?? await CardModel.findOne({ oracle_id: opts.cardId }).lean()
      )?.oracle_id);
    } else if (opts.cardName) {
      oracleIds.push(...Arrays.uniqueValues((await CardModel.find({ name: opts.cardName }).lean()).map(c => c.oracle_id)));
    } else if (opts.setCode) {
      oracleIds.push(...Arrays.uniqueValues((await CardModel.find({ set_code: opts.setCode }).lean()).map(c => c.oracle_id)));
    } else {
      oracleIds.push(...await CardModel.distinct('oracle_id'));
    }


    let i = 0;
    for (const oracle_id of oracleIds) {
      const cards = await CardModel.find({ oracle_id }).lean();
      const priceSummary = this.getPriceSummary(cards.filter(c => !!c.prices));
      const gamesAvailable = this.getGamesAvailable(cards);

      logger.debug(`update: ${oracle_id} %o %o`, gamesAvailable, priceSummary);

      if (!opts.pretend) {
        const result = await CardModel.updateMany({ oracle_id }, {
          $set: {
            'prices.summary': priceSummary,
            'games_available': gamesAvailable
          }
        }).exec();

        logger.debug('update result: %o', result);
      }

      logger.verbose(`${i++} Summary: %o`, priceSummary);
    }

  }

  // FIXME: Refactor to shared library. Search 9f7ds87fSD
  getGamesAvailable (cards: CardSchema[]): string[] {
    return Arrays.uniqueValues(Arrays.concat(...(cards ?? []).map(c => c.games ?? [])));
  }

  // FIXME: Refactor to shared library. Search 9f7ds87fSD
  getPriceSummary (cards: CardSchema[]): SummaryPricesSchema {

    logger.verbose(`summarizing: oid:${cards?.[0]?.oracle_id} id:${cards?.[0]?.id} ${cards?.[0]?.name}`);

    const points: (keyof SummaryPricePointSchema)[] = ['eur', 'eur_foil', 'usd', 'usd_foil', 'usd_etched', 'tix', 'tix_foil'];

    const summary = Objects.clone(EMPTY_AVERAGES);
    const average: SummaryPricePointSchema = {} as any;
    const averageInstances: Record<string, number> = {};

    summary.instances = cards.length;

    const WORLD_CHAMPIONSHIP_REGEX = /^wc\d{2}$/;
    const ANNIV_ED_REGEX = /^[tp]?30[ahmt]{1}$/;

    const playable = cards.some(c => c.games?.includes('paper'));
    const playableCards = (playable ? cards.filter(c =>
      c.games?.includes('paper')
      && !ANNIV_ED_REGEX.test(c.set_code)
      && !WORLD_CHAMPIONSHIP_REGEX.test(c.set_code) // World championship decks (not legal)
    ) : cards);

    const OUTLIERS = ['lea', 'leb', '2ed', 'ced', 'cei', 'fbb', 'sum', 'ptk',];
    const outliers = playableCards.filter(p => OUTLIERS.includes(p.set_code)
      || WORLD_CHAMPIONSHIP_REGEX.test(p.set_code) // World championship decks (not legal)
      || ANNIV_ED_REGEX.test(p.set_code) // World championship decks (not legal)
      || !p.games?.includes('paper') // World championship decks (not a real card)
      || p.oversized // Oversize cards (not a real card)
      || p.promo // Promo cards (tend to throw off prices)
      || p.promo_types?.includes('serialized')
      || Time.before(p.released_at, new Date(Date.parse('1994-01-01')))  // Cards that aren't released?
    );
    const inliers = playableCards.filter(p => !outliers.some(o => o.id === p.id));

    const isOutlier = (c: CardSchema): boolean => {
      if (!inliers.length) {
        return false;
      }
      return outliers.some(o => o.id === c.id);
    }
    for (const c2 of playableCards) {
      if (c2?.prices?.tcgplayer || c2?.prices?.scryfall) {

        const getInstancePrice = (point: keyof SummaryPricePointSchema, opts?: { range: keyof SummaryPricesSchema }) => {
          const foil = point.includes('foil');
          const etched = point.includes('etched');
          const sflOnly = point.includes('tix') || point.includes('eur');

          const tcg = c2.prices?.tcgplayer?.prices ?? {};
          const tcg_etched = c2.prices?.tcgplayer?.prices_etched ?? {};
          const tcg_foil = c2.prices?.tcgplayer?.prices_foil ?? {};
          const sfl = c2.prices?.scryfall ?? {};

          const ckRange = (opts.range === 'highest') ? '' : ((['average', 'budget'].includes(opts.range)) ? 'mid' : 'low')
          const ckPrice = c2.prices?.cardkingdom?.[`${point}_${ckRange}`];

          const sflPrice = sfl[point] ?? -1;
          const tcgEtchedPrice = opts?.range === 'lowest' ? (tcg_etched.low ?? tcg_etched.market) : (tcg_etched.market ?? tcg_etched.low);
          const tcgFoilPrice = opts?.range === 'lowest' ? (tcg_foil.low ?? tcg_foil.market) : (tcg_foil.market ?? tcg_foil.low);
          const tcgNormalPrice = opts?.range === 'lowest' ? (tcg.low ?? tcg.market) : (tcg.market ?? tcg.low);
          const tcgPrice = etched ? (tcgEtchedPrice ?? sflPrice) : ((foil ? tcgFoilPrice : tcgNormalPrice) ?? sflPrice) ?? -1;

          return sflOnly ? (sflPrice) : (tcgPrice ?? ckPrice);
        };


        const updateAverage = (point: keyof SummaryPricePointSchema, price: number) => {
          if (price > 0) {
            averageInstances[point] = 1 + (averageInstances[point] ?? 0);
            average[point] = Math.round(((average[point] ?? 0) + price) * 100) / 100;
          }
        };

        const updatePoint = (
          range: keyof SummaryPricesSchema,
          point: keyof SummaryPricePointSchema,
          price: number) => {

          const minMax = range === 'highest' ? Math.max : Math.min;
          if (price > 0) {
            summary[range][point] = Math.round(minMax(price, summary[range][point] ?? price) * 100) / 100;
          }
        };

        for (const point of points) {
          const lowest = getInstancePrice(point, { range: 'lowest' });
          const budget = getInstancePrice(point, { range: 'budget' });
          const highest = getInstancePrice(point, { range: 'highest' });
          const average = getInstancePrice(point, { range: 'average' });

          if (logger.isDebugEnabled() && (lowest > 0 || highest > 0)) {
            const arr = (length: number) => Array.apply(null, Array(length)).map(function () { });
            const pad = (value: string, length: number) => (value.length < length) ? (value + arr(length - value.length).map(() => ' ').join('')) : value
            logger.debug(`${pad(point, 8)}: lowest - ${lowest},\tbudget - ${budget},\taverage - ${average},\thighest - ${highest}`)
          }

          updatePoint('budget', point, budget);
          updatePoint('lowest', point, lowest);
          updatePoint('highest', point, highest);

          if (['tix', 'tix_foil'].includes(point) || !isOutlier(c2)) {
            updateAverage(point, average);
          }
        }

      }
    }

    for (const point of points) {
      const rounded = Math.round((average[point] ?? 0) / (averageInstances[point] ?? 1) * 100) / 100;
      average[point] = average[point] ? rounded : undefined;
      Objects.deleteNils(average);
    }

    const update: SummaryPricesSchema = {
      ...summary,
      average,
      updated: Time.now()
    };

    return update;
  }

}
