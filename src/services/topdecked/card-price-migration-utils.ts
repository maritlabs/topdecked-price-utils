import { CardV1, PriceMotionSnapshotSchemaV1 } from '@topdecked/common-model';
import { HasDate } from '@topdecked/common-model/build/main/models/base-schema';

import { logger } from '../../util/logger';

const PRICE_MOTION_SNAPSHOT: () => PriceMotionSnapshotSchemaV1 = () => ({
  high: 0,
  market: 0,
  mid: 0,
  low: 0,
  etched: 0,
  foil: 0,
  euro: 0,
  euro_foil: 0,
  tix: 0,
  tix_foil: 0
});

const PRICE_POINTS = Object.keys(PRICE_MOTION_SNAPSHOT());



const round = (x: number) => Math.round(x * 100) / 100;

const calculations = {
  percent: (curr: number, prev: number) => {
    curr = curr || 0;
    prev = prev || 0;
    if (prev === 0 && curr === 0) {
      return 0;
    }
    return round(((curr - prev) / (prev ?? curr)) * 100);
  },

  difference: (curr: number, prev: number) => {
    curr = curr || 0;
    prev = prev || 0;
    return round(curr - prev);
  }
};


export class PriceMigrationUtils {
  public updateTrends (card: CardV1, opts: {
    force?: boolean
  } = {}) {

    const TODAY = new Date();
    const MAX_DAILY_POINTS = 14; // days
    const MAX_WEEKLY_POINTS = 8; // weeks

    try {
      if (!card) {
        return;
      }

      if (!card.priceTrends ||
        !card.priceTrends.daily ||
        !card.priceTrends.weekly ||
        !card.priceTrends.monthly) {
        card.priceTrends = {
          daily: [],
          weekly: [],
          monthly: []
        };
      }

      for (const period of ['daily', 'weekly', 'monthly']) {
        if (card.priceTrends[period].length > 0) {
          if (card.priceTrends[period][card.priceTrends[period].length - 1].date.toDateString() === TODAY.toDateString()) {
            if (!opts.force) {
              logger.info(`Price motion current for ${card.id}, ${card.id}`);
              return;
            } else {
              (card.priceTrends[period] as any[]).splice(card.priceTrends[period].length - 1, 1);
            }
          }
        }
      }

      if (!card.pricesSet) {
        logger.warn('Set prices not found for card %s %s', card.id, card.name);
        logger.debug('%o', card);
      } else {
        const point: PriceMotionSnapshotSchemaV1 | HasDate = {
          date: TODAY,
          low: card.pricesSet.lowprice,
          market: card.pricesSet.marketprice,
          mid: card.pricesSet.avgprice,
          high: card.pricesSet.hiprice,
          etched: card.pricesSet.etchedavgprice,
          foil: card.pricesSet.foilavgprice,
          euro: card.pricesSet.euro,
          euro_foil: card.pricesSet.euro_foil,
          tix: card.pricesSet.tix,
          tix_foil: card.pricesSet.tix_foil
        };

        card.priceTrends.daily.push(point);
        if (card.priceTrends.daily.length > MAX_DAILY_POINTS) {
          card.priceTrends.daily.splice(0, 1);
        }

        if (TODAY.getDay() === 0) {
          card.priceTrends.weekly.push(point);
          if (card.priceTrends.weekly.length > MAX_WEEKLY_POINTS) {
            card.priceTrends.weekly.splice(0, 1);
          }
        }

        if (TODAY.getDate() === 1) {
          card.priceTrends.monthly.push(point);
        }
      }

      this.updatePriceTrendMotion(card);

    } catch (err) {
      console.error(err);
    }
  }


  private updatePriceTrendMotion (card: CardV1) {
    if (card) {
      const motion = this.calculatePriceTrendMotion(card);
      if (motion) {
        card.priceTrends.motion = motion;
      }
    }
  }


  private calculatePriceTrendMotion (card: CardV1) {
    const today = new Date();
    const result = {
      date: today,
      difference: {
        daily: PRICE_MOTION_SNAPSHOT(),
        weekly: PRICE_MOTION_SNAPSHOT(),
        monthly: PRICE_MOTION_SNAPSHOT(),
      },
      percent: {
        daily: PRICE_MOTION_SNAPSHOT(),
        weekly: PRICE_MOTION_SNAPSHOT(),
        monthly: PRICE_MOTION_SNAPSHOT(),
      }
    };

    if (card && card.priceTrends && card.priceTrends.daily) {
      const daily = card.priceTrends.daily;
      const weekly = card.priceTrends.weekly;

      // Calculate daily & weekly if we have daily entries
      if (daily && daily.length > 1) {
        const current = daily[daily.length - 1];
        const yesterday = daily[daily.length - 2];
        const lastweek = daily[daily.length >= 7 ? daily.length - 7 : 0];

        for (const metric of Object.keys(calculations)) {
          // Daily
          for (const point of PRICE_POINTS) {
            result[metric].daily[point] = calculations[metric](current[point], yesterday[point]);
          }
          // Weekly
          for (const point of PRICE_POINTS) {
            result[metric].weekly[point] = calculations[metric](current[point], lastweek[point]);
          }
        }
      }

      // Calculate monthly if we have weekly entries
      if (weekly && weekly.length > 0) {
        const current = daily[daily.length - 1];
        const lastmonth = weekly[0];

        for (const metric of Object.keys(calculations)) {
          for (const point of PRICE_POINTS) {
            result[metric].monthly[point] = calculations[metric](current[point], lastmonth[point]);
          }
        }
      }
    }

    return result;
  }
}
