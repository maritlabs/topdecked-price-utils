import { CardLegalitySchema, CardSchema, CardV1, Formats } from '@topdecked/common-model';
import { Arrays, Objects, Strings } from '@topdecked/common-util';

import { logger } from '../../util/logger';

export class CardConstantMapper {

  public static layoutToV1 (card: CardSchema) {
    let layout: string = card.layout;

    if (card.set_name.indexOf('Token') > -1) {
      layout = 'token';
    }

    const supportedInV1 = [
      'aftermath',
      'double-faced',
      'flip',
      'leveler',
      'meld',
      'phenomenon',
      'plane',
      'scheme',
      'split',
      'token',
      'vanguard'
    ];

    const layoutMap = {
      'art_series': 'token',
      'adventure': 'split',
      'emblem': 'token',
      'planar': 'plane',
      'double_faced_token': 'token',
      'transform': 'double-faced',
      'modal_dfc': 'double-faced',
      'class': 'normal',
      'saga': 'normal',
      'augment': 'normal',
      'host': 'normal',
    };

    if (!supportedInV1.includes(layout)) {
      if (Objects.hasNestedProperty(layoutMap, layout)) {
        layout = layoutMap[layout];
      } else if (card.oversized) {
        logger.warn('Card is oversized: %s, %s', card.set_code, card.name);
        layout = 'oversized';
      } else if (card.layout === 'normal') {
        layout = 'normal';
      } else {
        logger.error('Card is an unknown layout: %s, %s: %s', card.set_code, card.name, card.layout);
        layout = 'unknown';
      }
    }

    return layout;
  }

  public static multiverseIdToV1 (card: CardSchema, face: number) {
    return card && card.multiverse_ids ? card.multiverse_ids[face || 0] : null;
  }

  public static rarityToV1 (rarity: string) {
    return {
      'common': 'Common',
      'uncommon': 'Uncommon',
      'rare': 'Rare',
      'mythic': 'Mythic Rare',
      'special': 'Special'
    }[rarity];
  }

  public static rarityToV2 (rarity: string) {
    return {
      'Basic Land': 'common',
      'Common': 'common',
      'Uncommon': 'uncommon',
      'Mythic': 'mythic',
      'Mythic Rare': 'mythic',
      'Rare': 'rare',
      'Special': 'special',
    }[rarity] ?? 'common';
  }

  public static fieldsToV1 (v1: CardV1, v2: CardSchema, opts: {
    fields?: string | string[],
    force?: boolean
  } = {}) {
    let result = false;
    for (const field of Arrays.arrayify(opts.fields)) {
      if (!Objects.isNil(v2[field]) && (!Objects.hasNestedProperty(v1, field) || opts.force)) {
        v1[field] = v2[field];
        result = true;
      }
    }
    return result;
  }

  public static legalitiesToV1 (legalities: CardLegalitySchema) {
    const result: { format: string, legality: string }[] = [];

    const legalityToV1 = (l: string) => {
      return {
        'legal': 'Legal',
        'restricted': 'Restricted',
        'not_legal': 'Not Legal',
        'banned': 'Banned'
      }[l];
    };

    for (const key of Object.keys(legalities)) {
      const format = Strings.capitalizeFirst(key);
      if (Formats[key]) {
        const legality = legalityToV1(legalities[key]);
        result.push({ format, legality });
      }
    }

    return result;
  }


  public static legalitiesToV2 (legalities: { format: string, legality: string }[]) {
    const result: { [id: string]: string } = {};

    const legalityToV2 = (l: string) => {
      return {
        'Legal': 'legal',
        'Restricted': 'restricted',
        'Not Legal': 'not_legal',
        'Banned': 'banned'
      }[l];
    };

    for (const l of legalities) {
      const format = (l.format ?? '').toLowerCase();
      const legality = legalityToV2(l.legality);
      if (format.length > 0 && legality) {
        result[format] = legality;
      }
    }

    return result;
  }
}
