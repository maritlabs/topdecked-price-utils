// tslint:disable:max-line-length

// 39117 cards in production V1 database

import { Document } from 'mongoose';

import { CardV1 } from '@topdecked/common-model';
import { CardSchema } from '@topdecked/common-model/build/main/models/cards-v2/card-schema';
import { SetSchema } from '@topdecked/common-model/build/main/models/cards-v2/set-model-schema';
import { Arrays, Objects } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, CardModelV1, SetModel, SetModelV1 } from '../../model';
import { logger } from '../../util/logger';
import { CardBackporter } from './card-backporter';
import { SetBackporter } from './set-backporter';
import { CardConstantMapper } from './v1-v2-constant-mappings';
import { SetCodeMapper } from './v1-v2-set-code-mappings';

const UNLINKED_V1_SETS = {
  scryfall_code: null
};

const LINKABLE_V2_SETS = {
};

const UNLINKED_V2_SETS = {
  ...LINKABLE_V2_SETS,
  topdecked_code: null
};

const UNLINKED_V1_CARDS = {
  scryfall_id: null
};

const UNLINKED_V2_CARDS = {
  topdecked_id: null
};

const LINKED_V1_CARDS = {
  scryfall_id: { $ne: null }
};

const LINKED_V2_CARDS = {
  topdecked_id: { $ne: null }
};

export interface MissingCard {
  id: string;
  code: string;
  name: string;
}

export class CardMigrationService {
  private variationPattern = /(.*?)\s*\(([a-z])\).*/i;
  private collectorNumberPattern = /^\D*(\d+?)\D*^/i;

  constructor() {
    // nothing yet
  }

  /**
   * Diff ALL Sets & Cards
   */
  public async diffAll () {
    await database.ready();
    const unlinkedSetsV1 = await SetModelV1.find(UNLINKED_V1_SETS).sort({ code: 1 }).exec();
    const linkableSetsV2 = await SetModel.find(LINKABLE_V2_SETS).sort({ code: 1 }).exec();
    const unlinkedSetsV2 = await SetModel.find(UNLINKED_V2_SETS).sort({ code: 1 }).exec();

    const v1map = Arrays.toDictionary(unlinkedSetsV1, 'scryfall_code');
    const v2map = Arrays.toDictionary(unlinkedSetsV2, 'code');

    logger.verbose('sets:\tv2\t==>\tv1');
    for (const code of Object.keys(v2map)) {
      logger.verbose(`${code}\t=>\t${(v1map[code] || { code: 'unlinked' }).code}`);
    }


    logger.info(`${unlinkedSetsV1.length} unlinked v1 sets.`);
    for (const setV1 of unlinkedSetsV1) {
      logger.verbose(`  ${setV1.code}, ${setV1.name}`);
    }
    logger.info(`${unlinkedSetsV2.length} unlinked v2 sets.`);
    for (const setV2 of unlinkedSetsV2) {
      logger.verbose(`  ${setV2.code}, ${setV2.name}`);
    }

    logger.info(`Checking for unlinked cards...`);

    const cardsV1 = await CardModelV1.find(UNLINKED_V1_CARDS).lean().exec();

    const cardsV2 = await CardModel.find({
      ...UNLINKED_V2_CARDS,
      set_code: { $in: linkableSetsV2.map(s => s.code) },
    }).lean().exec();


    logger.info(`${cardsV1.length} cards in V1 not yet linked:`);
    for (const cV1 of cardsV1) {
      logger.verbose(`  ${cV1.setcode}, ${cV1.name}`);
    }
    logger.info(`${cardsV2.length} cards in V2 not yet linked`);
    for (const cV2 of cardsV2) {
      logger.verbose(`  ${cV2.set_code}, ${cV2.name}`);
    }

    const linkedCardsV1 = await CardModelV1.find(LINKED_V1_CARDS).lean().exec() as unknown as CardV1[];
    const linkedCardsV2 = await CardModel.find(LINKED_V2_CARDS).lean().exec() as unknown as CardSchema[];

    const linkedCardsV1MapScryfallId = Arrays.toDictionary(linkedCardsV1, 'scryfall_id');
    const linkedCardsV1MapTopDeckedId = Arrays.toDictionary(linkedCardsV1, 'id');

    const linkedCardsV2MapScryfallId = Arrays.toDictionary(linkedCardsV2, 'id');
    const linkedCardsV2MapTopDeckedId = Arrays.toDictionary(linkedCardsV2, 'topdecked_id');


    const printV1 = (v1: CardV1, v2: CardSchema) => {
      logger.verbose(`==> v1 card: ${v1.id}\t${v1.setcode}\t${v1.name}\t (v2 id: ${v1.scryfall_id})`);
      if (v2) {
        logger.debug(`!== v2 card: ${v2.topdecked_id}\t${v2.topdecked_set_code}\t${v2.topdecked_name}`);
        logger.debug(`['${v1.id}', '${v2.topdecked_id}']`);
        logger.debug(`['${v1.scryfall_id}', '${v2.id}']`);
      } else {
        logger.debug(`NIL v2 card: --------- Lookup failed, no result.`);
      }
      logger.debug(``);
    };

    const printV2 = (v1: CardV1, v2: CardSchema) => {
      logger.verbose(`==> v2 card: ${v2.id}\t${v2.set_code}\t${v2.name}\t (v1 id: ${v2.topdecked_id})`);
      if (v1) {
        logger.debug(`!== v1 card: ${v1.scryfall_id}\t${v1.scryfall_set_code}\t${v1.scryfall_name} `);
        logger.debug(`['${v1.id}', '${v2.topdecked_id}']`);
        logger.debug(`['${v1.scryfall_id}', '${v2.id}']`);
      } else {
        logger.debug(`NIL v1 card: --------- Lookup failed, no result.`);
      }
      logger.debug(``);
    };

    // Check for v1 non-recyprocal links (our linked card does not point back to us)
    logger.info(`Checking for non-recyprocal links...`);
    logger.verbose(`\n\n`);

    let brokenLinksV1 = 0;
    let brokenLinksV1EventuallyConsistent = 0;
    for (const card of linkedCardsV1) {
      const cardV2 = linkedCardsV2MapScryfallId[card.scryfall_id];
      if (!cardV2 || card.id !== cardV2.topdecked_id) {

        // If it's pointing to our own flip-side instance, it's NOT a problem.

        if (cardV2) {
          const linkedV1 = linkedCardsV1MapTopDeckedId[cardV2.topdecked_id];
          if (linkedV1 && Objects.areDeeplyEqual(linkedV1.names, card.names) && linkedV1.setcode === card.setcode) {
            continue;
          } else if (linkedV1) {
            const l2CardV2 = linkedCardsV2MapScryfallId[linkedV1.scryfall_id];
            const l3CardV1 = linkedCardsV1MapTopDeckedId[l2CardV2.topdecked_id];

            if (l2CardV2 && l3CardV1 && l3CardV1.id === l2CardV2.topdecked_id && l3CardV1.scryfall_id === l2CardV2.id) {
              brokenLinksV1EventuallyConsistent++;
            } else {
              logger.error(`card not consistent: ${l2CardV2.set_code} ${l2CardV2.name}, ${l3CardV1.setcode} ${l3CardV1.name}`);
              logger.error(`card not consistent: ${l2CardV2.id} =?= ${l3CardV1.scryfall_id} || ${l2CardV2.topdecked_id} =?= ${l3CardV1.id}`);
            }
          } else {
            logger.error(`linkedV1 not found for topdecked_id: ${cardV2.topdecked_id}`);
          }
        } else {
          logger.error(`cardV2 not found for scryfall_id: ${card.scryfall_id}`);
        }

        brokenLinksV1++;
        printV1(card, cardV2);
      } else if (cardV2 && cardV2.topdecked_name !== card.name) {
        brokenLinksV1++;
        logger.error(`${cardV2.topdecked_name} !== ${card.name}`);
        printV1(card, cardV2);
      }
    }

    logger.info(`${brokenLinksV1} non-reciprocated links in V1 cards (lookup by v1.scryfall_id)`);
    logger.info(`${brokenLinksV1EventuallyConsistent} eventually consistent non-reciprocated links in V1 cards (lookup by v1.scryfall_id)`);
    logger.verbose(`\n\n`);

    // Check for v1 detached links (links that point back to our card but are not the one we expected)
    let detachedLinksV1 = 0;
    let detachedLinksV1EventuallyConsistent = 0;
    for (const card of linkedCardsV1) {
      const cardV2 = linkedCardsV2MapTopDeckedId[card.id];
      if (!cardV2 || card.id !== cardV2.topdecked_id) {

        // If it's pointing to our own flip-side instance, it's NOT a problem.

        const linkedV2 = linkedCardsV2MapScryfallId[card.scryfall_id];
        if (linkedV2) {
          const linkedV1 = linkedCardsV1MapTopDeckedId[linkedV2.topdecked_id];
          if (linkedV1 && Objects.areDeeplyEqual(linkedV1.names, card.names) && linkedV1.setcode === card.setcode) {
            continue;
          } else if (linkedV1) {
            const l2CardV2 = linkedCardsV2MapScryfallId[linkedV1.scryfall_id];
            const l3CardV1 = linkedCardsV1MapTopDeckedId[l2CardV2.topdecked_id];

            if (l2CardV2 && l3CardV1 && l3CardV1.id === l2CardV2.topdecked_id && l3CardV1.scryfall_id === l2CardV2.id) {
              detachedLinksV1EventuallyConsistent++;
            } else {
              logger.error(`card not consistent: ${l2CardV2.set_code} ${l2CardV2.name}, ${l3CardV1.setcode} ${l3CardV1.name}`);
              logger.error(`card not consistent: ${l2CardV2.id} =?= ${l3CardV1.scryfall_id} || ${l2CardV2.topdecked_id} =?= ${l3CardV1.id}`);
            }
          } else {
            logger.error(`linkedV1 not found for topdecked_id: ${linkedV2.topdecked_id}`);
          }
        } else {
          logger.error(`linkedV2 not found for scryfall_id: ${card.scryfall_id}`);
        }

        detachedLinksV1++;
        printV1(card, cardV2);
      } else if (cardV2 && cardV2.topdecked_name !== card.name) {
        detachedLinksV1++;
        logger.error(`${cardV2.topdecked_name} !== ${card.name}`);
        printV1(card, cardV2);
      }
    }

    logger.info(`${detachedLinksV1} detached links in V1 cards (lookup: [v1.id not referenced in v2 database])`);
    logger.info(`${detachedLinksV1EventuallyConsistent} eventually consistent detached links in V1 cards (lookup: [v1.id not referenced in v2 database])`);
    logger.verbose(`\n\n`);

    // Check for v2 non-recyprocal links (our linked card does not point back to us)
    let brokenLinksV2 = 0;
    let brokenLinksV2EventuallyConsistent = 0;
    for (const card of linkedCardsV2) {
      const cardV1 = linkedCardsV1MapTopDeckedId[card.topdecked_id] as CardV1;
      if (!cardV1 || card.id !== cardV1.scryfall_id) {
        brokenLinksV2++;
        printV2(cardV1, card);

        if (cardV1) {
          const l2CardV2 = linkedCardsV2MapScryfallId[cardV1.scryfall_id];
          const l3CardV1 = linkedCardsV1MapTopDeckedId[l2CardV2.topdecked_id];

          if (l2CardV2 && l3CardV1 && l3CardV1.id === l2CardV2.topdecked_id && l3CardV1.scryfall_id === l2CardV2.id) {
            brokenLinksV2EventuallyConsistent++;
          } else {
            logger.error(`card not consistent: ${l2CardV2.set_code} ${l2CardV2.name}, ${l3CardV1.setcode} ${l3CardV1.name}`);
            logger.error(`card not consistent: ${l2CardV2.id} =?= ${l3CardV1.scryfall_id} || ${l2CardV2.topdecked_id} =?= ${l3CardV1.id}`);
          }
        } else {
          logger.error(`cardV1 not found for topdecked_id: ${card.topdecked_id}`);
        }
      } else if (cardV1 && cardV1.scryfall_name !== card.name) {
        brokenLinksV2++;
        logger.error(`${cardV1.scryfall_name} !== ${card.name}`);
        printV2(cardV1, card);
      }
    }

    logger.info(`${brokenLinksV2} non-reciprocated links in V2 cards (lookup by v2.topdecked_id)`);
    logger.info(`${brokenLinksV2EventuallyConsistent} eventually consistent non-reciprocated links in V2 cards (lookup by v2.topdecked_id)`);
    logger.verbose(`\n\n`);

    // Check for v2 detached links (links that point back to our card but are not the one we expected)
    let detachedLinksV2 = 0;
    let detachedLinksV2EventuallyConsistent = 0;
    for (const card of linkedCardsV2) {
      const cardV1 = linkedCardsV1MapScryfallId[card.id] as CardV1;
      if (!cardV1 || card.id !== cardV1.scryfall_id) {
        detachedLinksV2++;
        printV2(cardV1, card);

        const linkedV1 = linkedCardsV1MapTopDeckedId[card.topdecked_id];
        const l2CardV2 = linkedCardsV2MapScryfallId[linkedV1.scryfall_id];
        const l3CardV1 = linkedCardsV1MapTopDeckedId[l2CardV2.topdecked_id];

        if (l2CardV2 && l3CardV1 && l3CardV1.id === l2CardV2.topdecked_id && l3CardV1.scryfall_id === l2CardV2.id) {
          detachedLinksV2EventuallyConsistent++;
        } else {
          logger.error(`card not consistent: ${l2CardV2.set_code} ${l2CardV2.name}, ${l3CardV1.setcode} ${l3CardV1.name}`);
          logger.error(`card not consistent: ${l2CardV2.id} =?= ${l3CardV1.scryfall_id} || ${l2CardV2.topdecked_id} =?= ${l3CardV1.id}`);
        }

      } else if (cardV1 && cardV1.scryfall_name !== card.name) {
        detachedLinksV2++;
        logger.error(`${cardV1.scryfall_name} !== ${card.name}`);
        printV2(cardV1, card);
      }
    }

    logger.info(`${detachedLinksV2} detached links in V2 cards (lookup: [v2.id not referenced in v1 database])`);
    logger.info(`${detachedLinksV2EventuallyConsistent} eventually consistent non-reciprocated links in V2 cards (lookup by v2.topdecked_id)`);
  }


  /**
   * Diff one Set & Cards
   */
  public async diffSet (codeV1: string, codeV2: string) {
    await database.ready();
    const setV1 = await SetModelV1.findOne({ code: codeV1 }).exec();
    const setV2 = await SetModel.findOne({ code: codeV2 }).exec();
    logger.info('v1, v2 %o, %o', setV1, setV2);
  }

  /**
   * Backport All Sets & Cards from V2 to V1
   */
  public async backportMissingCards (force: boolean) {
    // TODO: Implement this

    const setsToBackport: { [id: string]: SetSchema & Document } = {};

    const linkableSetsV2 = await SetModel.find(LINKABLE_V2_SETS).sort({ code: 1 }).exec();
    const cardsV2 = await CardModel.find({
      ...UNLINKED_V2_CARDS,
      set_code: { $in: linkableSetsV2.map(s => s.code) },
    }).select('+metadata').exec();

    logger.info(`backporting ${Object.keys(cardsV2).length} cards`);
    for (const card of cardsV2) {
      const set = setsToBackport[card.set_code];
      if (!set) {
        const setV1 = await SetModelV1.findOne({ scryfall_code: card.set_code }).exec();
        if (!setV1) {
          setsToBackport[card.set_code] = await SetModel.findOne({ code: card.set_code }).exec();
        }
      }

      const cb = new CardBackporter();

      const cardV1 = await cb.card2toCard1(card);
      if (card && (!card.topdecked_id || force)) {
        card.topdecked_id = cardV1.id;
        card.topdecked_name = cardV1.name;
        card.topdecked_set_code = cardV1.setcode;
        card.metadata.backported = true;
        card.markModified('metadata');
        await card.save();
      }
    }

    logger.info(`backporting ${Object.keys(setsToBackport).length} sets`);
    for (const set of Object.values(setsToBackport)) {
      await this.backportSet(set.code, force);
    }
    logger.info('backport complete');
  }

  /**
   * Backport Set & Cards from V2 to V1
   */
  public async backportSet (code: string, force: boolean) {
    const sb = new SetBackporter();
    logger.verbose('checking for existing set %s', code);

    logger.info('backport begin...');

    if (force) {
      await SetModelV1.remove({
        $or: [
          { code },
          { scryfall_code: code }
        ],
        source: 'imported'
      });
    }
    await sb.set2toSet1(code);

    if (force) {
      await CardModelV1.remove({
        $or: [
          { setcode: code },
          { scryfall_set_code: code }
        ],
        source: 'imported'
      }).exec();
    }

    const cb = new CardBackporter();

    logger.verbose('finding cards');
    const cardsV2 = await CardModel.find({ set_code: code }).exec();
    logger.debug('found cards, %o', cardsV2);
    for (const card of cardsV2) {
      const cardV1 = await cb.card2toCard1(card);
      if (card && (!card.topdecked_id || force)) {
        card.topdecked_id = cardV1.id;
        card.topdecked_name = cardV1.name;
        card.topdecked_set_code = cardV1.setcode;
        await card.save();
      }
    }

    logger.info('backport complete');
  }

  public async backportFields (opts: {
    fields?: string | string[],
    force?: boolean,
    ignoreCode?: string | string[]
    setCode?: string
  } = {}) {
    const query = {
      set_code: opts.setCode ? opts.setCode : { $exists: true, $ne: null, $nin: Arrays.arrayify(opts.ignoreCode) },
      topdecked_id: { $exists: true, $ne: null }
    };

    const getFields = (c: CardV1 | CardSchema) => {
      const result = {};
      for (const field of Arrays.arrayify(opts.fields)) {
        result[field] = c[field];
      }
      return result;
    };

    logger.debug('%o', query);
    const cardsV2 = await CardModel.find(query).lean().exec() as unknown as CardSchema[];
    let i = 0;
    for (const v2 of cardsV2) {
      logger.verbose(`${i} backporting %s %s, %o`, v2.id, v2.name, getFields(v2));
      const v1 = await CardModelV1.findOne({ id: v2.topdecked_id }).exec();
      const modified = CardConstantMapper.fieldsToV1(v1, v2, {
        fields: opts.fields,
        force: opts.force
      });

      if (modified || opts.force) {
        await v1.save();
        logger.verbose(`${i++} fields saved for card %s %s, %o`, v1.id, v1.name, getFields(v1));
      }
    }
  }

  public async backportLegalities (opts: {
    ignoreCode?: string | string[]
    setCode?: string
  } = {}) {
    const query = {
      set_code: opts.setCode ? opts.setCode : { $exists: true, $ne: null, $nin: Arrays.arrayify(opts.ignoreCode) },
      topdecked_id: { $exists: true, $ne: null }
    };

    logger.debug('%o', query);
    const cardsV2 = await CardModel.find(query).lean().exec() as unknown as CardSchema[];
    let i = 0;
    for (const v2 of cardsV2) {
      const v1 = await CardModelV1.findOne({ id: v2.topdecked_id }).exec();
      v1.legalities = CardConstantMapper.legalitiesToV1(v2.legalities);
      await v1.save();
      logger.verbose(`${i++} legalities saved: ${v1.setcode}, ${v1.name}`);
      logger.debug(JSON.stringify(v1.legalities));
    }
  }

  /**
   * Clear all Set and Card links between V1 and V2 databases
   */
  public async clearLinks () {
    logger.info('Cleared links in V1 cards database: %o', await CardModelV1.updateMany({
      $or: [
        { scryfall_id: { $ne: null } },
        { scryfall_name: { $ne: null } },
        { scryfall_set_code: { $ne: null } },
      ]
    }, {
      $unset: {
        scryfall_id: '',
        scryfall_name: '',
        scryfall_set_code: ''
      }
    }).exec());

    logger.info('Cleared links in V2 cards database: %o', await CardModel.updateMany({
      $or: [
        { topdecked_id: { $ne: null } },
        { topdecked_name: { $ne: null } },
        { topdecked_set_code: { $ne: null } },
      ]
    }, {
      $unset: {
        topdecked_id: '',
        topdecked_name: '',
        topdecked_set_code: '',
        'metadata.backported': ''
      }
    }).exec());

    logger.info('Cleared links in V1 sets database: %o', await SetModelV1.updateMany({
      $or: [
        { scryfall_code: { $ne: null } },
      ]
    }, {
      $unset: { scryfall_code: '' }
    }).exec());

    logger.info('Cleared links in V2 sets database: %o', await SetModel.updateMany({
      $or: [
        { topdecked_code: { $ne: null } },
      ]
    }, {
      $unset: { topdecked_code: '' }
    }).exec());
  }


  /**
   * Remove all backported sets and cards from V1 to V2
   */
  public async clearBackportedSetsCards () {
    logger.info('clearing backported cards and sets');
    logger.info('Cleared imported cards: %o', await CardModelV1.deleteMany({ source: 'imported' }).exec());
    logger.info('Cleared imported sets: %o', await SetModelV1.deleteMany({ source: 'imported' }).exec());

    logger.info('Cleared backported links in V2 cards database: %o', await CardModel.updateMany({
      'metadata.backported': true
    }, {
      $unset: {
        topdecked_id: '',
        topdecked_name: '',
        topdecked_set_code: '',
        'metadata.backported': ''
      }
    }).exec());
  }

  /**
   * Link All Sets & Cards between V1 and V2 databases
   */
  public async linkAll (force: boolean): Promise<{
    missingCards: MissingCard[],
    setsFound: string[],
    setsMissing: string[]
  }> {
    logger.verbose(`Beginning migration`);

    const setResults = await this.linkAllSets(force);

    let missingCards: MissingCard[] = [];
    for (const c of setResults.setsMissing) {
      const cards = await CardModelV1.find({ setcode: c }).exec();
      for (const card of cards) {
        missingCards.push({ code: c, name: card.name, id: card.id });
      }
    }

    for (const originalSet of setResults.setsFound) {
      const cardsMissing = await this.linkSetCards(originalSet, {
        force
      });
      missingCards = missingCards.concat(cardsMissing);
    }

    this.printMissingCards(missingCards);

    return {
      ...setResults,
      missingCards
    };
  }

  public async linkAllSets (force: boolean) {

    const setsV1 = await SetModelV1.find({}).exec();
    const setsV1Map = Arrays.toDictionary(setsV1, 'code');

    const setsV2 = await SetModel.find({}).exec();
    const setsV2Map = Arrays.toDictionary(setsV2, 'code');

    const setcodeLinks: { [id: string]: string } = {};

    logger.verbose(`Original sets found ${setsV1.length} sets`);
    logger.verbose(`New sets found ${setsV2.length} sets`);

    const setsFound: string[] = [];
    const setsMissing: string[] = [];
    for (const codeV1 of Object.keys(setsV1Map)) {
      const potentialsV2 = SetCodeMapper.setCodeToSet2(codeV1);
      let found = false;
      for (const codeV2 of potentialsV2) {
        const setV2 = setsV2Map[codeV2];
        if (setV2 && !found) {
          setsFound.push(codeV1);
          setcodeLinks[codeV1] = codeV2;
          found = true;

          const setV1 = setsV1Map[codeV1];
          if (!setV1.scryfall_code) {
            setV1.scryfall_code = codeV2;
            await setV1.save();
          }

          if (!setV2.topdecked_code) {
            setV2.topdecked_code = codeV1;
            await setV2.save();
          }
          logger.verbose(`Linked sets:\t${codeV1}\t<=>\t${codeV2}`);
        }
      }
      if (!found) {
        setsMissing.push(codeV1);
        logger.warn(`Missing ${codeV1} potentials: %o`, potentialsV2);
      }
    }
    logger.info(`Found ${setsFound.length} sets`);
    logger.debug(`%o`, setsV2Map);
    logger.info(`Missing ${setsMissing.length} sets %o`, setsMissing);

    return {
      setsFound,
      setsMissing,
      setcodeLinks,
    };
  }


  /**
   * Lets Sets & Cards between V1 and V2 databases
   */
  public async linkSetCards (set1code: string, opts: { force?: boolean, verbose?: boolean } = {}): Promise<{ name: string, code: string, id: string }[]> {

    await database.ready();
    const missingCards: MissingCard[] = [];
    let multipleCards = [];
    const cardsV1Query = {
      ...UNLINKED_V1_CARDS,
      setcode: set1code
    };
    if (opts.force) {
      delete cardsV1Query['scryfall_id'];
    }
    const cardsV1 = await CardModelV1.find(cardsV1Query).exec();

    logger.info(`linking ${cardsV1.length} cards for set ${set1code}`);

    const processFoundCardsResult = async (card1: CardV1, cards2: CardSchema[]) => {
      let backside = false;
      if (card1.names && card1.names.length > 1 && card1.number && card1.number.indexOf('b') > -1) {
        backside = true;
      }

      let found = false;
      if (cards2.length > 0) {
        if (cards2.length === 1) {
          found = true;
          const card2 = cards2[0];
          if (!card2.topdecked_id && !backside) {
            await CardModel.updateOne({ id: card2.id }, {
              topdecked_id: card1.id,
              topdecked_name: card1.name,
              topdecked_set_code: card1.setcode
            }).exec();
            logger.verbose(`linked ${card1.setcode} ${card1.name} <== ${card2.set_code} ${card2.name} (bidirectional)`);
          } else {
            logger.warn('Card already has topdecked_id, skipping link on V2 side:',
              '\n\tcard v2 sf:', '\t', card2.set_code, '\t', card2.name, '\t', card2.id,
              '\n\tcard v2 td:\t', card2.topdecked_set_code, '\t', card2.topdecked_name, '\t', card2.topdecked_id,
              '\n\tcard v1 sf:\t', card1.scryfall_set_code, '\t', card1.scryfall_name, '\t', card1.scryfall_id,
              '\n\tcard v1 td:\t', card1.setcode, '\t', card1.name, '\t', card1.id);
          }
          await CardModelV1.updateOne({ id: card1.id }, {
            scryfall_id: card2.id,
            scryfall_name: card2.name,
            scryfall_set_code: card2.set_code
          }).exec();

          logger.verbose(`linked ${card1.setcode} ${card1.name} ==> ${card2.set_code} ${card2.name}`);
        } else {
          logger.warn('More than one potential match found for %s %s, %o:', card1.id, card1.name, cards2.map(c => ({ name: c.name, id: c.id })));
          multipleCards = multipleCards.concat({ card: card1, potentials: cards2 });
        }
      }
      return found;
    };


    CARD: for (const card1 of cardsV1) {
      logger.verbose('processing card %s %s %s', card1.setcode, card1.name, card1.number);

      const card2name = this.cardToCard2Name(card1);
      let found = false;

      if (card1.multiverseid) {
        const query = { name: card2name, multiverse_ids: card1.multiverseid };
        const cards2 = await CardModel.find(query).exec();
        found = await processFoundCardsResult(card1, cards2);
        if (found) {
          logger.debug(`found via multiverse_id: ${card1.setcode}, ${card1.name} --> ${card1.multiverseid}`);
        }
      }

      if (!found) {
        if (card1.id) {
          const query = { id: card1.id.slice(0, 36) };
          const cards2 = await CardModel.find(query).exec();
          found = await processFoundCardsResult(card1, cards2);
          if (found) {
            logger.debug(`found via straight up id: ${card1.setcode}, ${card1.name} --> ${card1.multiverseid}`);
          }
        }
      }

      if (!found) {
        const set2codes = SetCodeMapper.setCodeToSet2(set1code);
        const query = { name: card2name, set_code: { $in: set2codes } };
        const cards2 = await CardModel.find(query).exec();
        found = await processFoundCardsResult(card1, cards2);
        if (found) {
          logger.debug(`found via set_code: ${card1.setcode}, ${card1.name} --> ${set2codes}`);
        }
      }


      if (!found) {
        let set2codes = SetCodeMapper.setCodeToSet2(set1code);
        const printings2 = await CardModel.find({ name: card2name }).exec();
        logger.debug(`Querying for: ${card2name}, %o`, printings2.map(p => p.set_code + ' ' + p.collector_number));

        const preReleaseCodes = [];
        if (['pPRE', 'pLPA', 'pMGD', 'pMEI'].includes(set1code)) {
          let updatedSet2Code = [];
          if (card1.name === 'Plains' && set1code === 'pPRE') {
            updatedSet2Code = ['pdgm'];
          } else {
            for (const card2 of printings2) {
              if (card2.set_code.match(/^[^p]\w{2}$/)) {
                for (const pre of printings2) {
                  if (pre.set_code === 'p' + card2.set_code) {
                    updatedSet2Code.push(pre.set_code);
                    preReleaseCodes.push(pre.set_code);
                  }
                }
              }
            }
          }

          if (updatedSet2Code.length) {
            set2codes = set2codes.concat(updatedSet2Code).concat(['plpa', 'ppre']);
            set2codes = Arrays.uniqueValues(set2codes);
          }
        }

        if (card1.number) {
          let collector_number = card1.number.trim();
          const variation = this.cardToCard2Variation(card1);
          if (variation !== '' && card1.number.indexOf(variation) === -1) {
            collector_number = card1.number + variation;
          }


          for (const card2 of printings2) {
            for (const set2code of set2codes) {
              if (card2.collector_number === collector_number && card2.set_code === set2code) {
                found = await processFoundCardsResult(card1, [card2]);
                if (found) {
                  logger.debug(`found via number: ${card1.setcode}, ${card1.name} --> ${card2.set_code}, ${card2.name}`);
                }
              }
            }
          }
        }

        if (!found && card1.mciNumber) {
          let collector_number = card1.mciNumber;
          const variation = this.cardToCard2Variation(card1);
          if (variation !== '' && collector_number.indexOf(variation) === -1) {
            collector_number = collector_number + variation;
          }

          for (const card2 of printings2) {
            for (const set2code of set2codes) {
              if (card2.collector_number === collector_number && card2.set_code === set2code) {
                found = await processFoundCardsResult(card1, [card2]);
                if (found) {
                  logger.debug(`found via mciNumber: ${card1.setcode}, ${card1.name} --> ${card2.set_code}, ${card2.name}`);
                }
              }
            }
          }
        }

        if (!found && preReleaseCodes.length > 0) {
          for (const card2 of printings2) {
            for (const set2code of set2codes) {
              if (card2.set_code === set2code) {
                found = await processFoundCardsResult(card1, [card2]);
                if (found) {
                  logger.debug(`found via preRelease fallback: ${card1.setcode}, ${card1.name} --> ${card2.set_code}, ${card2.name}`);
                }
              }
            }
          }
        }

        const matchBy = async (field1, field2, compareFn: (l, r) => boolean) => {
          if (!found && card1[field1]) {
            const potentials = [];
            for (const card2 of printings2) {
              for (const set2code of set2codes) {
                if (card2.set_code === set2code
                  && card1[field1] && card2[field2]
                  && compareFn(card1[field1], card2[field2])
                ) {
                  potentials.push(card2);
                }
              }
            }
            found = await processFoundCardsResult(card1, potentials);
            if (found) {
              logger.debug(`found via field ${field1}-->${field2}: ${card1.setcode}, ${card1.name} = [${card1[field1]}]`);
            }
          }
        };

        await matchBy('artist', 'artist', (l, r) => {
          return l.trim().toLowerCase() === r.trim().toLowerCase();
        });
        await matchBy('releaseDate', 'released_at', (l, r) => {
          return new Date(l.trim().toLowerCase()).getTime() === r.getTime();
        });

        if (!found) {
          const manualMatch = async (id) => {
            found = await processFoundCardsResult(card1, await CardModel.find({ id }).exec());
            if (found) {
              logger.debug(`found via manual: ${card1.setcode}, ${card1.name} --> ${id}`);
            }
          };

          // Hacks -- these might not be exact matches, but they were done by hand so they should be "close enough".

          // Misspelled Sultai Ascenda(-n)cy
          if (card1.id === '39e8a45a9fa49a198eeed4ddb312985d87c87cf5') { await manualMatch('32f4c02f-9a64-45b5-8c2e-d34b0ff3e54b'); }

          if (card1.setcode === 'ITP' && card1.name === 'Forest') { await manualMatch('f9f4448e-076b-4add-828f-6f218bef8a93'); }
          if (card1.setcode === 'ITP' && card1.name === 'Island') { await manualMatch('44178108-9c6e-4095-b248-089c826f9e18'); }
          if (card1.setcode === 'ITP' && card1.name === 'Mountain') { await manualMatch('f7a2af07-ef48-4492-bd2e-53320a26a89a'); }
          if (card1.setcode === 'ITP' && card1.name === 'Plains') { await manualMatch('9919afe3-b65e-4ec6-8a4f-9adbe5682ac7'); }
          if (card1.setcode === 'ITP' && card1.name === 'Swamp') { await manualMatch('7bfc4649-734d-4140-821c-bfff56c5decf'); }

          if (card1.setcode === 'pFNM' && card1.name === 'Goblin Warchief') { await manualMatch('153abbb7-85e7-4d43-a62e-541aa6d9c8b2'); }
          if (card1.setcode === 'pHHO' && card1.name === 'Gifts Given') { await manualMatch('ba72c564-0ca6-4f52-b19a-175646965291'); }
          if (card1.setcode === 'pHHO' && card1.name === 'Stocking Tiger') { await manualMatch('616af376-26a2-4302-8a0e-e922634602f4'); }
          if (card1.setcode === 'pJGP' && card1.name === 'Vampiric Tutor') { await manualMatch('f9ab4647-e025-4a63-b2c1-a90541ab17d1'); }
          if (card1.setcode === 'pMGD' && card1.name === 'Languish') { await manualMatch('69923060-9015-41ea-91ad-f3252ba25c85'); }

          if (card1.setcode === 'pMEI' && card1.name === 'Alhammarret, High Arbiter') { await manualMatch('028ed496-6472-490b-8b1d-fe3c667fb7ba'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Chandra, Pyromaster' && card1.id === '4be2b58eac38fd16e934d40a5edce3b1fe6a55f4') { await manualMatch('53ce8381-4cbb-4bf9-bdac-3b2375a46340'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Chandra, Pyromaster' && card1.id === '72b95fa5d1a0a7a726753e3cfad2d24a78fde888') { await manualMatch('0947a15a-957a-4858-8143-5fa3d286e6d9'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Consume Spirit') { await manualMatch('f267137c-57f5-43b1-9a4b-98f2b84106b9'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Dwynen, Gilt-Leaf Daen') { await manualMatch('ee9797ee-58a4-4d8d-8c67-aff2cd61c88d'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Hixus, Prison Warden') { await manualMatch('98599d53-35f6-489f-8bce-9525615bcb20'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Kothophed, Soul Hoarder') { await manualMatch('3086d1f8-30f1-4aa8-89c7-ce6029ee5613'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Liliana Vess' && card1.id === '5471d2a45de359f9ba4ad034e3b8f4b2afd40608') { await manualMatch('8a628087-9179-49eb-87a5-f67cfd0a814c'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Liliana Vess' && card1.id === 'a564054a0686984f4c7448206610530fda2137e3') { await manualMatch('58041a68-19f0-4343-9102-fb86fb817b07'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Pia and Kiran Nalaar') { await manualMatch('2eb6fc54-77e7-4e90-abc9-fd88581b57bf'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Hamletback Goliath') { await manualMatch('13517c52-76fd-469e-a2c5-d2779e9dc298'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Brion Stoutarm') { await manualMatch('79d68ed0-1397-4381-afb5-c594fd40ee00'); }

          if (card1.setcode === 'pPRE' && card1.name === 'Barrage Tyrant') { await manualMatch('adf81fdf-0499-4deb-a658-0cdf03595e99'); }
          if (card1.setcode === 'pPRE' && card1.name === 'Alhammarret, High Arbiter') { await manualMatch('9120b615-dc31-4623-9cbd-d4cb99ae9f6f'); }
          if (card1.setcode === 'pPRE' && card1.name === 'Languish') { await manualMatch('76947376-7afd-4792-b0f7-c3bc0423ef2f'); }

          if (card1.setcode === 'RQS' && card1.name === 'Island') { await manualMatch('19be2858-b22d-4bab-a3af-532e952c88de'); }
          if (card1.setcode === 'RQS' && card1.name === 'Forest') { await manualMatch('374f7c5e-d788-4c8a-8be7-3448e33f7009'); }
          if (card1.setcode === 'RQS' && card1.name === 'Mountain') { await manualMatch('af780463-7111-45f9-8b62-9d83eb01258b'); }
          if (card1.setcode === 'RQS' && card1.name === 'Plains') { await manualMatch('459a5d4b-50df-4575-8033-1ef1732611a2'); }
          if (card1.setcode === 'RQS' && card1.name === 'Swamp') { await manualMatch('a852cdb1-eaf3-4f27-a26b-4cd7dfe67c1a'); }

          if (card1.setcode === 'S00' && card1.name === 'Forest') { await manualMatch('dc477b44-a7b3-4dda-b897-6f164e28541b'); }
          if (card1.setcode === 'S00' && card1.name === 'Island') { await manualMatch('b3f9ffc8-8115-4858-bd71-deaca9889f72'); }
          if (card1.setcode === 'S00' && card1.name === 'Mountain') { await manualMatch('f2e450ae-3ed8-424f-b974-7d9a8e8ac7e4'); }
          if (card1.setcode === 'S00' && card1.name === 'Plains') { await manualMatch('d3c1ab59-3771-484c-8a79-2bb36655b6dd'); }
          if (card1.setcode === 'S00' && card1.name === 'Swamp') { await manualMatch('0e2a9fa6-3c7c-409a-8565-e3d533107971'); }

          if (card1.setcode === 'ATH' && card1.name === 'Mountain') { await manualMatch('4714b305-c017-4e1f-b788-79331de3dae1'); }
          if (card1.setcode === 'ATH' && card1.name === 'Plains') { await manualMatch('c333ca3a-b1d4-4ec4-a958-c3870a8434e7'); }
          if (card1.setcode === 'ATH' && card1.name === 'Swamp') { await manualMatch('2dd3fac4-488f-4899-ab19-6208188254fc'); }

          if (card1.setcode === 'UNH' && card1.name === 'Curse of the Fire Penguin Creature') { await manualMatch('c01e8089-c3a9-413b-ae2d-39ede87516d3'); }

          if (card1.setcode === 'UGL' && card1.name === 'B.F.M. (Big Furry Monster, Right Side)') { await manualMatch('d15c6375-2e4e-47f7-88e1-d90794e7f724'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Valorous Stance') { await manualMatch('f482213a-4e3e-4e13-82a1-88e7d6c4ba2c'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Seeker of the Way') { await manualMatch('b1e50811-048f-4dc6-a453-a5e36f3eb0a0'); }
          if (card1.setcode === 'pMEI' && card1.name === 'Sandsteppe Citadel') { await manualMatch('7ce265fa-1b23-4d03-8675-6c9447d31115'); }



          // CEI Forest
          if (card1.id === 'c1b449c976eac7def96da80068c10fc2215df494') { await manualMatch('7bbba144-ac7c-43de-9171-6638a91c9f55'); }
          if (card1.id === '393934ed0166f423e60a1755cf39c16c8daceb4c') { await manualMatch('7ee18815-21af-4cc4-bb2c-9ec60d0c30da'); }
          if (card1.id === '3033d9dcc732195e6fa8c452100dae3866ab0b8c') { await manualMatch('589de90a-f48e-4368-ba5d-d8dabcdc61ef'); }
          // CEI Island
          if (card1.id === '1fa87ecd927eb5b6a8b38cfa04f8303a1689341e') { await manualMatch('5846e763-7df9-4255-aac5-d342da0dbd3a'); }
          if (card1.id === '9abd17954791e1e43abb2a04cb7e43c79d8b50b1') { await manualMatch('4ca25840-1e36-4a68-87d0-c76e7fe4298b'); }
          if (card1.id === '9c65abc3434a2f8a780b1b19e3cd519440504ca5') { await manualMatch('57c9e38a-bb57-49bc-a9c3-9ba6d9dd5438'); }
          // CEI Mountain
          if (card1.id === 'f2a408557c85ab65bc1be0209c4f72862d6ede4d') { await manualMatch('5deefab6-4734-464a-b930-64447a8490ca'); }
          if (card1.id === 'b0ae93a57315441642383343b38c51d1aa5b27da') { await manualMatch('ca039e3e-2d55-4b7e-97b4-5c4fca717e00'); }
          if (card1.id === 'dd1b1a2fa92ea46c171aa7a281b0c1f6cc91cb70') { await manualMatch('2d03b35a-e622-4a5a-880c-a0aff969b12d'); }
          // CEI Plains
          if (card1.id === '42b75cf37aa816c4659c16fdd1c4722db4fa1e95') { await manualMatch('0bb3a7d7-6ea9-4207-8f6e-0f50ad5804e0'); }
          // CEI Swamp
          if (card1.id === '78e62f43ad46d4a828b7b1493948e86aff44e7d9') { await manualMatch('8587372d-48d1-4c5e-9e36-28838986536f'); }

          // CED Forest
          if (card1.id === 'cb45bfed658815f4ca935a673988a7e31f89fb57') { await manualMatch('6d426fa8-72e8-4027-9df5-1289b7267a14'); }
          if (card1.id === '53963a16a7964090ca981073489761d7a35b7c0e') { await manualMatch('d06130fa-f4a5-45c1-81e3-25982e49fab1'); }
          if (card1.id === 'd8711289d3223f2e383c71862856d7dad1d80471') { await manualMatch('ba79c5da-12a4-419e-9793-7a85beeee30f'); }
          // CED Island
          if (card1.id === 'fa36af58b0ecb651a7e161537bac2f9daafe93b0') { await manualMatch('a5159732-319d-450c-b5d8-977810090aad'); }
          if (card1.id === '0f1d449fe045409f0d2e7d9e8dcc2ac1ecd00c1a') { await manualMatch('181edf62-34ca-4a55-a2f6-f15940a5b43a'); }
          if (card1.id === 'a4d8910ddf16db8e2812b2d75a83d046d94fd271') { await manualMatch('713dcdc5-bd50-4aef-97c0-375ea4b902cf'); }
          // CED Mountain
          if (card1.id === '6e1cc75b505cd536311cb10909f79c4ef8d0cc48') { await manualMatch('3b37902c-8184-43ce-8669-3a617953dfe6'); }
          if (card1.id === 'da0e54f0365244a022e13fe8c0427d1197bb7bce') { await manualMatch('ad6c52df-ebc4-4f00-bd9b-0cdc69279d20'); }
          if (card1.id === '697e0aa77c013cc55578fe33c987f69b0bdaf720') { await manualMatch('5c6db89c-21ce-4123-955a-310c3978b2fc'); }
          // CED Plains
          if (card1.id === 'c2e4cd45c34fb851dc18b672db26b7f72715e099') { await manualMatch('5549b73a-9abe-4d63-a673-aa08e9f6a678'); }
          // CED Swamp
          if (card1.id === 'b92b9af45e847c24a1a026bfd8bef0d6bea743c8') { await manualMatch('18da8fa9-32e3-48ea-b4ec-461e9bc61392'); }

          // TOKEN
          if (card1.setcode === 'DDD' && card1.name === 'Beast') { await manualMatch('6fd6877b-b4f1-4953-b9a4-c97d0ae7d673'); }


          if (!found) {
            found = await processFoundCardsResult(card1, printings2);
          }
        }
      }

      if (!found) {
        missingCards.push({ code: set1code, name: card2name, id: card1.id });
      }
    }
    if (opts.verbose && multipleCards.length > 0) {
      logger.warn(`Detected ${multipleCards.length} multiple card/setcode uncertainties`);
    }
    if (opts.verbose) {
      if (missingCards.length > 0) {
        this.printMissingCards(missingCards);
      } else {
        logger.info('All cards were linked successfully.');
      }
    }
    return missingCards;
  }



  /**
   * Utility methods for card linking
   */
  public collectorNumberWithoutSuffixes (collector_number: string): string {
    let result = collector_number;
    if (result) {
      result = result.replace(this.collectorNumberPattern, '$1');
    }
    return result;
  }

  public cardToCard2Name (card: any): string {
    if (!card) {
      return '';
    }
    let result: string = card.name;
    if (card.names && card.names.length > 1 && ['aftermath', 'double-faced', 'split', 'flip'].includes(card.layout)) {
      result = card.names.join(' // ');
    } else if (card.names && card.names.length > 1 && ['meld'].includes(card.layout)) {
      result = card.name;
    } else if (card.names && card.names.length > 1) {
      logger.error('Did not know what to do with card names: %o', card.names);
    }

    if (result.indexOf(' token card') > -1) {
      result = result.replace(' token card', '');
    }

    if (result.match(this.variationPattern)) {
      result = result.replace(this.variationPattern, '$1').trim();
    }

    return result;
  }

  public cardToCard2Variation (card: any) {
    let result = '';
    if (card && card.name && card.name.match(this.variationPattern)) {
      result = card.name.replace(this.variationPattern, '$2').trim();
    }
    return result;
  }

  private printMissingCards (missingCards: any[]) {
    if (missingCards && missingCards.length > 0) {
      logger.warn(`${missingCards.length} cards could not be linked.`);
      for (const c of missingCards) {
        logger.verbose('Missing card\t\n' + `'${c.code}' '${c.name}'`);
        logger.debug(`%o`, c);
      }
      logger.warn('Printing code snippits by setcode and name');
      for (const c of missingCards) {
        logger.error(`if (card1.setcode === '${c.code}' && card1.name === '${c.name}') { await manualMatch(''); }`);
      }
      logger.warn('Printing code snippits by ID');
      for (const c of missingCards) {
        logger.error(`if (card1.id === '${c.id}') { await manualMatch(''); }`);
      }
    }
  }

}
