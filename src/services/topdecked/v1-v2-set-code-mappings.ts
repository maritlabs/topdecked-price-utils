import { Arrays } from '@topdecked/common-util';

export class SetCodeMapper {

  public static CODE_TO_CODE2: { [id: string]: string | string[] } = {
    'celd': ['eld'],
    'cpk': ['cp1', 'cp2', 'cp3'],
    'dd3_(\\w{3})': '$1',
    '(gk\\d)_\\w+': '$1',
    'gp1': 'g18',
    'frf_ugin': 'ugin',
    'med2': 'med',
    'mps_akh': 'mp2',
    'nms': ['nem'],
    'van': ['pvan', 'pmoa'],
    'phho': 'hho',
    'parl': [
      'parl',
      'pal99',
      'pal00',
      'pal01',
      'pal02',
      'pal03',
      'pal04',
      'pal05',
      'pal06'
    ],
    'pfnm': [
      'fnm',
      'f01',
      'f02',
      'f03',
      'f04',
      'f05',
      'f06',
      'f07',
      'f08',
      'f09',
      'f10',
      'f11',
      'f12',
      'f13',
      'f14',
      'f15',
      'f16',
      'f17',
      'f18',
    ],
    'pgtw': [
      'pgtw',
      'pg07',
      'pg08'
    ],
    'pjgp': [
      'jgp',
      'g99',
      'g00',
      'g01',
      'g02',
      'g03',
      'g04',
      'g05',
      'g06',
      'g07',
      'g08',
      'g09',
      'g10',
      'g11',
      'j12',
      'j13',
      'j14',
      'j15',
      'j16',
      'j17',
      'j18'
    ],
    'plpa': [
      'plpa',
      'ppre'
    ],
    'pmei': [
      'pbok',
      'pdtp',
      'pdp10',
      'pdp11',
      'pdp12',
      'pdp13',
      'pdp14',
      'pdrc',
      'pidw',
      'pi13',
      'pi14',
      'plpa',
      'pmei',
      'pres',
      'psdc',
      'ps11',
      'ps14',
      'ps15',
      'ps16',
      'ps17',
      'ps18',
      'purl',

      // buy-a-box promos
      'pavr',
      'pbfz',
      'pbng',
      'pdgm',
      'pdka',
      'pdtk',
      'pfrf',
      'pktk',
      'pgtc',
      'pisd',
      'pjou',
      'pmbs',
      'pm10',
      'pm11',
      'pm12',
      'pm13',
      'pm14',
      'pm15',
      'pnph',
      'pogw',
      'pori',
      'proe',
      'prtr',
      'psom',
      'pths',
      'ptkdf',
      'pzen',
      'pwwk',
    ],
    'pmgd': [
      'ppre',
      'thp3',
      'nph'
    ],
    'pmpr': [
      'mpr',
      'phbr',
      'pr2',
      'p03',
      'p04',
      'p05',
      'p06',
      'p07',
      'p08',
      'p09',
      'p10',
      'p11'
    ],
    'po2': 'p02',
    'pwcq': ['pwcq', 'ppro'],
    'pwpn': [
      'fnm',
      'f01',
      'f02',
      'f03',
      'f04',
      'f05',
      'f06',
      'f07',
      'f08',
      'f09',
      'f10',
      'f11',
      'f12',
      'f13',
      'f14',
      'f15',
      'f16',
      'f17',
      'f18',
      'pm10',
      'pwpn',
      'pwp09',
      'pwp10',
      'pwp11',
      'pwp12',
      'pwwk',
      'pzen',
      'proe'
    ],
    'prel': ['prel'],
    's00': ['s00', '6ed']
  };


  public static setCodeToSet2 (code: string) {
    const result = [];
    if (code) {
      code = code.toLowerCase();
    }
    MAPPINGS: for (const pattern of Object.keys(this.CODE_TO_CODE2)) {
      const re = new RegExp(pattern);
      if (code.match(re)) {
        const replacements = Arrays.arrayify(this.CODE_TO_CODE2[pattern]);
        for (const repl of replacements) {
          result.push(code.replace(re, repl));
        }
        break MAPPINGS;
      }
    }
    if (result.length === 0) {
      result.push(code);
      if (code.length === 3) {
        result.push('p' + code);
        result.push('t' + code);
      }
    }
    return result.filter(c => !!c).map(c => c.trim().toLowerCase());
  }


}
