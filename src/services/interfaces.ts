import { CardSchema } from '@topdecked/common-model';
import { Dates, Objects, Time } from '@topdecked/common-util';

export interface UpdatePricesOptions {
  tcgPublicKey?: string;
  tcgPrivateKey?: string;
  cardId?: string;
  setCode?: string;
  cardName?: string;
  force?: boolean;
  modifyOnly?: boolean;
}

export interface PriceService {
  updatePrices (opts: UpdatePricesOptions): Promise<any>;
}

export const cardRequiresPriceUpdate = (provider: keyof CardSchema['prices'], card: CardSchema) => {
  if (card) {
    if (Objects.hasNestedProperty(card, `prices.${provider}.updated`)) {
      if (Time.getElapsedMs(Time.now(), Dates.ensureDate(card.prices[provider].updated)) < (6 * Time.HOUR)) {
        return false;
      }
    }
    return true;
  }
  return false;
};
