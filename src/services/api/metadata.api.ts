export interface UpdateMetadataOpts {
  cardId?: string;
  force?: boolean;
  incremental?: boolean;
  setCode?: string;
}

export interface CatalogService {
  updateAllMetadata<T extends UpdateMetadataOpts> (opts: T): Promise<void>;
  cleanupDeprecatedMetadata<T extends UpdateMetadataOpts> (opts: T): Promise<void>;
}

export const BASE_CLEANUP_DEPRECATED_QUERY = (opts: UpdateMetadataOpts) => {
  return {
    deprecated: true,
    ...(opts.cardId ? { id: opts.cardId } : {}),
    ...(opts.setCode ? { set_code: opts.setCode } : {})
  }
}
