
import request from 'request-promise';

import { Objects, Time } from '@topdecked/common-util';

import { tcgplayer } from '../../config/tcgplayer';
import { SystemConfigElement } from '../../model';
import { logger } from '../../util/logger';

const TCGPLAYER_AUTH_CONFIG_KEY = 'tcgplayerAuth';


/* P '84CA52F8-65A7-46EF-8DD1-EE8AA2CAB0A0', p '04AA2EB8-EEFF-4337-9D80-FDC06C09F144' */

export class TcgPlayerAuthService {
  public async getAccessToken (publicKey: string, privateKey: string): Promise<string> {
    const doc = await SystemConfigElement.findOne({
      key: TCGPLAYER_AUTH_CONFIG_KEY
    }).exec();

    logger.debug('system config for %s: %o', TCGPLAYER_AUTH_CONFIG_KEY, doc);

    if (doc && doc.value && doc.value.access_token && doc.value.expires) {
      if (doc.value.expires.getTime() <= (Date.now() + Time.DAY)) {
        logger.verbose('access_token expired or expires within one day, requesting a new one');
        return this.requestAccessToken(publicKey, privateKey);
      } else {
        logger.verbose('access_token is current, using existing');
        return doc.value.access_token;
      }
    } else {
      logger.verbose('No previous access_token found, requesting one');
      return this.requestAccessToken(publicKey, privateKey);
    }
  }

  private async requestAccessToken (publicKey: string, privateKey: string) {
    const options: request.OptionsWithUri = {
      method: 'POST',
      headers: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      uri: tcgplayer.baseUrl + tcgplayer.apiVersion + '/token',
      form: {
        grant_type: 'client_credentials',
        client_id: (publicKey || process.env.TCGPLAYER_AUTH_PUBLIC_KEY) + '',
        client_secret: (privateKey || process.env.TCGPLAYER_AUTH_PRIVATE_KEY) + ''
      },
    };

    logger.debug(JSON.stringify(options));

    const json = await request(options);
    try {
      const results = JSON.parse(json);
      logger.debug('Got credentials %s', json);
      return this.saveAccessTokenResponse(results);
    } catch (err) {
      logger.error('Failed to request access_token', err);
      throw err;
    }
  }

  private async saveAccessTokenResponse (results) {
    try {
      const credentials = Objects.renameKeys(results, {
        '.issued': 'issued',
        '.expires': 'expires'
      });
      credentials.expires = new Date(credentials.expires);
      const doc = await SystemConfigElement.findOneAndUpdate(
        {
          key: TCGPLAYER_AUTH_CONFIG_KEY
        },
        {
          key: TCGPLAYER_AUTH_CONFIG_KEY,
          value: credentials
        },
        {
          upsert: true,
          new: true
        }
      ).exec();

      logger.verbose('Credentials stored, %o', doc);
      return credentials.access_token;
    } catch (err) {
      logger.error('Failed to save credentials: %s, %o', err.message, err);
      return results.access_token;
    }
  }

}
