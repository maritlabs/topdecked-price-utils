import { TCGPLAYER_AFFILIATE_IMPACT_URL_PREFIX, TcgPlayerCatalog } from './catalog';

test('can get affiliate link - single', () => {
  const catalog = new TcgPlayerCatalog();
  const url = catalog.getAffiliateLinkSingle('guilds-of-ravnica', 'guilds-of-ravnica-booster-box');
  console.error(url);
  expect(url)
    .toBe(TCGPLAYER_AFFILIATE_IMPACT_URL_PREFIX + 'https%3A%2F%2Fshop.tcgplayer.com%2Fmagic%2Fguilds-of-ravnica%2Fguilds-of-ravnica-booster-box%3Fpartner%3DTopDeckedMTG%26utm_campaign%3Daffiliate%26utm_medium%3DTopDeckedMTG%26utm_source%3DTopDeckedMTG');
});


test('can append affiliate link params', () => {
  const catalog = new TcgPlayerCatalog();
  const url = catalog.appendGoogleAnalyticsCodesToLink('https://shop.tcgplayer.com/magic/guilds-of-ravnica/guilds-of-ravnica-booster-box');
  console.error(url);
  expect(url)
    .toBe('https://shop.tcgplayer.com/magic/guilds-of-ravnica/guilds-of-ravnica-booster-box?partner=TopDeckedMTG&utm_campaign=affiliate&utm_medium=TopDeckedMTG&utm_source=TopDeckedMTG');
});
