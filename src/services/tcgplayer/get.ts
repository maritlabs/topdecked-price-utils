
import request from 'request-promise';

import { Arrays } from '@topdecked/common-util';

import { tcgplayer } from '../../config/tcgplayer';
import { logger } from '../../util/logger';

export interface TcgPlayerGroup {
  groupId: 0;
  name: string;
  abbreviation: string;
  isSupplemental: true;
  publishedOn: string;
  modifiedOn: string;
  categoryId: 0;
}

export interface TcgPlayerProduct {
  productId: number;
  name: string;
  cleanName: string;
  imageUrl: string;
  categoryId: number;
  groupId: number;
  url: string;
  modifiedOn: string;
  extendedData?: TcgPlayerExtendedData[];
}

export interface TcgPlayerExtendedData {
  name: string;
  displayName: string;
  value: string;
}

export interface TcgPlayerPriceResponse {
  productId: number;
  lowPrice: number;
  midPrice: number;
  highPrice: number;
  marketPrice: number;
  directLowPrice: number;
  subTypeName: 'Foil' | 'Normal';
}

const TCGPLAYER_MAGIC_CATEGORY_ID = 1;

export type TcgPlayerProductId = number | string;

export class TcgPlayerQuerier {

  public async getPricesByProductIds (
    token: string,
    productIds: TcgPlayerProductId | TcgPlayerProductId[]
  ): Promise<TcgPlayerPriceResponse[]> {
    // http://api.tcgplayer.com/v1.10.0/pricing/product/164338,164339
    /*
    {
      "success": true,
      "errors": [],
      "results": [
        {
          "productId": 164338,
          "lowPrice": 1.89,
          "midPrice": 3.53,
          "highPrice": 9.99,
          "marketPrice": 2.35,
          "directLowPrice": 0.25,
          "subTypeName": "Foil"
        },
        {
          "productId": 164338,
          "lowPrice": 0.21,
          "midPrice": 0.65,
          "highPrice": 6.27,
          "marketPrice": 0.39,
          "directLowPrice": 0.25,
          "subTypeName": "Normal"
        }
      ]
    }
    */

    const options = {
      method: 'GET',
      uri: tcgplayer.baseUrl + tcgplayer.apiVersion + '/pricing/product/' + Arrays.arrayify(productIds ?? []).join(','),
      headers: {
        Authorization: ' Bearer ' + token
      },
      json: true
    };

    try {
      const json = await request(options);
      logger.debug('pricing results: \n%o', json);
      return json.results;
    } catch (err) {
      logger.error(`failed to query prices for productIds -- %s: %o`, err.message, productIds);
      return [];
    }
  }

  public async getPricesByGroupId (token, groupId): Promise<TcgPlayerPriceResponse[]> {
    // http://api.tcgplayer.com/v1.10.0/pricing/group/1234
    /*
    {
      "success": true,
      "errors": [
        "string"
      ],
      "results": [
        {
            "productId": 15023,
            "lowPrice": 0.81,
            "midPrice": 2.25,
            "highPrice": 3.99,
            "marketPrice": 2.28,
            "directLowPrice": 0.5,
            "subTypeName": "Normal"
        },
        {
            "productId": 15023,
            "lowPrice": 26.99,
            "midPrice": 28.99,
            "highPrice": 30.89,
            "marketPrice": 29.99,
            "directLowPrice": 0.5,
            "subTypeName": "Foil"
        },
        .....
    ]
    }
    */
    const options = {
      method: 'GET',
      uri: tcgplayer.baseUrl + tcgplayer.apiVersion + '/pricing/group/' + groupId,
      headers: {
        Authorization: ' Bearer ' + token
      },
      json: true
    };

    try {
      const json = await request(options);
      return json.results;
    } catch (err) {
      logger.error(`failed to query prices for groupId ${groupId}: %s`, err.message);
      return [];
    }
  }


  public async getAllGroups (token, groups?: TcgPlayerGroup[]): Promise<TcgPlayerGroup[]> {
    if (!groups) { groups = []; }
    const results = await this.getGroups(token, groups.length);
    if (!results.length) {
      return groups;
    } else {
      return this.getAllGroups(token, groups.concat(results));
    }
  }

  public async getGroups (token, offset): Promise<TcgPlayerGroup[]> {
    const options = {
      method: 'GET',
      uri: tcgplayer.baseUrl + tcgplayer.apiVersion +
        '/catalog/categories/' + TCGPLAYER_MAGIC_CATEGORY_ID + '/groups',
      headers: {
        Authorization: ' Bearer ' + token
      },
      qs: {
        limit: 100,
        offset
      },
      json: true
    };

    try {
      const json = await request(options);
      logger.debug('Groups results: \n%o', json);
      return json.results;
    } catch (err) {
      return [];
    }
  }

  public async getProductsByName (token, groupId, cardName): Promise<TcgPlayerProduct[]> {
    const options = {
      method: 'GET',
      uri: tcgplayer.baseUrl + tcgplayer.apiVersion +
        '/catalog/products',
      headers: {
        Authorization: ' Bearer ' + token
      },
      qs: {
        limit: 1,
        getExtendedFields: true,
        categoryId: TCGPLAYER_MAGIC_CATEGORY_ID,
        groupId,
        productName: cardName
      },
      json: true
    };

    try {
      const json = await request(options);
      logger.debug('Product results: \n%o', json);
      return json.results;
    } catch (err) {
      logger.error(`failed to get products for ${groupId}, ${cardName} %s`, err.message);
      return [];
    }
  }

  public async getProductsByProductIds (token, productIds: number[]): Promise<TcgPlayerProduct[]> {
    const options = {
      method: 'GET',
      uri: tcgplayer.baseUrl + tcgplayer.apiVersion +
        '/catalog/products/' + productIds.join(','),
      headers: {
        Authorization: ' Bearer ' + token
      },
      qs: {
        getExtendedFields: true
      },
      json: true
    };

    try {
      const json = await request(options);
      logger.debug('products results: \n%o', json);
      return json.results;
    } catch (err) {
      logger.error(`failed to get products for ids %o %s`, productIds, err.message);
      return [];
    }
  }

  public async getAllProductsByGroupId (bearerToken, productGroupId, products?: any): Promise<TcgPlayerProduct[]> {
    if (!products) { products = []; }

    const getProductsByGroupId = async (token, groupId, offset) => {
      const options = {
        method: 'GET',
        uri: tcgplayer.baseUrl + tcgplayer.apiVersion +
          '/catalog/products',
        headers: {
          Authorization: ' Bearer ' + token
        },
        qs: {
          categoryId: TCGPLAYER_MAGIC_CATEGORY_ID,
          getExtendedFields: true,
          groupId,
          limit: 100,
          offset
        },
        json: true
      };

      try {
        const data = await request(options);
        logger.debug('products results: \n%o', data);
        return data;
      } catch (err) {
        logger.error(`failed to get products for groupId ${groupId}: %s`, err.message);
        return { results: [], totalItems: offset };
      }
    };


    const json = await getProductsByGroupId(bearerToken, productGroupId, products.length);
    products = products.concat(json.results);
    if (json.results.length === 0 || products.length === json.totalItems) {
      return products;
    } else {
      return this.getAllProductsByGroupId(bearerToken, productGroupId, products);
    }
  }

  public tcgAbbreviationToSetCodes (code: string): string[] {
    if (!code) {
      return [];
    }

    const codeLowerCase = code.trim().toLowerCase();

    const abbreviationSetCodes = {
      'APAC': ['pjas'],
      'ARENA': [
        'parl',
        'pal99',
        'pal00',
        'pal01',
        'pal02',
        'pal03',
        'pal04',
        'pal05',
        'pal06'
      ],
      'ASMH1': ['amh1'],
      'CTD': ['cst'],
      'DD3': ['gvl', 'evg', 'dvd', 'jvc', 'tgvl', 'tevg', 'tdvd', 'tjvc'],
      'EURO': 'pelp',
      'FRF_UGIN': 'ugin',
      'GBP': [
        'g18',
        'g17',
        'mgb'
      ],
      'GR1': ['gk1'],
      'GUR': 'pgru',
      'HERO': ['thp1', 'thp2', 'thp3'],
      'IED': [
        'cei'
      ],
      'JDG': [
        'jgp',
        'g99',
        'g00',
        'g01',
        'g02',
        'g03',
        'g04',
        'g05',
        'g06',
        'g07',
        'g08',
        'g09',
        'g10',
        'g11',
        'j12',
        'j13',
        'j14',
        'j15',
        'j16',
        'j17',
        'j18',
        'j19',
        'j20'
      ],
      'JSS': 'pjse',
      'LARP': ['plpa', 'pcmd'],
      'LEP': [
        'l12',
        'l13',
        'l14',
        'l15',
        'l16',
        'l17'
      ],
      'MED2': 'med',
      'MEDIA': ['pmei', 'pres'],
      'MFP': 'pf19',
      'MPRP': ['mpr', 'pr2', 'p03', 'p04', 'p05', 'p06', 'p07', 'p08', 'p09', 'p10', 'p11'],
      'MPS2': ['mp2'],
      'OVER': ['ocmd', 'ocm1', 'oc13', 'oc14', 'oc15', 'oc16', 'oc17', 'oc18', 'oc19', 'oc20'],
      'PRE': 'ppre',
      'PREM': ['pmps', 'pmps06', 'pmps07', 'pmps08', 'pmps09', 'pmps10', 'pmps11'],
      'SSP': [
        'pss3',
        'pss2',
        'pss1'
      ],
      'TDP': ['ptkdf'],
      'UMA:BT': ['puma'],
      'WCD': [
        'wc97',
        'wc98',
        'wc99',
        'wc00',
        'wc01',
        'wc02',
        'wc03',
        'wc04',
      ],
      'WMCQ': ['pwcq', 'ppro'],
      'WPN': ['pgtw', 'pg07', 'pg08']
    };

    const mapping = abbreviationSetCodes[code] ?? abbreviationSetCodes[codeLowerCase];
    return Arrays.arrayify(mapping ? mapping : [codeLowerCase, codeLowerCase.replace(/p(p[a-z0-9]{3})/i, '$1')]);
  }

  public tcgNameToSetCodes (name: string) {
    if (!name) {
      return [];
    }

    const abbreviationSetCodes = {
      'Mystery Booster: Retail Exclusives': [
        'fmb1'
      ],
      'Fourth Edition (Foreign Black Border)': [
        'fbb'
      ],
      // 'Fourth Edition (Foreign White Border)': [
      //   '4ed' // This would overwrite 4th edition, and we don't want that. Support multiple codes per set?
      // ],
    };
    const result = Arrays.arrayify(abbreviationSetCodes[name]);
    return result ? result : [name.trim().toLowerCase()];
  }
}
