// tslint:disable: max-line-length

import { Document } from 'mongoose';

import { CardSchema, TcgPlayerPriceSourceSchema } from '@topdecked/common-model';
import { Arrays, Objects, Time } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel } from '../../model';
import { logger } from '../../util/logger';
import { cardRequiresPriceUpdate, PriceService, UpdatePricesOptions } from '../interfaces';
import { TcgPlayerAuthService } from './auth';
import { TcgPlayerPriceResponse, TcgPlayerQuerier } from './get';

const querier = new TcgPlayerQuerier();

const CARD_SELECT_TCG_ID_FIELDS = '+tcgplayer_id +tcgplayer_etched_id';

export class TcgPlayerPriceService implements PriceService {

  private auth: TcgPlayerAuthService = new TcgPlayerAuthService();

  public async updatePrices (opts: UpdatePricesOptions) {
    logger.info(`OPTS: %o`, opts);
    await database.ready();
    const token = await this.auth.getAccessToken(opts.tcgPublicKey, opts.tcgPrivateKey);
    logger.debug(token);
    await this.updatePricesByCard(token, opts);
  }

  private async updatePricesByCard (bearerToken: string, opts: UpdatePricesOptions = {}) {
    const cursor = CardModel.find({
      ...(opts.cardId ? { id: opts.cardId } : {}),
      ...(opts.cardName ? { name: opts.cardName } : {}),
      ...(opts.setCode ? { set_code: opts.setCode } : {}),
      $or: [
        { tcgplayer_id: { $ne: null } },
        { tcgplayer_etched_id: { $ne: null } }
      ]
    }).select(CARD_SELECT_TCG_ID_FIELDS).cursor();

    const BATCH_SIZE = 50;
    let batch = {};

    await cursor.eachAsync(async (card) => {
      if (opts.force || cardRequiresPriceUpdate('tcgplayer', card)) {
        if (card.tcgplayer_id) {
          logger.debug(`${card.id} ${card.name} tcgplayer_id: ${card.tcgplayer_id}`);
          batch[card.tcgplayer_id] = card;
        }
        if (card.tcgplayer_etched_id) {
          logger.debug(`${card.id} ${card.name} tcgplayer_etched_id: ${card.tcgplayer_etched_id}`);
          batch[card.tcgplayer_etched_id] = card;
        }
        if (Object.keys(batch).length >= BATCH_SIZE) {
          await this.updateCardsPricesBatch(bearerToken, batch, opts);
          batch = {};
        }
      } else {
        logger.info(`${card.id} [${card.tcgplayer_id}, ${card.tcgplayer_id}]: Prices already current`);
      }
    });

    if (Object.keys(batch).length > 0) {
      await this.updateCardsPricesBatch(bearerToken, batch, opts);
    }
  }

  public async updateCardsPricesBatch (bearerToken: string, batchMap: { [id: string]: CardSchema }, opts: UpdatePricesOptions = {}) {
    const prices = await querier.getPricesByProductIds(bearerToken, Object.keys(batchMap));
    await this.commitPricesToDB(prices, opts);
  }


  private async commitPricesToDB (prices: TcgPlayerPriceResponse[], opts: UpdatePricesOptions = {}) {
    await database.ready();
    logger.debug('%o', prices);

    prices = prices.filter((price) => {
      return price && (price.midPrice || price.lowPrice || price.highPrice || price.marketPrice || price.directLowPrice);
    });

    const productIds = prices.map(p => p.productId);
    const cards = await CardModel.find({
      $or: [{
        tcgplayer_id: { $in: productIds }
      }, {
        tcgplayer_etched_id: { $in: productIds }
      }]
    })
      .select(CARD_SELECT_TCG_ID_FIELDS).exec();

    const cardsByTcgId: Record<string, (CardSchema & Document)[]> = Arrays.toDictionaryTable(cards, 'tcgplayer_id');
    const cardsByTcgEtchedId: Record<string, (CardSchema & Document)[]> = Arrays.toDictionaryTable(cards, 'tcgplayer_etched_id');

    const pricesByCardId: {
      [id: string]: Partial<CardSchema['prices']['tcgplayer']>
    } = {};

    let result: CardSchema['prices']['tcgplayer'];

    for (const price of prices) {
      const normal = cardsByTcgId[price.productId] ?? [];
      const etched = cardsByTcgEtchedId[price.productId] ?? [];
      const found = Arrays.concat(normal, etched);

      if (!found?.length) {
        logger.error(`Card not found for productId ${price.productId}`);
      }

      for (const card of found) {
        const priceFieldVariation = (etched.includes(card) ? 'prices_etched' : 'prices_foil');
        const priceField = (price.subTypeName) === 'Normal' ? 'prices' : priceFieldVariation;

        if (card) {
          pricesByCardId[card.id] = {
            ...(pricesByCardId[card.id] ?? {}),
            [priceField]: Objects.deleteNils({
              low: price.lowPrice,
              market: price.marketPrice,
              mid: price.midPrice,
              high: price.highPrice,
              directLow: price.directLowPrice,
            })
          };
        }
      }

    }

    for (const card of cards) {

      const priceUpdate: TcgPlayerPriceSourceSchema = {
        prices: card.prices?.tcgplayer?.prices ?? {},
        prices_foil: card.prices?.tcgplayer?.prices_foil ?? {},
        prices_etched: card.prices?.tcgplayer?.prices_etched ?? {},
        ...(pricesByCardId[card.id] ?? {}),
        created: card.prices?.tcgplayer?.created ?? Time.now(),
        updated: Time.now()
      };

      if (opts.force || cardRequiresPriceUpdate('tcgplayer', card)) {

        logger.verbose(`${card.id} [${card.tcgplayer_id}, ${card.tcgplayer_etched_id}] ${card.name}: Curr. prices: %s`, JSON.stringify(card.prices.tcgplayer));
        logger.verbose(`${card.id} [${card.tcgplayer_id}, ${card.tcgplayer_etched_id}] ${card.name}: Updt. prices: %s`, JSON.stringify(priceUpdate));

        Objects.set(card, 'prices.tcgplayer', priceUpdate);
        card.markModified('prices.tcgplayer');

        const saved = await card.save();
        logger.info(`${card.id} [${card.tcgplayer_id}, ${card.tcgplayer_etched_id}] ${card.name}: Prices updated`);
        logger.verbose(`${card.id} [${card.tcgplayer_id}, ${card.tcgplayer_etched_id}] ${card.name}: Savd. prices: %o`, JSON.stringify(saved.prices.tcgplayer));

        result = saved.prices.tcgplayer;
      } else {
        logger.info(`${card.id} [${card.tcgplayer_id}, ${card.tcgplayer_etched_id}] ${card.name}: Prices already current`);
      }
    }

    return result;
  }

}
