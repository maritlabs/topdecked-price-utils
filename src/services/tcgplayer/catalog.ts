

import { Document } from 'mongoose';

import { CardSchema } from '@topdecked/common-model';
import { Arrays, Dates, Numbers, Strings, Time, UUID } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, SetModel } from '../../model';
import { logger } from '../../util/logger';
import {
  BASE_CLEANUP_DEPRECATED_QUERY, CatalogService, UpdateMetadataOpts
} from '../api/metadata.api';
import { TcgPlayerAuthService } from './auth';
import { TcgPlayerGroup, TcgPlayerProduct, TcgPlayerQuerier } from './get';

const querier = new TcgPlayerQuerier();

export interface TcgPlayerMetadataOptions extends UpdateMetadataOpts {
  force?: boolean;
  incremental?: boolean;
  setCode?: string;
  tcgPublicKey?: string;
  tcgPrivateKey?: string;
}


export class TcgPlayerCatalog implements CatalogService {
  private authService: TcgPlayerAuthService;

  constructor() {
    this.authService = new TcgPlayerAuthService();
    logger.debug('TCGPLAYER_AUTH_PUBLIC_KEY %s, TCGPLAYER_AUTH_PRIVATE_KEY %s',
      process.env.TCGPLAYER_AUTH_PUBLIC_KEY, process.env.TCGPLAYER_AUTH_PRIVATE_KEY);
  }

  public async updateAllMetadata (opts: TcgPlayerMetadataOptions = {}) {
    await database.ready();
    const bearerToken = await this.authService.getAccessToken(opts.tcgPublicKey, opts.tcgPrivateKey);

    if (opts.incremental) {
      logger.info('Updating remaining cards with tcgplayer metadata');
      await this.updateCardsWithPartialMetadata(bearerToken, opts);
    } else {
      logger.info('Attempting to resolve all tcgplayer product metadata');
      const groups = await querier.getAllGroups(bearerToken);
      const mappings = await this.updateSetsWithTCGPlayerMetadata(groups, opts);

      let groupsProcessed = 0;
      for (const mapping of mappings) {
        if (!opts.setCode || opts.setCode === mapping.setcode) {
          const products = await querier.getAllProductsByGroupId(bearerToken, mapping.groupId);
          await this.updateCardsWithTCGPlayerMetadata(products, mapping.setcode, opts);
          logger.info('Cards updated with tcgplayer metadata successfully for set: %s', mapping.setcode);
          groupsProcessed++;
          logger.verbose('%s/%s groups processed', groupsProcessed, Object.keys(mappings).length);
        }
      }
      logger.info('Sets updated with tcgplayer metadata successfully');

      logger.info('Updating remaining cards with tcgplayer metadata');
      await this.updateCardsWithPartialMetadata(bearerToken, opts);
    }
    logger.info('Updated cards with tcgplayer metadata');
  }

  private async updateCardsWithPartialMetadata (bearerToken: string, opts: TcgPlayerMetadataOptions = {}) {
    const query = {
      deprecated: { $ne: true },
      ...(opts.cardId ? { id: opts.cardId } : {}),
      ...(opts.force ? {} : {
        $and: [{
          $or: [
            { 'metadata.tcgplayerLinksInitialized': { $ne: true } },
            { 'purchase_uris.tcgplayer': null }
          ]
        }]
      }),
      ...(opts.setCode ? { set_code: opts.setCode } : {}),
      $or: [
        { tcgplayer_id: { $ne: null } },
        { tcgplayer_etched_id: { $ne: null } },
        opts.force ? { "purchase_uris.tcgplayer": { $ne: null, $not: new RegExp(TCGPLAYER_AFFILIATE_IMPACT_DOMAIN) } } : {}
      ]
    };
    logger.verbose(`Query partial metadata, %o`, query);


    const cursor = CardModel.find(query).select('+tcgplayer_id +tcgplayer_etched_id +metadata').cursor();
    const BATCH_SIZE = 50;
    let batchMap: { [id: string]: CardSchema & Document } = {};

    await cursor.eachAsync(async (card) => {
      if (card) {
        logger.verbose(`Staging card ${card?.name} for incremental update: ${card?.id}`);
        batchMap[card.tcgplayer_id ?? UUID.generateUUID()] = card;
        if (Object.keys(batchMap).length >= BATCH_SIZE) {
          await this.updateCardsWithPartialMetadataBatch(bearerToken, batchMap);
          batchMap = {};
        }
      }
    });

    if (Object.keys(batchMap).length > 0) {
      await this.updateCardsWithPartialMetadataBatch(bearerToken, batchMap);
    }
  }

  public async updateCardsWithPartialMetadataBatch (bearerToken: string, batchMap: { [id: string]: CardSchema & Document }) {
    const productIds: number[] = (Object.keys(batchMap) ?? []).map(p => Numbers.ensureInteger(p, p as any));
    const productIdsFound: number[] = [];
    logger.warn(`Fetching product batch of ${productIds.length}, %o`, productIds);

    const products = await querier.getProductsByProductIds(bearerToken, productIds);
    for (const product of products) {
      logger.verbose(`Updating product "${product.name}" with groupId(${product.groupId}) and productId(${product.productId})`);
      const card = batchMap[product.productId];
      if (card) {
        productIdsFound.push(product.productId);
        card.purchase_uris.tcgplayer = this.createAffiliateLink(product.url);
        card.set('metadata.tcgplayerLinksInitialized', true);
        try {
          await card.save();
        } catch (err) {
          logger.error('Failed to update Card metadata %o', card);
          throw err;
        }
        logger.verbose(`Updated product id "${product.productId}", "${card.set_code}", "${card.name}", [${product.name}, ${card.purchase_uris.tcgplayer}]`);
      } else {
        logger.warn(`Card not found for "${product.productId}", [${product.name}, ${product.url}]`);
      }
    }

    const productIdsMissing = Arrays.diff(productIds, productIdsFound);
    logger.warn(`Updating links for ${productIdsMissing.length}/${productIds.length} unprocessed cards: %o`, productIdsMissing);
    for (const productId of productIdsMissing) {
      const card = batchMap[productId];
      if (card && card.purchase_uris?.tcgplayer) {
        card.purchase_uris.tcgplayer = this.createAffiliateLink(card.purchase_uris.tcgplayer);
        card.set('metadata.tcgplayerLinksInitialized', true);
        try {
          await card.save();
        } catch (err) {
          logger.error('Failed to update missing Card metadata %o', card);
          throw err;
        }
        logger.verbose(`Updated missing product id "${productId}", "${card.set_code}", "${card.name}", [${card.name}, ${card.purchase_uris.tcgplayer}]`);
      }
    }
  }

  private async updateSetsWithTCGPlayerMetadata (groups: TcgPlayerGroup[], opts: TcgPlayerMetadataOptions = {}) {
    await database.ready();
    const groupIdSetCodeMapping: { setcode: string, groupId: number }[] = [];
    let updatedCount = 0;
    for (const group of groups) {
      logger.debug(`TCGplayer group [${group.name}]: [${group.abbreviation}] [${group.groupId}]`)
      let sets = [];
      const strictQuery = {
        ...(opts.force ? {} : { 'metadata.tcgplayerLinksInitialized': { $ne: true } }),
        ...(opts.setCode ? { code: opts.setCode } : {}),
        $or: [
          { code: { $in: Arrays.nonNullValues(Arrays.uniqueValues(querier.tcgAbbreviationToSetCodes(group.abbreviation))) } },
          { code: { $in: Arrays.nonNullValues(querier.tcgNameToSetCodes(group.name)) } },
          { name: group.name }
        ]
      };
      sets = await SetModel.find(strictQuery).select('+tcgplayer_group_id +tcgplayer_group_name');
      if (!sets?.length) {
        const nameRegex = '^' + group.name.replace(/[^a-z0-9 ]+/gi, '').split(/\s+/).join('.*');
        logger.debug(`Name regex: ${group.name} - ${nameRegex}`)
        const query = {
          ...(opts.setCode ? { code: opts.setCode } : {}),
          $or: [
            { name: { $regex: nameRegex } }
          ]
        };
        sets = await SetModel.find(query).select('+tcgplayer_group_id +tcgplayer_group_name');
      }

      if (!sets?.length) {
        if (!opts.setCode) {
          logger.warn(`\tCould not find set for tcgplayer metadata: %o`, group);
        }
      } else {
        if (sets.length > 1) {
          logger.warn('Multiple sets found when updating metadata: %o %o', group, sets);
        }
        for (const s of sets) {
          // TODO: Store tcgplayer group/set links after urlification?
          // URLS.urilify(group.name) ----> 'Return to Ravnica':'return-to-ravnica'
          // logger.debug("set link, %s", group.name)
          s.tcgplayer_group_id = group.groupId;
          s.tcgplayer_group_name = group.name;
          await s.save();
          updatedCount++;
          logger.verbose(`\tUpdated set: "${s.code}, ${s.name}" with tcgplayer_group_id: ${group.groupId}, ${group.abbreviation}, tcgplayer_group_name: ${group.name}`);

          groupIdSetCodeMapping.push({
            setcode: s.code,
            groupId: group.groupId
          });
        }
      }
    }

    if (!updatedCount) {
      logger.error(`\tFailed to update sets with TCGplayer metadata.`);
    }

    return groupIdSetCodeMapping;
  }

  private async updateCardsWithTCGPlayerMetadata (products: TcgPlayerProduct[], setcode: string, opts: TcgPlayerMetadataOptions = {}) {
    await database.ready();
    setcode = setcode.toLowerCase().trim();

    const groupIds = Arrays.uniqueValues(products.map(p => p.groupId));
    const productIds = Arrays.uniqueValues(products.map(p => p.productId));
    const productsByGroupId = Arrays.toDictionary(products, 'groupId');
    const sets = (await SetModel.find({ tcgplayer_group_id: { $in: groupIds } })
      .select('+tcgplayer_group_id +tcgplayer_group_name'))
      .filter(s => s.code === setcode);

    const setsByProductGroupId = Arrays.toDictionary(sets, 'tcgplayer_group_id');
    logger.debug('found sets matching tcgplayer_group_id %o, %o', groupIds, setsByProductGroupId)
    const query = {
      deprecated: { $ne: true },
      ...(opts.cardId ? { id: opts.cardId } : {}),
      set_code: setcode,
      $or: [
        { tcgplayer_id: { $in: productIds } },
        { tcgplayer_etched_id: { $in: productIds } }
      ]
    };
    logger.debug('querying for cards matching product ids: %o', query);
    const cards = await CardModel.find(query).select('+tcgplayer_id +tcgplayer_etched_id +metadata').exec();

    const tcgplayerIdMap: Record<string, (CardSchema & Document)[]> = Arrays.toDictionaryTable(cards, 'tcgplayer_id');
    const tcgplayerEtchedIdMap: Record<string, (CardSchema & Document)[]> = Arrays.toDictionaryTable(cards, 'tcgplayer_etched_id');
    logger.verbose('tcgplayer products: %o', products.length);
    logger.verbose('cards found: %o', cards.length);

    let updatedCount = 0;
    const failed: TcgPlayerProduct[] = [];
    PRODUCT: for (const product of products) {
      const productId = product.productId;

      let name = product.name;

      const REGEX_ART_CARD = /Art(\W+)Card/i;
      const REGEX_DOUBLE_SIDED = /Double(\W+)sided/i;
      const REGEX_FOIL_ETCHED = /Foil(\W+)Etched/i;
      const REGEX_HALOFOIL = /Halo Foil/i;
      const REGEX_OVERSIZED = /Oversized/i;
      const REGEX_PROMO = /\bPromo\b/i;
      const REGEX_SERIALIZED = /Serial Numbered/i;
      const REGEX_TOKEN = /Token/i;

      const etched = REGEX_FOIL_ETCHED.test(name);
      const artCard = REGEX_ART_CARD.test(name);
      const oversized = REGEX_OVERSIZED.test(name);
      const halofoil = REGEX_HALOFOIL.test(name);
      const serialized = REGEX_SERIALIZED.test(name);
      const token = REGEX_TOKEN.test(name);
      const promo = REGEX_PROMO.test(product.url);

      const doubleSided = name.includes('//') || REGEX_DOUBLE_SIDED.test(name);

      name = Strings.replaceAll(name, REGEX_ART_CARD, '');
      name = Strings.replaceAll(name, REGEX_DOUBLE_SIDED, '');

      if (name.startsWith('Emblem - ')) {
        name = name.split(' - ', 2)[1].trim() + ' Emblem';
      } else {
        name = name.split(/[\[\(\{\]]/i)[0].trim().split('Token')[0].trim();
      }

      logger.debug(`Locating product "${product.name}" with groupId(${product.groupId}) and productId(${productId}) %o`, product);

      const set = setsByProductGroupId[product.groupId]
      const found: (CardSchema & Document)[] = (etched ? tcgplayerEtchedIdMap[productId] : tcgplayerIdMap[productId]) ?? [];
      if (found?.length) {
        logger.debug(`Located via product id "${productId}"`);
      } else if (!set) {
        logger.debug('Skipping unrelated product: %o', product.groupId)
        continue PRODUCT;
      } else {
        const executeQuery = async (vars: { names: string[], setcode: string }) => {
          const collector_number = Arrays.toDictionary(product.extendedData, 'name')?.Number?.value
            ?? product.name.match(/.*\([^)]*(\d+)[^)]*\).*/)?.[1];

          const setcodes = ['', 'p', 't'].map(p => p + vars.setcode);
          if (setcodes.includes('mh1')) {
            setcodes.push('h1r');
          }

          const nameRegexes = Arrays.uniqueValues(vars.names).map(n => new RegExp(`^${Strings.escapeRegExp(n)}.*`, 'i'));
          const collectorNumberRegex = '^0*' + collector_number + 'z?$';

          const query: any = {
            deprecated: { $ne: true },
            ...(opts.cardId ? { id: opts.cardId } : {}),
            games: 'paper',
            name: { $in: nameRegexes },
            set_code: { $in: setcodes },
            ...(artCard ? { layout: 'art_series' } : { layout: { $nin: ['art_series'] } }),
            ...(collector_number ? { collector_number: { $regex: collectorNumberRegex } } : {}),
            ...(doubleSided ? { 'card_faces.1': { $exists: true } } : {}),
            ...(halofoil ? { promo_types: 'halofoil' } : {}),
            ...(oversized ? { oversized: true } : {}),
            ...(promo ? { promo_types: { $exists: true, $ne: [] } } : {}),
            ...(serialized ? { promo_types: 'serialized' } : {}),
            ...(token ? { 'card_faces.types': 'Token' } : {}),
          };

          logger.verbose(`Locating card for product, by query "${vars.setcode} ${collector_number} %o`, Arrays.uniqueValues(vars.names));
          logger.debug(`%o`, query);
          const cards: (CardSchema & Document)[] = await CardModel.find(query).select('+tcgplayer_id +tcgplayer_etched_id +metadata').exec();
          return cards;
        };



        const names = [name];
        if (doubleSided) {
          for (const n of name.split('//')) {
            names.push(n);
          }
        }

        let results: (CardSchema & Document)[] = [];
        results = await executeQuery({ names, setcode });
        if (!results?.length) {
          const parentCode = set?.parent_set_code && set.parent_set_code !== set.code ? set.parent_set_code : null;
          if (parentCode) {
            results = await executeQuery({ names, setcode: parentCode });
          }
        }


        const strict = !(token && doubleSided);
        if (results?.length) {

          const recent = results.some(r => Time.after(Dates.ensureDate(r.created, Time.now()), Time.ago(Time.MONTH)));
          if (results.length > 1 && strict) {
            const message = `Found multiple potential matches for product: ${name}: `
              + `[${results.map(c => `${c.set_code}:${c.name}`).join(', ')}] [${results.map(c => `${c.id}`).join(', ')}]`;

            if (recent) {
              logger.warn(`Skipping -- ${message}`);
            } else {
              logger.warn(`Product: %o`, product)
              // throw new Error(message);
            }
          } else {
            for (const card of results) {
              found.push(card);
            }
          }
        }
      }

      if (!found?.length) {
        failed.push(product);
      } else {
        logger.verbose(`Found cards for product: %o`, found.map(c => ({
          id: c.id,
          name: c.name,
          set_code: c.set_code,
          set_name: c.set_name,
          tcgplayer_id: c.tcgplayer_id,
          tcgplayer_etched_id: c.tcgplayer_etched_id
        })))
      }

      for (const card of found) {
        if (card) {

          if (etched && card.finishes.includes('etched')) {
            if (opts.force || !card.tcgplayer_etched_id) {
              card.tcgplayer_etched_id = productId;
            }
            if (opts.force || !card.purchase_uris?.tcgplayer_etched) {
              card.purchase_uris.tcgplayer_etched = this.createAffiliateLink(product.url);
            }

          } else {
            if (opts.force || !card.tcgplayer_id) {
              card.tcgplayer_id = productId;
            }
            if (opts.force || !card.purchase_uris?.tcgplayer) {
              card.purchase_uris.tcgplayer = this.createAffiliateLink(product.url);
            }
          }

          if (!card.purchase_uris.tcgplayer && card.purchase_uris.tcgplayer_etched) {
            card.purchase_uris.tcgplayer = card.purchase_uris.tcgplayer_etched;
          }

          card.set('metadata.tcgplayerLinksInitialized', true);

          try {
            await card.save();
          } catch (err) {
            logger.error('Failed to update Card product IDs %o', card);
            throw err;
          }
          updatedCount++;

          logger.verbose(`Updated card ${card.id} product ${setcode}, "${product.name}"` +
            ` with groupId(${product.groupId}) and productId(${product.productId})`);
          logger.debug(`--> purchase URIs: %o %o`, card.purchase_uris.tcgplayer, card.purchase_uris.tcgplayer_etched)
        } else {
          logger.warn(`Failed to update product ${setcode}, "${product.name}"` +
            ` with groupId(${product.groupId}) and productId(${product.productId})`);
        }
      }
    }
    logger.info(`${updatedCount}/${products.length} cards updated`);
    logger.warn(`Failed to find ${failed?.length} products %o`, failed?.map(f => `[${f.productId}, ${f.name}]`));
  }

  public async cleanupDeprecatedMetadata (opts: TcgPlayerMetadataOptions = {}) {
    await database.ready();
    logger.info('cleaning deprecated metadata from  %s in %s', (opts.cardId ?? 'all cards'), (opts.setCode ?? 'all sets'));
    const query = {
      ...BASE_CLEANUP_DEPRECATED_QUERY(opts),
      $or: [
        { tcgplayer_id: { $exists: true } },
        { tcgplayer_etched_id: { $exists: true } },
        { 'metadata.tcgplayerLinksInitialized': { $exists: true } }
      ]
    };
    logger.debug('querying for cards matching product ids: %o', query);
    const cards = await CardModel.find(query).select('+tcgplayer_id +tcgplayer_etched_id +metadata').exec();

    if (cards.length > 0) {
      const cardsNonDeprecated = cards.filter(c => c.deprecated);
      if (cardsNonDeprecated?.length) {
        logger.error(`Attempted to remove metadata from non-deprecated cards, %o`, cardsNonDeprecated.map(c => c.id))
        throw new Error('Attempted to remove metadata from non-deprecated cards');
      }

      try {
        query.deprecated = true; // we REALLY don't want this to be wrong
        const updated = await CardModel.updateMany(query, {
          $unset: {
            tcgplayer_id: 1,
            tcgplayer_etched_id: 1,
            'metadata.tcgplayerLinksInitialized': 1
          }
        }).exec();

        const updatedCount = updated.modifiedCount + updated.upsertedCount;

        if (updatedCount !== cards.length) {
          throw new Error(`Modified documents count ${updatedCount} differs from expected ${cards.length}`);
        }

        logger.info(`${updatedCount}/${cards.length} cards updated`);
      } catch (err) {
        logger.error('Failed to cleanup deprecated card metadata %o', query);
        throw err;
      }

    } else {
      logger.info(`No deprecated cards to update`);
    }
  }

  public createAffiliateLink (url: string) {
    if (url) {
      if (!url.includes(TCGPLAYER_AFFILIATE_IMPACT_URL_PREFIX)) {
        return TCGPLAYER_AFFILIATE_IMPACT_URL_PREFIX + encodeURIComponent(this.appendGoogleAnalyticsCodesToLink(url))
      } else {
        logger.warn('Purchase URIs: already uses Impact tracking: ' + url);
      }
    }
    return url;
  }

  public appendGoogleAnalyticsCodesToLink (url: string, affiliateCode = TCGPLAYER_AFFILIATE_CODE_GA) {
    if (!url || url.indexOf('utm_source') > -1) {
      logger.warn('Purchase URIs: Not appending affiliate link to URL. It alread contains "utm_source": ' + url);
      return url;
    }
    if (!url.includes('?')) {
      return this.substituteAffiliateCode(url + '?' + TCGPLAYER_AFFILIATE_CODE_GA_URL);
    } else {
      return this.substituteAffiliateCode(url + '&' + TCGPLAYER_AFFILIATE_CODE_GA_URL);
    }
  }

  public getAffiliateLinkSingle (group_name: string, product_name: string, affiliateCode = TCGPLAYER_AFFILIATE_CODE_GA) {
    if (!group_name || !product_name) {
      return null;
    }
    return this.createAffiliateLink(`https://shop.tcgplayer.com/magic/${group_name.trim()}/${product_name.trim()}`);
  }

  public substituteAffiliateCode (url: string, affiliateCode = TCGPLAYER_AFFILIATE_CODE_GA) {
    if (url) {
      return url.replace(/AFFILIATECODE/g, affiliateCode);
    }
    return url;
  }
}

export const TCGPLAYER_AFFILIATE_IMPACT_DOMAIN = 'partner.tcgplayer.com'
export const TCGPLAYER_AFFILIATE_IMPACT_URL_PREFIX = `https://${TCGPLAYER_AFFILIATE_IMPACT_DOMAIN}/c/4927964/1830156/21018?u=`;
export const TCGPLAYER_AFFILIATE_CODE_GA = 'TopDeckedMTG';
export const TCGPLAYER_AFFILIATE_CODE_GA_URL = 'partner=AFFILIATECODE'
  + '&utm_campaign=affiliate'
  + '&utm_medium=AFFILIATECODE'
  + '&utm_source=AFFILIATECODE';
