import levenshtein from 'js-levenshtein';
import { Document } from 'mongoose';
import request from 'request-promise';

import { CardSchema, SetSchema } from '@topdecked/common-model';
import { DateLike } from '@topdecked/common-model/build/main/models/base-schema';
import { Arrays, Numbers, Strings, Time } from '@topdecked/common-util';

import { database } from '../../config/database';
import { CardModel, SetModel } from '../../model';
import { Downloads } from '../../util/download';
import { logger } from '../../util/logger';
import { cardRequiresPriceUpdate, PriceService, UpdatePricesOptions } from '../interfaces';

export type CardKingdomFinishPrefix = 'F' | 'TF' | 'FE';

export interface CardKingdomProduct {
  id: number;
  // SKU appears to be set-code & collector number
  // F prefix for foil
  // [F][set_code]-[collector_number]
  sku: string;
  url: string; // this is the suffix to append to base_url
  name: string;
  variation: string;
  edition: string;
  is_foil: string;
  price_retail: string; // "0.25",
  qty_retail: number;
  price_buy: string; // "0.05",
  qty_buying: number;

  condition_values?: {
    nm_price: number;
    nm_qty: number;
    ex_price: number;
    ex_qty: number;
    vg_price: number;
    vg_qty: number;
    g_price: number;
    g_qty: number;
  };
}

export type CardKingdomPricePoint = 'nm' | 'ex' | 'vg' | 'g';

export interface CardKingdomPriceSheet {
  meta: {
    base_url: string;
    created_at: DateLike
  };
  data: CardKingdomProduct[];
}

interface CardKingdomDictionaries {
  prices: CardKingdomPriceSheet;
  productsBySku: Record<string, CardKingdomProduct>;
  productsByEdition: Record<string, CardKingdomProduct[]>;
  productsByName: Record<string, CardKingdomProduct[]>;
  sets: SetSchema[];
  setsByCode: Record<string, SetSchema>;
}

const MIDPOINTS = ['nm', 'ex', 'vg', 'g'];
const CARDKINGDOM_SET_CODE_MAP = {
  h1r: 'mh1'
};

export class CardKingdomPriceService implements PriceService {

  private updatedMetadataCount = 0;
  private updatedPricesCount = 0;
  private unmatched: CardSchema[] = [];
  private unmatchedMetadata: CardSchema[] = [];

  public async updatePrices (opts: UpdatePricesOptions) {
    await database.ready();

    const prices = await this.getPriceSheetJson();
    const productsBySku = Arrays.toDictionary(prices.data, 'sku');
    const productsByEdition = Arrays.toDictionaryTable(prices.data, 'edition');
    const productsByName = Arrays.toDictionaryTable(prices.data, 'name');
    const sets = await SetModel.find();
    const setsByCode = Arrays.toDictionary(sets, 'code');

    const dictionaries = {
      prices,
      productsBySku,
      productsByEdition,
      productsByName,
      sets,
      setsByCode
    };

    logger.debug(`OPTS: %o`, opts);

    if (opts.cardId || opts.setCode || opts.cardName) {
      await this.updateSelection(dictionaries, opts);
    } else {
      await this.updateAllSets(dictionaries, opts);
    }

    logger.info(`Updated ${this.updatedPricesCount} of ${prices.data?.length} possible prices.`)
    logger.info(`Updated ${this.updatedMetadataCount} of ${prices.data?.length} possible metadata.`)

    if (this.unmatched.length) {
      logger.error(`Failed to update ${this.unmatched.length} cards prices.`);
      logger.debug(`%o`, this.unmatched.map(c => ({ name: c.name, set_code: c.set_code, collector_number: c.collector_number, id: c.id })));
    }
    if (this.unmatchedMetadata.length) {
      logger.error(`Failed to update ${this.unmatched.length} cards metadata.`);
      logger.debug(`%o`, this.unmatched.map(c => ({ name: c.name, set_code: c.set_code, collector_number: c.collector_number, id: c.id })));
    }
  }

  public async updateSelection (dictionaries: CardKingdomDictionaries, opts: UpdatePricesOptions) {
    await database.ready();
    const query = {
      deprecated: { $ne: true },
      ...(opts.cardId ? { $or: [{ id: opts.cardId }, { oracle_id: opts.cardId }] } : {}),
      ...(opts.setCode ? { set_code: opts.setCode } : {}),
      ...(opts.cardName ? { name: opts.cardName } : {})
    };
    logger.debug(`QUERY: %o`, query);
    const ids = (await CardModel.find(query)).map(c => c.id);
    logger.verbose(`SELECTION: Updating with latest prices from CardKingdom (${ids.length} cards)`);
    for (const id of ids) {
      await this.updatePricesById(dictionaries, id, opts);
    }
  }

  public async updateAllSets (dictionaries: CardKingdomDictionaries, opts: UpdatePricesOptions) {
    await database.ready();
    const ids = await CardModel.distinct('id');
    logger.verbose(`ALL SETS: Updating latest prices from CardKingdom (${ids.length} cards)`);
    for (const id of ids) {
      await this.updatePricesById(dictionaries, id, opts);
    }
  }

  private async updatePricesById (dictionaries: CardKingdomDictionaries, cardId: string, opts: UpdatePricesOptions) {
    try {
      const card = await CardModel.findOne({
        deprecated: { $ne: true },
        $or: [
          { id: cardId },
          { oracle_id: cardId }
        ]
      }).select('+cardkingdom_id');

      if (card) {
        logger.verbose(`${cardId}: Querying prices - [${card.set_code}] ${card.name} - ${card.set_name}`);

        const isNameCloseEnough = (card: CardSchema, name: string, opts: { distance?: number } = {}) => {
          const distance = opts.distance ?? 2;
          return (levenshtein(card.name, name) <= distance)
            || (card.card_faces?.some(f => levenshtein(f.name, name) <= Math.max(0, distance - 1)));
        }

        const getProductMysteryBooster = (card: CardSchema, finish?: CardKingdomFinishPrefix) => {
          const products = dictionaries.productsByEdition['Mystery Booster/The List'];

          const potentials = products.filter(p => isNameCloseEnough(card, p.name));
          const matches = potentials.filter(p => !finish || (finish === 'F' ? p.variation === 'Foil' : p.variation === 'Foil Etched'))

          if (matches.length > 1) {
            const exact = matches.filter(p => isNameCloseEnough(card, p.name, { distance: 0 }));
            logger.warn('Multiple potential matches found for Mystery Booster variation.');
            logger.debug('%o', matches);

            if (exact.length) {
              logger.warn('Selected closest exact match: %o', exact[0]);
              return exact[0];
            }
          }

          return matches[0];
        };

        const getCollectorNumber = (card: CardSchema, length: number) => {
          let collectorNumber = Strings.replaceAll(card.collector_number, /\D+/, '');
          while (collectorNumber.length < length) {
            collectorNumber = '0' + collectorNumber;
          }
          return collectorNumber;
        }

        const getProductVaried = (setCode: string, finish?: CardKingdomFinishPrefix): CardKingdomProduct => {
          if (!setCode) {
            return null;
          }

          let result: CardKingdomProduct = null;

          LENGTH: for (let length of [2, 3, 4, 5]) {
            const collectorNumber = getCollectorNumber(card, length);
            const sku = `${this.getSetCode(setCode).toUpperCase()}-${collectorNumber.toUpperCase()}`;

            const isRetro = (card.frame === '1997');
            const isPromo = (card.promo);
            const normal = dictionaries.productsBySku[`${finish ?? ''}${sku}`];
            const promo = isPromo ? dictionaries.productsBySku[`${finish ?? ''}${sku}P`] : null;
            const retro = isRetro ? dictionaries.productsBySku[`${finish ?? ''}R${sku}`] : null;
            result = retro ?? promo ?? normal;

            if (result) {
              break LENGTH;
            }
          }

          return result;
        };

        const getProduct = (finish?: CardKingdomFinishPrefix) => {
          let set = dictionaries.setsByCode[card.set_code];
          let result = getProductVaried(card.set_code, finish);
          while (!result && set.parent_set_code && set.parent_set_code !== set.code) {
            set = dictionaries.setsByCode[set.parent_set_code];
            if (set) {
              result = getProductVaried(set.code, finish);
            }
          }
          if (!result && /plist|(c|m|f)?mb\d{1,}/i.test(card.set_code)) {
            result = getProductMysteryBooster(card, finish);
          }
          if (result && isNameCloseEnough(card, result.name)) {
            return result;
          }
          return null;
        };





        let product = getProduct();
        // console.log('KEYS', JSON.stringify(Object.keys(dictionaries.productsBySku).filter(k => k.includes('MRD'))))
        let productFoil = getProduct('F') || getProduct('TF');
        let productEtched = getProduct('FE');
        logger.debug(`${cardId}: product: %o`, product);
        logger.debug(`${cardId}: productFoil: %o`, productFoil);
        logger.debug(`${cardId}: product: %o`, productEtched);

        if (productEtched && !productEtched.variation.includes('Etched')) {
          productEtched = null;
          console.error('Etched variant was not etched.', card.id, productEtched);
        }

        // Determine if products are actually foil.
        if (product?.is_foil && !productFoil) {
          productFoil = product;
          product = null;
        } else if (!product && !productFoil?.is_foil && !productEtched) {
          product = productFoil;
          productFoil = null;
        }


        if (product || productFoil || productEtched) {
          logger.info(`${cardId}: Updating prices non_foil: ${!!product}, foil: ${!!productFoil}, etched: ${!!productEtched}`);
          await this.savePrices(dictionaries.prices, card, {
            nonfoil: product,
            etched: productEtched,
            foil: productFoil
          }, opts);
          this.updatedPricesCount++;
          logger.info(`${cardId}: Prices updated`);
        } else {
          logger.warn(`SKU [X//${card.set_code}-${card.collector_number}] not found for id: ${cardId}`);
          this.unmatched.push(card);

          const normalizedName = card.name?.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
          const productForPurchaseURI = dictionaries.productsByName[card.name]
            ?? dictionaries.productsByName[card.name.replace(/\s+\/\/.*/, '')]
            ?? dictionaries.productsByName[card.name.replace(/\s+\/\/\s+/, ' ')]
            ?? dictionaries.productsByName[card.name + ' Token']
            ?? dictionaries.productsByName[card.name.replace(/\s+\/\/\s+/, '') + ' Token']
            ?? dictionaries.productsByName[normalizedName]
            ?? dictionaries.productsByName[normalizedName.replace(/\s+\/\/.*/, '')]
            ?? dictionaries.productsByName[normalizedName.replace(/\s+\/\/\s+/, ' ')]
            ?? dictionaries.productsByName[normalizedName + ' Token']
            ?? dictionaries.productsByName[normalizedName.replace(/\s+\/\/\s+/, '') + ' Token'];

          if (productForPurchaseURI?.length) {
            logger.info(`${cardId}: Updating generic purchase URI for  ${card.set_code} ${card.name}`);
            await this.saveSearchURI(dictionaries.prices, card, opts);
            this.updatedMetadataCount++;

          } else {
            this.unmatchedMetadata.push(card);
            logger.error(`${cardId}: Not updating prices or purchase_uris for: ${card.set_code} ${card.name}`);
          }
        }
      } else {
        logger.error(`Card not found with ID: ${cardId}`);
      }

    } catch (err) {
      logger.error(`${cardId}: Error updating prices: %o`, err);
    }
  }

  private getSetCode (code: string) {
    return CARDKINGDOM_SET_CODE_MAP[code] ?? code;
  }

  // private async updatePricesByCardAndSet (dictionaries: CardKingdomDictionaries, setCode: string, cardName: string, opts: UpdatePricesOptions) {
  //   try {
  //     logger.verbose(`${setCode}, ${cardName}: Fetching latest prices from Scryfall`);
  //     const cards = await loader.getCardsByNameAndSet(cardName, setCode);
  //     logger.info(`${setCode}, ${cardName}: Updating prices`);
  //     await this.savePrices(cards, opts);
  //     logger.info(`${setCode}, ${cardName}: Prices updated successfully`);
  //   } catch (err) {
  //     logger.error(`${setCode}, ${cardName}: Error updating prices: %o`, err);
  //   }
  // }

  // private async updatePricesByCard (dictionaries: CardKingdomDictionaries, cardName: string, opts: UpdatePricesOptions) {
  //   try {
  //     logger.verbose(`${cardName} Fetching latest prices from Scryfall`);
  //     const cards = await loader.getCardsByName(cardName);
  //     logger.info(`${cardName} Updating prices`);
  //     await this.savePrices(cards, opts);
  //     logger.info(`${cardName} Prices updated successfully`);
  //   } catch (err) {
  //     logger.error(`${cardName}: Error updating prices: %o`, err);
  //   }
  // }

  public async savePrices (pricesheet: CardKingdomPriceSheet, card: CardSchema & Document, products: {
    etched: CardKingdomProduct,
    foil: CardKingdomProduct,
    nonfoil: CardKingdomProduct,
  }, opts: UpdatePricesOptions & {
    metadataOnly?: boolean
  }) {

    if (opts.force || cardRequiresPriceUpdate('cardkingdom', card)) {
      if (!card.prices?.cardkingdom) {
        card.prices.cardkingdom = {
          created: Time.now(),
          updated: Time.now()
        };
      }

      if (products.nonfoil || products.foil || products.etched) {
        logger.debug(`${card.id}: Updating purchase URI`);
        card.purchase_uris.cardkingdom = pricesheet.meta.base_url
          + Strings.replaceAll((products.nonfoil?.url ?? products.foil?.url ?? products.etched?.url), /\\/, '')
          + '?partner=topdecked&utm_source=topdecked&utm_medium=affiliate&utm_campaign=topdecked';
      } else {
        logger.debug(`${card.id}: Not updating purchase URI`);
      }

      if (products.etched) {
        logger.debug(`${card.id}: Updating purchase URI`);
        card.purchase_uris.cardkingdom_etched = pricesheet.meta.base_url
          + Strings.replaceAll((products.etched).url, /\\/, '')
          + '?partner=topdecked&utm_source=topdecked&utm_medium=affiliate&utm_campaign=topdecked';
      } else {
        logger.debug(`${card.id}: Not updating purchase URI`);
      }

      const pointPrice = (conditions: CardKingdomProduct['condition_values'], point: CardKingdomPricePoint) => {
        let result = null;
        if (conditions) {
          if (Numbers.ensureInteger(conditions[point + '_qty']) > 0) {
            result = Numbers.ensureFloat(conditions[point + '_price']);
          }
        }
        return result;
      };

      if (!opts.metadataOnly) {
        const prefixes = { nonfoil: '', foil: '_foil', etched: '_etched' };
        for (const variant of ['nonfoil', 'foil', 'etched']) {
          const xfix = prefixes[variant];
          const idProp = `cardkingdom${xfix}_id`;
          if (products[variant]) {
            if (!card[idProp]) {
              card[idProp] = products[variant].id;
            }

            const conditions = products[variant].condition_values;
            card.prices.cardkingdom[`usd${xfix}`] = Numbers.round(Numbers.ensureFloat(products[variant].price_retail), 2);
            card.prices.cardkingdom[`usd${xfix}_low`] = Numbers.round(pointPrice(conditions, 'g')
              ?? pointPrice(conditions, 'vg')
              ?? pointPrice(conditions, 'ex')
              ?? pointPrice(conditions, 'nm')
              ?? Numbers.ensureFloat(products[variant].price_retail), 2);

            const mid = Arrays.nonNullValues(MIDPOINTS.map((p: CardKingdomPricePoint) => pointPrice(conditions, p)));
            card.prices.cardkingdom[`usd${xfix}_mid`] = Numbers.round(Arrays.sum(mid) / (mid.length > 0 ? mid.length : 1), 2);
            card.prices.cardkingdom[`usd${xfix}_qty`] = Numbers.ensureInteger(products[variant].qty_retail);
            card.prices.cardkingdom[`usd${xfix}_buy`] = Numbers.round(Numbers.ensureFloat(products[variant].price_buy), 2);
            card.prices.cardkingdom[`usd${xfix}_buy_qty`] = Numbers.ensureInteger(products[variant].qty_buying);
          } else if (opts.force) {
            card[idProp] = undefined;
            card.prices.cardkingdom[`usd${xfix}`] = undefined;
            card.prices.cardkingdom[`usd${xfix}_low`] = undefined;
            card.prices.cardkingdom[`usd${xfix}_mid`] = undefined;
            card.prices.cardkingdom[`usd${xfix}_qty`] = undefined;
            card.prices.cardkingdom[`usd${xfix}_buy`] = undefined;
            card.prices.cardkingdom[`usd${xfix}_buy_qty`] = undefined;
          }
        }

        card.prices.cardkingdom.updated = Time.now();
      }

    } else {
      logger.info(`${card.id}: Prices already current`);
    }

    if (!opts.modifyOnly) {
      logger.verbose(`${card.id}: Prices updated for "${card.name}": %o`, card.prices.cardkingdom);
      return card.save();
    }

    return card;
  }


  public async saveSearchURI (pricesheet: CardKingdomPriceSheet, card: CardSchema & Document, opts: UpdatePricesOptions) {

    if (opts.force || cardRequiresPriceUpdate('cardkingdom', card)) {
      if (!card.prices?.cardkingdom) {
        card.prices.cardkingdom = {
          created: Time.now(),
          updated: Time.now()
        };
      }

      if (opts?.force || !card.purchase_uris?.cardkingdom?.match('topdecked')) {
        const name = card.name.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
        card.purchase_uris.cardkingdom = pricesheet.meta.base_url
          + `catalog/search?search=header&filter%5Bname%5D=${encodeURIComponent(name)}`
          + '&partner=topdecked&utm_source=topdecked&utm_medium=affiliate&utm_campaign=topdecked';
      }


    } else {
      logger.info(`${card.id}: Card purchase URIs already exist`);
    }

    if (!opts.modifyOnly) {
      logger.verbose(`${card.id}: Card purchase URIs updated for "${card.name}": %o`, card.prices.cardkingdom);
      return card.save();
    }

    return card;
  }


  private async getPriceSheetJson (opts: { force?: boolean } = {}): Promise<CardKingdomPriceSheet> {
    const filename = 'cardkingdom-pricesheet.json';
    const result = await Downloads.downloadIfExpired({
      ...opts,
      filename,
      download: async () => request({
        method: 'GET',
        uri: 'https://api.cardkingdom.com/api/v2/pricelist',
        headers: {
        },
        qs: {
        },
        json: true
      }),
      ttl: Time.ttl(2 * Time.HOUR)
    });
    return result.data ?? await Downloads.readJsonFile({ filename });
  }

}
