import { database } from '../../config/database';
import { CardModel } from '../../model';
import { logger } from '../../util/logger';
import {
  BASE_CLEANUP_DEPRECATED_QUERY, CatalogService, UpdateMetadataOpts
} from '../api/metadata.api';

export class CardKingdomCatalog implements CatalogService {
  async updateAllMetadata<T extends UpdateMetadataOpts> (opts: T) {
  }
  async cleanupDeprecatedMetadata<T extends UpdateMetadataOpts> (opts: T) {
    await database.ready();
    logger.info('cleaning deprecated metadata from  %s in %s', (opts.cardId ?? 'all cards'), (opts.setCode ?? 'all sets'));
    const query = {
      ...BASE_CLEANUP_DEPRECATED_QUERY(opts),
      $or: [
        { cardkingdom_id: { $exists: true } },
        { cardkingdom_foil_id: { $exists: true } },
        { cardkingdom_etched_id: { $exists: true } }
      ]
    };
    logger.debug('querying for cards matching product ids: %o', query);
    const cards = await CardModel.find(query).select('+cardkingdom_id +cardkingdom_foil_id cardkingdom_etched_id +metadata').exec();

    if (cards.length > 0) {
      const cardsNonDeprecated = cards.filter(c => c.deprecated);
      if (cardsNonDeprecated?.length) {
        logger.error(`Attempted to remove metadata from non-deprecated cards, %o`, cardsNonDeprecated.map(c => c.id))
        throw new Error('Attempted to remove metadata from non-deprecated cards');
      }

      try {
        query.deprecated = true; // we REALLY don't want this to be wrong
        const updated = await CardModel.updateMany(query, {
          $unset: {
            cardkingdom_id: 1,
            cardkingdom_foil_id: 1,
            cardkingdom_etched_id: 1
          }
        }).exec();

        const updatedCount = updated.modifiedCount + updated.upsertedCount;

        if (updatedCount !== cards.length) {
          throw new Error(`Modified documents count ${updatedCount} differs from expected ${cards.length}`);
        }

        logger.info(`${updatedCount}/${cards.length} cards updated`);
      } catch (err) {
        logger.error('Failed to cleanup deprecated card metadata %o', query);
        throw err;
      }

    } else {
      logger.info(`No deprecated cards to update`);
    }
  }

}
