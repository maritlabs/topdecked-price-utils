

import {
  CardModelInitializer, CardModelV1Initializer, MongooseInstance, PriceMotionModelInitializer,
  PriceMotionModelInitializerV1, SetModelInitializer, SetModelV1Initializer,
  SystemConfigModelInitializer
} from '@topdecked/common-model';

const mongoose = MongooseInstance.get();

export const SetModel = SetModelInitializer(mongoose);
export const SetModelV1 = SetModelV1Initializer(mongoose);

export const CardModel = CardModelInitializer(mongoose);
export const CardModelV1 = CardModelV1Initializer(mongoose);

export const PriceMotionModelV1 = PriceMotionModelInitializerV1(mongoose);
export const PriceMotionModelV2 = PriceMotionModelInitializer(mongoose);

export const SystemConfigElement = SystemConfigModelInitializer(mongoose);

CardModelV1.createIndexes();
SetModelV1.createIndexes();
