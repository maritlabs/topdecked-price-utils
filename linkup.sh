#!/bin/bash

npm link mongoose
npm link @types/mongoose
npm link @topdecked/common-model

printf "\e[43mWARNING:\e[m Be sure to run 'npm link mongoose' in '@topdecked/common-model'\n"
